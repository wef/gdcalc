![gdcalc](images/hp-16c-48.png)

# gdcalc 

a Financial, Scientific, Statistical & Programming calculator for Unix/Linux (since 1983!)

![gdcalc](images/gdcalc-3.png)

**gdcalc** is a financial, statistics, scientific and programmers calculator for Unix and Linux. The GUI was written with **glade**(1) and uses the Gtk toolkit - so it may well be compatible with themes and other whiz-bang features of those systems.
**gdcalc** provides both Algebraic notation (ie. conventional, TI or Casio style) and Reverse Polish Notation (RPN or Hewlett-Packard style). If you've not heard of RPN before, you are probably familiar with algebraic calculators. Very briefly, while simpler and more natural to use, RPN calculators may need some study eg. they have an Enter key instead of the equals key and are based on the use of a stack.

**gdcalc** is based on my venerable dcalc RPN calculator which I wrote about a million years ago to learn C and curses - about 1983, I suppose.

The original dcalc for curses (Unix console) is at [dcalcCurses](http://bhepple.freeshell.org/dcalc/unix/dcalcCurses.html) and is also included in this package.

If you want to know more about RPN calculators (and why they are more intuitive than algebraic calculators) take a look at http://www.hpcalc.org

Note: if you are using a locale other than English, please make sure you run **gdcalc** with the following command (thanks to Gösta):

    LC_NUMERIC=en_US gdcalc &
    
Note: if you get a badly drawn border around windows in **dcalc** (curses-mode in a terminal) then try this:

    TERM=gnome dcalc # or
    TERM=vte dcalc

## Major modes

All functions are available in both algebraic and RPN modes:
* Financial mode - compound interest, present value, final value, interest, number of payments etc - these all work in the same way as in the HP calculators. Annuities can be set for the start or the end of the compounding period. Days between dates.
* Scientific mode - Sin, Cos, Tan, Sinh, Cosh, Tanh, log, ln, etc and their inverses
* Statistics mode - Sum, Sum-, mean, std dev, factorial, Permutations & combinations, Linear regression
* Programming mode - Bin, Oct, Dec, Hex, Ascii and Internet Address (IP) displays. AND OR NOT MOD and shift operations. Prime factors.

## Conversions

There is a full set of unit conversions including:
* length - mm, cm, m, km, in, feet, mile, nautical mile etc etc
* area - acre, hectare, sq. m, sq. mm, sq. in., sq. foot etc etc
* volume - gallons, pints, litres, cu. m, cu. feet etc etc
* mass - kg, pound, ton, tonne, gram etc etc
* speed - kph, mph. ft/s, m/s etc
* fuel consumption - mpg, litres/100 km
* pressure - atmospheres, mmHg, pounds/sq.in., Pascals etc
* temperature - centigrade, fahrenheit, Kelvin
* ... and anything else that **units**(1) can cope with.

Metric, US and British units are supported - e.g. a US gallon is a measly 3.78 litres as opposed to the much more generous British gallon at 4.54 litres.

## Extending gdcalc

**gdcalc** is programmable (quite apart from the fact that you can get the source!) and I've left loads of blank keys on the GUI for you to add in your favourites. Just click on a blank button and add your own label and (algebraic) formula. You can reference x, y, z, t and l registers eg label="**HYP**" expr="sqrt(x^2+y^2)".

Custom keys are indicated by the prefix "CUSTOM: " in the tooltip, visible if you hover the mouse over the button.

To change or delete a custom key, right click on it.

## Keyboard shortcuts


| key | function |
|---|---|
| s | store |
| r | recall |
| y | exchange x and y |
| z | exchange x and z |
| t | exchange x and t |
| l | last x |
| x | clear x |
| &lt;down> | ROLL |
| % | X percent of Y |
| ESC | INV |
| &lt;enter>, &lt;up> | enter |
| &lt;bsp> | backspace |
| Ctrl+c | Copy |
| Ctrl+v | Paste |
| Ctrl+d | degrees mode |
| Ctrl+r | radians mode |
| Ctrl+f | Financial mode |
| Ctrl+s | Scientific mode |
| Ctrl+t | Statistics mode |
| Ctrl+p | Programming mode |
| Ctrl+q | quit |
| Shift+Ctrl+a | Convert Area |
| Shift+Ctrl+c | Convert Currency |
| Shift+Ctrl+f | Convert fuel |
| Shift+Ctrl+l | Convert length |
| Shift+Ctrl+m | Convert mass |
| Shift+Ctrl+o | Convert other |
| Shift+Ctrl+p | Convert pressure |
| Shift+Ctrl+s | Convert speed |
| Shift+Ctrl+t | Convert temperature |
| Shift+Ctrl+v | Convert volume |

## Styling

Version 3.x (the image at top of page) uses CSS styles  in `~/.config/gdcalc/gdcalc.css` or `/etc/gdcalc/gdcalc.css`:

    .register_entry_class { 
        font-family: "Lcd2"; /* http://legionfonts.com/fonts/lcd2-normal */
        font-size: 25px;
        background-color: lightgrey;
    }
    
    .register_x_entry_class { 
        background-color: lightblue;
    
    }
    
    .button_class {
        font-family: "Free Sans"; /* gnu-free-sans-fonts-20120503-24.fc34.noarch */
        font-size: 15px;
        font-weight: 800;
        padding: 3px 0px 3px;
    }
    
    .custom_button_class {
        color: black;
    }


There is a sample CSS file `/etc/gdcalc/gdcalc.css` with some good fonts
and colours. This uses fonts "Lcd2" and "Free Sans".

For "Lcd2", download the zip file from
[legionfonts](http://legionfonts.com/fonts/lcd2-normal) and install the ttf in
`/usr/share/fonts`.

For "Free Sans" on RPM-based systems, install
gnu-free-sans-fonts.

Customise and install as `~/.config/gdcalc/gdcalc.css`

## Screenshots

The following screenshots are from version 2.x - see above for version 3.x

### Algebraic (Casio/TI), financial mode

![gdcalc](images/gdcalc_alg.png)

### RPN (HP), scientific mode

![gdcalc](images/gdcalc_sci.png)

### Programming mode

![gdcalc](images/gdcalc_pro.png)

### Statistics mode

![gdcalc](images/gdcalc_sta.png)

## Building

You will need autoconf, automake, libtool, ncurses-devel, gtk3-devel as well as gcc and make.

### Build and install from tarball or git clone:

    ./autogen.sh
    ./configure
    make
    sudo make install

The **gdcalc**(1) binary should now appear in `/usr/bin`

### Build and install from tarball or git clone, using **stow**(1):

    ./autogen.sh
    ./configure --prefix=/usr/local/stow/gdcalc
    make
    sudo mkdir -p /usr/local/stow/gdcalc
    sudo make install
    cd /usr/local/stow
    sudo stow gdcalc
    
The **gdcalc**(1) binary should now appear in `/usr/local/bin`

### Build an RPM

You will also need the RPM devel toolchain including the `rpm-build` package

    ./autogen.sh
    ./configure
    make rpm

## Installing from RPM

[![Packaging status](https://repology.org/badge/vertical-allrepos/gdcalc.svg)](https://repology.org/project/gdcalc/versions)

Fedora-33, -34 and -35: (until it gets into the main repositories):

    sudo dnf copr enable wef/gdcalc
    sudo dnf install gdcalc

From an rpm on RPM-based systems, eg fedora:

    sudo rpm -U gdcalc-x.xx-x.x86_64.rpm
    
From rpm on debian-based systems, eg ubuntu: (not tested)

    sudo alien -i gdcalc-x.xx-x.x86_64.rpm
    
On arch: it's in [aur](https://aur.archlinux.org/packages/gdcalc/)

## Copyright

Copyright (C) 1983-2023 Bob Hepple
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


## Contact

For corrections/additions/suggestions for this page, please send email to: [[ bob dot hepple at gmail dot com ]]

Copyright (c) 1983-2023 Bob Hepple. All rights reserved.

<!-- 
Local Variables:
mode: gfm
markdown-command: "pandoc -s -f gfm --metadata title=README"
eval: (add-hook 'after-save-hook (lambda nil (shell-command (concat markdown-command " README.md > README.html"))) nil 'local)
End:
-->
