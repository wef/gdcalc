#!/bin/sh
set -e

BETA="$1" # add any argument to build a beta!!

SUDO=sudo # you might have sudo

export VERSION=$( awk -F',' '/AC_INIT\(/ { print $2 }' configure.ac | tr -cd '0-9.')
export VERSION="${VERSION// /}"
[ "$BETA" ] && {
    VERSION="${VERSION// /}.$(date '+%Y%m%d%H%M')"
}

export SVNVERSION=`echo $VERSION | sed -e 's/\./_/g'`
export BASE=`pwd`
DIR=gdcalc-$VERSION
[ -r $DIR ] && {
  echo "$DIR already exists, delete?"
  read ANS
  [ "$ANS" != 'y' ] && exit 1
  rm -rf $DIR || exit $?
}
type rpmbuild || exit $?

echo "Preparation before running this script in anger:

Firstly, you can run this with any argument to do a beta.

Make sure the version number is correct in configure.ac:

$( grep 'AC_INIT(' configure.ac /dev/null )

Then:
<QUOTE>
svn cp https://svn.code.sf.net/p/gdcalc/code/trunk https://svn.code.sf.net/p/gdcalc/code/tags/gdcalc-${SVNVERSION}
svn commit -m \"add gdcalc-${SVNVERSION} tag\" https://svn.code.sf.net/p/gdcalc/code/tags/gdcalc-${SVNVERSION}
cd /tmp
rm -rf gdcalc
svn checkout --username=bhepple svn+ssh://bhepple@svn.code.sf.net/p/gdcalc/code/tags/${SVNVERSION} gdcalc
cd gdcalc
./make_release
</QUOTE>

But you can also run it from the source directory.

Press enter to continue or ^C:
"

read P
mkdir -p $DIR/lib $DIR/doc $DIR/bin $DIR/po $DIR/ui $DIR/pixmaps $DIR/src

DOCS="doc/gdcalc.1 doc/dcalc.1 doc/manual_en.html"
ICONS="pixmaps/gdcalc.png pixmaps/HP-16C-48.png pixmaps/HP-16C-48.xpm pixmaps/mini-HP-16C-48.xpm"
SUPPORT="gdcalc.desktop gdcalc.glade2 gdcalc.spec.in gdcalc.rc configure.ac autogen.sh Makefile.am README.md COPYING ChangeLog AUTHORS NEWS"

cp -p $SUPPORT $DIR/
cp -p $DOCS $DIR/doc
cp -p $ICONS $DIR/pixmaps
cp src/*.[chy] src/Makefile.am $DIR/src

[ "$BETA" ] && {
    sed -i "s/\(AC_INIT(.*,\).*)/\1 [$VERSION])/" $DIR/configure.ac
}

(cd $DIR; ./autogen.sh; ./configure; autoreconf -i )
tar czf ${DIR}.tar.gz $DIR

# build RPM package if rpmbuild is installed:
if type rpmbuild &> /dev/null; then
    export RPM_ROOT=$(rpmbuild --eval %_topdir 2>/dev/null)
    mkdir -p ${RPM_ROOT}
    pushd ${RPM_ROOT} &>/dev/null
    mkdir -p SOURCES BUILD RPMS RPMS/noarch SRPMS
    popd &>/dev/null
    cp -f ${BASE}/gdcalc-${VERSION}.tar.gz ${RPM_ROOT}/SOURCES/
    rpmbuild -ba ${BASE}/gdcalc-$VERSION/gdcalc.spec
fi

[[ "$BETA" ]] && exit 0

PACKAGES="${RPM_ROOT}/SRPMS/gdcalc-${VERSION}-1.src.rpm ${RPM_ROOT}/RPMS/noarch/gdcalc-${VERSION}-1.noarch.rpm gdcalc-${VERSION}.tar.gz gdcalc-${VERSION}.ebuild"

echo "Now upload $PACKAGES to https://sourceforge.net/projects/gdcalc/files/gdcalc/"
