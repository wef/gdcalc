/* Platform independent parts of DCALC - Reverse Polish Notation (RPN)
 * calculator.

    Contact info:
    bob.hepple@gmail.com
    https://gitlab.com/wef/gdcalc

    $Id: dcalc.c,v 1.13 2006/12/30 22:46:07 bhepple Exp $

    Copyright (C) 1999-2018 Bob Hepple

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING. If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth
    Floor, Boston, MA 02110-1301 USA

 */

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/param.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h> /* for pipe() */
#include <time.h> /* for localtime */
#include <float.h> /* for DBL_MAX */

extern int errno;

#ifdef HAVE_CONFIG_H /* gtk & wx */
#  include "config.h"
#endif

#include "dcalc.h"

#ifdef HAS_ALGEBRAIC_MODE
extern int yyparse(void);
extern void yyinit(char *);

long    dcalc_xSave, dcalc_ySave, dcalc_zSave, dcalc_tSave, dcalc_lSave;  /* Shadow of Integer stack */
double  dcalc_xfSave,dcalc_yfSave,dcalc_zfSave,dcalc_tfSave,dcalc_lfSave; /* Shadow of floating point stack */
char   dcalc_last_eval[MAX_ENTRY];
int    dcalc_algebraic_mode = 0;
#endif /* HAS_ALGEBRAIC_MODE */

long    dcalc_xiReg, dcalc_yiReg, dcalc_ziReg, dcalc_tiReg, dcalc_liReg;  /* Integer stack */
long    dcalc_reg[NUMREGS];   /* Integer registers */

double  dcalc_xfReg,dcalc_yfReg,dcalc_zfReg,dcalc_tfReg,dcalc_lfReg; /* Floating point stack */

double  dcalc_regf[NUMREGS],                 /* Floating point registers                  */
    pi,                                /* The usual pi.                             */
    d_to_r,                            /* Degrees to radians factor                 */
    r_to_d;                            /* Radians to degrees factor                 */

/*

  The widget 'entry_X' is schizophrenic.
  Initially:
      displays a simple number generated from dcalc_xiReg/dcalc_xfReg. Entering=False.

  On a key stroke:
      stack is lifted. Entering= True.
      displays a char buffer.
  
 */

BOOLEAN dcalc_entering,                      /* True if we are still dcalc_entering into X      */
    dcalc_invert,                            /* Invert flag - causes the next operation   */
                                       /* to be inverted eg arcsin() instead of sin */
    dcalc_degree,                            /* True if numbers are in degrees; else      */
                                       /* radians                                   */
    was_initialised = 0;

/* To make sure program is only initialised once */
BOOLEAN
dcalc_last_was_fin_key;/* True if last operation was financial      */
BOOLEAN dcalc_finPayAt0 = 0;

char dcalc_inbuf[MAX_ENTRY],      /* Where typed characters are put            */
    *inptr,         /* Pointer into dcalc_inbuf                        */
    outbuf[MAX_ENTRY],     /* General buffer used to format output      */
    zero_string[] = "0",
    div_by_zero[] = "Division by 0!";

int dcalc_decplaces,  /* number of decimal places                  */
    max_digits;     /* Maximum number of digits displayable      */

int     dcalc_mode, dcalc_intmode, dcalc_floatmode;

typedef unsigned char byte;

#define DIALOG_LETTERS "(Y or N)"
#define DIALOG_NUMBERS "[0-9]"

/* function prototypes: */
static void echo_char(byte c);
static void save_x(void );
#if 0
static long round(double f);
#endif
static void process_digit(COMMAND c);

char *dcalc_cpy(char *dest, const char *src, size_t n) {
    size_t i;

    for (i = 0; i < n - 1 && src[i]; i++)
        dest[i] = src[i];
    if (i < n - 1)
        dest[i] = 0;
    dest[n - 1] = 0;

    return dest;
}

int
dcalc_is_inverted() {
    int retval = dcalc_invert;

    dcalc_invert = FALSE;
    gui_print_inv();
    gui_clear_msg();
    return retval;
}

static void fmt_ip(char *str, int x) {
    int negative, i1,i2,i3,i4;

    if (x < 0) {
        x = x & 0x7fffffff;
        negative = 1;
    } else {
        negative = 0;
    }
    i4 = x & 0xff;
    x = x >> 8;
    i3 = x & 0xff;
    x = x >> 8;
    i2 = x & 0xff;
    x = x >> 8;
    if (negative)
        x = x | 0x80;
    i1 = x & 0xff;
    sprintf(str, "%03d.%03d.%03d.%03d", i1, i2, i3, i4);
}

void fmt_bin(char *str, long x) {
    char inverted[50],
        reverted[50],
        *s, *t;
    BOOLEAN negative;

    if (x < 0) {
        negative = TRUE;
        x &= 0x7FFFFFFF;
    } else
        negative = FALSE;
    s = inverted;
    while (x != 0) {
        *(s++) = '0' + (char) (x % 2);
        x /= 2;
    }
    *s = ASCIIZ;
    while (strlen(inverted) < 31) {
        *(s++) = '0';
        *s = ASCIIZ;
    }
    if (negative)
        *(s++) = '1';
    else
        *(s++) = '0';
    *s = ASCIIZ;
    if (s == inverted) /* x==0 */
        strcpy(str, zero_string);
    else {
        t = reverted;
        while (s != inverted)
            *(t++) = *(--s);
        *t = ASCIIZ;
        sprintf(str, "%32.32s", reverted);
    }
}

void dcalc_fmt_base(char *s, long x, double xf) {
    char        format[20];
    char        *p;
    union       frig {
        long j;
        byte c[4];
    } frigger;
    int         i, leading_zero;

    if (dcalc_mode == PROG) {
        switch (dcalc_intmode) {
        case ASCIIM:
            frigger.j = x;
            for (i = 0; i < 4; i++) {
#if REVERSE_BYTES
                format[3 - i] = frigger.c[i];
#else
                format[i] = frigger.c[i];
#endif
            }
            format[4] = 0;
#if PCSPECIFIC
            p = format;
            for (i = 0; i < 4; i++, p++)
                if (*p == 0)
                    *s++ = ' ';
                else
                    *s++ = *p;
            *s = 0;
#else
            /* Convert to canonical form */
            p = format;
            leading_zero = 1;
            for (i = 0; i < 4; i++, p++) {
                if ((i < 3) && (*p == 0) && leading_zero)
                    continue;
                leading_zero = 0;
                if (*p & 0x80) {
                    strcpy(s, "a-");
                    s += 2;
                    *p &= 0x7f;
                }
                switch (*p) {
                case 0:
                    strcpy(s, "^spc");
                    s += 4;
                    break;
                case 7:
                    strcpy(s, "bell");
                    s += 4;
                    break;
                case 8:
                    strcpy(s, "bs");
                    s += 2;
                    break;
                case 9:
                    strcpy(s, "tab");
                    s += 3;
                    break;
                case 10:
                    strcpy(s, "lf");
                    s += 2;
                    break;
                case 13:
                    strcpy(s, "cr");
                    s += 2;
                    break;
                case 27:
                    strcpy(s, "esc");
                    s += 3;
                    break;
                case 32:
                    strcpy(s, "spc");
                    s += 3;
                    break;
                case 0x7F:
                    strcpy(s, "del");
                    s += 3;
                    break;
                default:
                    if (*p < 32) {
                        *s++ = '^';
                        *s++ = *p+0x60;
                    } else
                        *s++ = *p;
                }
                *s++ = ' ';
            }
            *s = 0;
#endif
            break;

        case BIN:
            fmt_bin(s, x);
            break;

        case IP:
            fmt_ip(s, x);
            break;

        case OCT:
            if (x == 0L)
                strcpy(s, zero_string);
            else
                sprintf(s, "%32.1o", (int)x);
            break;
        case DEC:
            if (x == 0L)
                strcpy(s, zero_string);
            else
                sprintf(s, "%32.1d", (int)x);
            break;
        case HEX:
            if (x == 0L)
                strcpy(s, zero_string);
            else
                sprintf(s, "%32.1x", (int)x);
            break;
        }
    } else {
        if (xf == 0.0) {
            strcpy(s, "0.0");
            return;
        }

        if (dcalc_floatmode == SCIFORMAT) {
            sprintf(format, "%%32.%dE", dcalc_decplaces);
            sprintf(s, format, xf);
        } else if (dcalc_floatmode == ENGFORMAT) {
            double mant;
            int isNeg = 0, exp;
            char *e, outb[80];

            if (xf < 0.0)
                isNeg = 1;
            mant = fabs(xf);
            exp = log10(mant);
            exp = exp / 3;
            exp = exp * 3;
            mant = mant * pow(0.1, (double) exp);
            while (mant < 0.1) {
                mant = mant * 1000.0;
                exp = exp - 3;
            }
            if (isNeg)
                mant = -mant;
            sprintf(outb, "%32.2E", pow(10.0, exp));
            e = strchr(outb, 'E');
            if (e) {
                sprintf(format, "%%32.%df%%s", dcalc_decplaces);
                sprintf(s, format, mant, e);
            } else {
                sprintf(format, "%%32.%dg", dcalc_decplaces);
                sprintf(s, format, xf);
            }
        } else { /* FIXFORMAT */
            double lowerLimit = pow(10.0, -dcalc_decplaces);
            double upperlimit = 1.0e25 / pow(10.0, dcalc_decplaces);

            if ((fabs(xf) > upperlimit) || (fabs(xf) < lowerLimit)) {
                sprintf(format, "%%32.%dE", dcalc_decplaces);
            } else {
                sprintf(format, "%%32.%df", dcalc_decplaces);
            }
            sprintf(s, format, xf);
        }
    }
}

void dcalc_prep_for_output(char *string) {
    /* Put suitable separators in string */
    char        *s, *d, sep = 0, *ft, rhs[MAX_ENTRY], lhs[MAX_ENTRY], scratch[MAX_ENTRY], prefix[3];
    int         len, interval = 0, count, negative;

    prefix[0] = 0;
    if (dcalc_mode == PROG) {
        switch (dcalc_intmode) {
        case ASCIIM:
            return; /* Nasty to return here - its easy to miss it when reading! */
        case IP:
            return; /* all done by fmt_ip */
        case BIN:
            sep = SPACE;
            interval = 8;
            break;
        case OCT:
            sep = SPACE;
            interval = 3;
            strcpy(prefix, "0");
            break;
        case DEC:
            sep = COMMA;
            interval = 3;
            break;
        case HEX:
            sep = SPACE;
            interval = 2;
            strcpy(prefix, "0x");
            break;
        }
    } else { /* FIXFORMAT or ENGFORMAT */
        sep = COMMA;
        interval = 3;
    }

    /* Find first non-blank ... */
    s = string;
    while (*s && isspace(*s)) s++;
    if (*s == '-') {
        negative = TRUE;
        *s++ = SPACE;
    } else
        negative = FALSE;

    /* move rhs to safe keeping - fraction or exponent part */
    ft = NULL;
    *rhs = 0;
    if ((dcalc_mode != PROG) && ((ft = strchr(s, '.')) ||
                           (ft = strchr(s, 'E')) ||
                           (ft = strchr(s, 'e')))) {
        dcalc_cpy(rhs, ft, MAX_ENTRY);
        *ft = 0;
    }

    /* add separators to the integer part ... */
    dcalc_cpy(lhs, s, MAX_ENTRY);
    len = strlen(s);
    if (len > interval) {
        count = 0;
        d = scratch;
        s = s + len - 1;
        while (--len >= 0) {
            if (count++ == interval) {
                *d++ = sep;
                count = 1;
            }
            *d++ = *s--;
        }
        *d = 0;
        /* reverse the string 'scratch' to 'lhs' */
        s = scratch + strlen(scratch) - 1;
        d = lhs;
        while (s >= scratch)
            *d++ = *s--;
        *d = 0;
    }

    /* Now reassemble ... */
    d = string;
    if (negative) {
        *d++='-';
    }
    *d = 0;
    strcpy(d, prefix);
    strcat(d, lhs);
    strcat(d, rhs);
}

long
dcalc_asc2int(char *s) {
    union       frig {
        long j;
        byte c[4];
    } frigger;
    int         i;

    for (i = 0; i < 4; i++)
#if REVERSE_BYTES
        frigger.c[3 - i] = *s++;
#else
    frigger.c[i] = *s++;
#endif
    return(frigger.j);
}

#if 0
long
dcalc_bin2int(char *s) {
    char        *t;
    long        accum;

    accum = 0;
    t = s;
    while (*t)
        accum = 2 * accum + (*(t++) - '0');
    return(accum);
}
#endif

long
dcalc_ip2int(char *s) {
    int index, i[4], leftBitSet = 0;
    long retval;

    if (!s || !*s)
        return 0;

    for (index = 0; index < 4; index++) {
        char *dot = strchr(s, '.');
        i[index] = 0;
        if (dot)
            *dot = 0;
        if (strlen(s)) {
            i[index] = atoi(s);
            i[index] &= 0xff;
            s += strlen(s);
        }
        if (dot) {
            *dot = '.';
            s = dot + 1;
        }
    }
    if (i[0] > 0x7f) {
        leftBitSet = 1;
        i[0] &= 0x7f;
    }
    retval = i[0];
    retval = (retval << 8) | i[1];
    retval = (retval << 8) | i[2];
    retval = (retval << 8) | i[3];
    if (leftBitSet)
        retval |= 0x80000000;
    return retval;
}

/* Note that strtol appears to have a bug - it cannot accept negative
 * hex, octal or binary numbers e.g. 0xffffffff, which is -1, hence
 * the special processing

 FIXME: this should be able to also parse an algebraic expression - would simplify things?

*/
void
dcalc_parse_buffer(char *buf, long *longResult, double *doubleResult) {
    if (dcalc_mode == PROG) {
        long temp = 0;
        int isNeg = 0;
        char *s, *d;

        /* remove leading 0's unless all are 0 */
        d = buf;
        s = buf;
        while (*s == '0')
            s++;
        if (s != d) {
            while (*s)
                *d++ = *s++;
            *d = 0;
        }

        switch (dcalc_intmode) {
        case ASCIIM:
            temp = dcalc_asc2int(buf);
            break;
        case IP:
            temp = dcalc_ip2int(buf);
            break;
        case BIN:
            if (strlen(buf) == 32 && *buf == '1') {
                d = buf; s= d + 1; while (*s) *d++=*s++; *d=0;
                isNeg = 1;
            }
            temp = strtol(buf, NULL, 2);
            break;
        case OCT:
            if (strlen(buf) == 11) {
                if (*buf == '2') {
                    d = buf; s= d + 1; while (*s) *d++=*s++; *d=0;
                    isNeg = 1;
                }
                if (*buf == '3') {
                    *buf = '1';
                    isNeg = 1;
                }
            }
            temp = strtol(buf, NULL, 8);
            break;
        case DEC:
            temp = strtol(buf, NULL, 10);
            break;
        case HEX:
            if (strlen(buf) == 8) {
                char b[2];
                int leftDigit;
                b[0] = *buf;
                b[1] = 0;
                leftDigit = strtol(b, NULL, 16);
                if (leftDigit & 8) {
                    *buf = (leftDigit & 7) + '0';
                    isNeg = 1;
                }
            }
            temp = strtol(buf, NULL, 16);
            break;
        }
        *longResult = isNeg? temp|0x80000000: temp;
    } else
        *doubleResult = strtod(buf, NULL);
}

void dcalc_push(long inx) {
    dcalc_tiReg = dcalc_ziReg;
    dcalc_ziReg = dcalc_yiReg;
    dcalc_yiReg = dcalc_xiReg;
    dcalc_xiReg = inx;
}

void dcalc_pushf(double inxf) {
    dcalc_tfReg = dcalc_zfReg;
    dcalc_zfReg = dcalc_yfReg;
    dcalc_yfReg = dcalc_xfReg;
    dcalc_xfReg = inxf;
}

long dcalc_pop() {
    long retval;
    retval = dcalc_xiReg;
    dcalc_xiReg = dcalc_yiReg;
    dcalc_yiReg = dcalc_ziReg;
    dcalc_ziReg = dcalc_tiReg;
    return retval;
}

double dcalc_popf() {
    double retval;
    retval = dcalc_xfReg;
    dcalc_xfReg = dcalc_yfReg;
    dcalc_yfReg = dcalc_zfReg;
    dcalc_zfReg = dcalc_tfReg;
    return retval;
}

/* puts X into the dcalc_entering state. Returns TRUE if we needed to
   start dcalc_entering ie if dcalc_entering==FALSE
 */
int
dcalc_start_entering() {
    long        temp;
    double      tempf;

    if (!dcalc_entering) {
        dcalc_entering = TRUE;
        if (dcalc_mode == PROG) {
            temp = dcalc_xiReg;
            dcalc_push(temp);
        } else {
            tempf = dcalc_xfReg;
            dcalc_pushf(tempf);
        }
        gui_dispnums();
        return TRUE;
    } else
        return FALSE;
}

int dcalc_stop_entering() {
    if (dcalc_entering) {
        dcalc_entering = FALSE;
        dcalc_parse_buffer(dcalc_inbuf, &dcalc_xiReg, &dcalc_xfReg);
        inptr = dcalc_inbuf;
        *inptr = 0;
        return TRUE;
    }
    return FALSE;
}

static void echo_char(byte c) {
    /* echo 'c' back to the screen and log in the input buffer */
    char        *p;

    if ((c == BACKSPACE) && (dcalc_mode != PROG || dcalc_intmode != ASCIIM)) {
        if (dcalc_entering && (inptr > dcalc_inbuf)) {
            inptr--;
            *inptr = ASCIIZ;
        } else {
            if (dcalc_mode == PROG)
                dcalc_xiReg = 0;
            else
                dcalc_xfReg = 0.0;
            inptr = dcalc_inbuf;
            *inptr = ASCIIZ;
            dcalc_entering = TRUE;
            gui_dispnums();
        }

    } else /* Neither backspace or ASCII dcalc_mode */ {
        if (dcalc_start_entering()) {
            for (inptr = dcalc_inbuf; inptr < dcalc_inbuf + 4; inptr++)
                *inptr = ASCIIZ;
            inptr = dcalc_inbuf;
        }

        if (inptr < dcalc_inbuf + max_digits)
            if (dcalc_mode == PROG && dcalc_intmode == ASCIIM) {
                for (p = dcalc_inbuf; p < dcalc_inbuf + 3; p++)
                    *p = *(p+1);
                dcalc_inbuf[3] = c;
                inptr++;
            } else {
                *inptr++ = c;
                *inptr = ASCIIZ;
            }
        else {
            gui_put_a_char(BELL);
            gui_msg("Too many digits!");
        }
    }

    if (dcalc_mode == PROG && dcalc_intmode == ASCIIM) {
        dcalc_xiReg = dcalc_asc2int(dcalc_inbuf);
        dcalc_fmt_base(outbuf, dcalc_xiReg, dcalc_xfReg);
    } else {
        dcalc_cpy(outbuf, dcalc_inbuf, MAX_ENTRY);
    }
    dcalc_prep_for_output(outbuf);
    gui_print_x(outbuf);
}

void dcalc_display() {
    gui_print_base();
    gui_print_deg();
    gui_dispnums();
    gui_dispregs();
    dcalc_invert = FALSE;
    gui_print_inv();
}

static void save_x() {
    dcalc_stop_entering();
    dcalc_liReg = dcalc_xiReg;
    dcalc_lfReg = dcalc_xfReg;
}

#if 0
static long round(double f) {
    BOOLEAN     negative;
    long        ret;

    negative = FALSE;
    if (f < 0.0) {
        negative = TRUE;
        f = -f;
    }
    ret = floor(f + 0.5);
    if (negative)
        ret = -ret;
    return(ret);
}
#endif

int
dcalc_exec_asciim() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode != PROG || dcalc_intmode != ASCIIM) {
        long tempx;
        tempx = dcalc_pop();
        dcalc_push(tempx);
        dcalc_mode = PROG;
        dcalc_intmode = ASCIIM;
        gui_msg("ASCII mode");
        gui_raw_mode(1);
        max_digits = 4;

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
    return 0;
}

static
void change_floatmode(COMMAND c) {
    dcalc_last_was_fin_key = 0;
    if ((c != dcalc_floatmode) || (dcalc_mode == PROG)) {
        char buf[MAX_ENTRY];

        if (dcalc_mode == PROG) dcalc_mode = SCI;
        dcalc_floatmode = c;
        dcalc_stop_entering();
        gui_raw_mode(0);
        max_digits = 20;
        sprintf(buf, "%s notation",
                (c==ENGFORMAT)? "Engineering":
                (c==SCIFORMAT)? "Scientific":
                "Fixed point");
        gui_msg(buf);

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
}

int
dcalc_exec_sciformat() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    change_floatmode(SCIFORMAT);
    return 0;
}

int
dcalc_exec_eng() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    change_floatmode(ENGFORMAT);
    return 0;
}

int
dcalc_exec_fix() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    change_floatmode(FIXFORMAT);
    return 0;
}

int
dcalc_exec_places() {
    long tempx;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (((tempx = gui_places("Enter number of decimal places")) >= 0) &&
        (tempx != dcalc_decplaces)) {
        char buf[MAX_ENTRY];

        dcalc_stop_entering();
        dcalc_decplaces = tempx;
        gui_raw_mode(0);
        max_digits = 20;
        sprintf(buf, "Places set to %d", dcalc_decplaces);
        gui_msg(buf);

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
    return 0;
}

int
dcalc_exec_ip() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if ((dcalc_intmode != IP) || (dcalc_mode != PROG)) {
        dcalc_mode = PROG;
        dcalc_stop_entering();
        gui_raw_mode(0);
        max_digits = 15;
        dcalc_intmode = IP;
        gui_msg("IP address mode");

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
    return 0;
}

int
dcalc_exec_bin() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if ((dcalc_intmode != BIN) || (dcalc_mode != PROG)) {
        dcalc_mode = PROG;
        dcalc_stop_entering();
        gui_raw_mode(0);
        max_digits = 32;
        dcalc_intmode = BIN;
        gui_msg("Binary mode");

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
    return 0;
}

int
dcalc_exec_oct() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if ((dcalc_intmode != OCT) || (dcalc_mode != PROG)) {
        dcalc_mode = PROG;
        dcalc_stop_entering();
        dcalc_intmode = OCT;
        gui_raw_mode(0);
        max_digits = 11;
        gui_msg("Octal mode");

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
    return 0;
}

int
dcalc_exec_dec() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if ((dcalc_intmode != DEC) || (dcalc_mode != PROG)) {
        dcalc_mode = PROG;
        dcalc_stop_entering();
        dcalc_intmode = DEC;
        gui_raw_mode(0);
        max_digits = 10;
        gui_msg("Decimal mode");

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
    return 0;
}

int
dcalc_exec_hex() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if ((dcalc_intmode != HEX) || (dcalc_mode != PROG)) {
        dcalc_mode = PROG;
        dcalc_stop_entering();
        dcalc_intmode = HEX;
        gui_raw_mode(0);
        max_digits = 8;
        gui_msg("Hexadecimal mode");

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
    return 0;
}

static
void change_fin_sci_stat(COMMAND c) {
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode != c) {
        char buf[MAX_ENTRY];
        dcalc_mode = c;
        dcalc_stop_entering();
        gui_raw_mode(0);
        max_digits = 20;
        sprintf(buf, "%s mode", (c==FIN)? "Financial": (c==SCI)? "Scientific": "Statistics");
        if (c == FIN) {
            if (dcalc_floatmode != FIXFORMAT)
                dcalc_exec_fix();
            gui_dispnums();
        }
        gui_msg(buf);

        gui_dispnums();
        gui_dispregs();
        gui_print_base();
    }
}

int
dcalc_exec_fin() {
    dcalc_is_inverted();
    change_fin_sci_stat(FIN);
    return 0;
}

int
dcalc_exec_sci() {
    dcalc_is_inverted();
    change_fin_sci_stat(SCI);
    return 0;
}

int
dcalc_exec_stat() {
    dcalc_is_inverted();
    change_fin_sci_stat(STAT);
    return 0;
}

int
dcalc_exec_prog() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode != PROG) {
        dcalc_mode = PROG;
        dcalc_stop_entering();
        gui_raw_mode(0);
        max_digits = 10;
        gui_msg("Programmers mode");
    }

    gui_dispnums();
    gui_dispregs();
    gui_print_base();
    return 0;
}

static void addPrime(long cand, long *primes, int *primeMultiplier, int *numPrimes) {
    if (*numPrimes == 0 ) {
        *numPrimes = 1;
        primes[0] = cand;
        primeMultiplier[0] = 1;
    } else {
        if (primes[*numPrimes - 1] == cand) {
            primeMultiplier[*numPrimes - 1] = primeMultiplier[*numPrimes - 1] + 1;
        } else {
            if (*numPrimes == KMaxPrimes) {
                gui_msg("Too many primes");
                return;
            }
            primes[*numPrimes] = cand;
            primeMultiplier[*numPrimes] = 1;
            (*numPrimes)++;
        }
    }
}

int dcalc_factors_error;
long dcalc_factors(long orig) {
    long        target;
    int         i;
    char        msgbuf[100];
    long        cand, rem, term;
    long        primes[KMaxPrimes];
    int         primeMultiplier[KMaxPrimes], numPrimes;

    dcalc_factors_error = 0;
    if (orig < 0) {
        orig = -orig;
    }

    if (orig < 2) {
        gui_msg("No prime factors");
        dcalc_factors_error = 1;
        return orig;
    }

    target = orig;
    term = sqrt((double) target) + 1;
    numPrimes = 0;
    for (i = 0; i < KMaxPrimes; i++) {
        primes[i] = 0;
        primeMultiplier[i] = 0;
    }

    // check for 2's
    cand = 2;
    while (cand < term) {
        rem = target / cand;
        if (rem * cand == target) {
            target = rem;
            term = sqrt(target) + 1;
            addPrime(cand, primes, primeMultiplier, &numPrimes);
        } else
            break;
    }

    // check odd numbers
    cand = 3;
    while (cand < term) {
        rem = target / cand;
        if (rem * cand == target) {
            target = rem;
            term = sqrt(target)+1;
            addPrime(cand, primes, primeMultiplier, &numPrimes);
        } else
            cand += 2;
    }
    if (target > 1)
        addPrime(target, primes, primeMultiplier, &numPrimes);

    if (numPrimes == 1 && primeMultiplier[0] == 1) {
        sprintf(msgbuf, "%ld is prime", orig);
    } else {
        sprintf(msgbuf,"%ld=", orig);
        for (i=0, target = 1; i < numPrimes; i++) {
            char *m = msgbuf;

            m += strlen(msgbuf);
#ifdef DEBUG
            {
                int j;
                for (j = 0; j < primeMultiplier[i]; j++)
                    target *= primes[i];
            }
#endif
            sprintf(m, "%ld", primes[i]);
            if (primeMultiplier[i] > 1) {
                m += strlen(m);
                sprintf(m, "^%d", primeMultiplier[i]);
            }
            if (i < numPrimes - 1) {
                strcat(m, "*");
            }
        }
    }
    gui_msg(msgbuf);

#ifdef DEBUG
    if (target != orig) { // ERRORCHECKING IN BETA ONLY!!!
        gui_msg("Error!");
    }
#endif
    return 0;
}

/* static void arith_int(COMMAND c) { */
int
dcalc_exec_chs() {
    long        tempx;
    double      tempxf;
    char        *c_ptr, c_buf[MAX_ENTRY];

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG) {
        if (dcalc_entering) {
            c_ptr = dcalc_inbuf;
            dcalc_cpy(c_buf, c_ptr, MAX_ENTRY);
            *c_ptr = ASCIIZ;
            c_ptr = c_buf;
            if (*c_ptr == '-')
                c_ptr++;
            else
                strcat(dcalc_inbuf, "-");
            strcat(dcalc_inbuf, c_ptr);
            inptr = &dcalc_inbuf[strlen(dcalc_inbuf)];
        } else {
            tempx = dcalc_pop();
            dcalc_push(-tempx);
        }
    } else {
        if (dcalc_entering) {
            if ((c_ptr = strchr(dcalc_inbuf, 'E')))
                c_ptr++;
            else
                c_ptr = dcalc_inbuf;
            dcalc_cpy(c_buf, c_ptr, MAX_ENTRY);
            *c_ptr = ASCIIZ;
            c_ptr = c_buf;
            if (*c_ptr == '-')
                c_ptr++;
            else
                strcat(dcalc_inbuf, "-");
            strcat(dcalc_inbuf, c_ptr);
            inptr = &dcalc_inbuf[strlen(dcalc_inbuf)];
        } else {
            tempxf = dcalc_popf();
            dcalc_pushf(-tempxf);
        }
    }
    gui_dispnums();
    return 0;
}

/* operations on X only */
int
dcalc_exec_not() {
    long        tempx;
    double      tempxf;

    dcalc_is_inverted();
    save_x();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempx = ~tempx;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_shiftl() {
    long        tempx;
    double      tempxf;

    if (dcalc_is_inverted())
        return dcalc_exec_shiftr();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempx = tempx << 1;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_shiftr() {
    long        tempx;
    double      tempxf;

    if (dcalc_is_inverted())
        return dcalc_exec_shiftl();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempx = tempx >> 1;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_primef() {
    long        tempx;
    double      tempxf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        dcalc_factors(tempx);
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_sqr() {
    long        tempx;
    double      tempxf;

    if (dcalc_is_inverted())
        return dcalc_exec_root();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempx = tempx * tempx;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        tempxf *= tempxf;
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_root() {
    double      tempxf;

    if (dcalc_is_inverted())
        return dcalc_exec_sqr();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempxf = dcalc_pop();
    } else
        tempxf = dcalc_popf();

    tempxf = sqrt(tempxf);
    if (dcalc_mode == PROG)
        dcalc_push((long)(tempxf+0.5));
    else
        dcalc_pushf(tempxf);

    gui_dispnums();
    return 0;
}

int
dcalc_exec_cube() {
    long        tempx;
    double      tempxf;

    if (dcalc_is_inverted())
        return dcalc_exec_croot();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempx = tempx * tempx * tempx;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        tempxf = tempxf * tempxf * tempxf;
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_croot() {
    double      tempxf;

    if (dcalc_is_inverted())
        return dcalc_exec_cube();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempxf = dcalc_pop();
    } else
        tempxf = dcalc_popf();

    tempxf = pow(tempxf, 1.0 / 3.0);
    if (dcalc_mode == PROG)
        dcalc_push((long)(tempxf+0.5));
    else
        dcalc_pushf(tempxf);

    gui_dispnums();
    return 0;
}

int
dcalc_exec_reci() {
    long        tempx;
    double      tempxf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempx = 0;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        if (tempxf == 0.0)
            gui_msg(div_by_zero);
        else {
            tempxf = 1.0 / tempxf;
        }
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

/* X & Y operations */
int
dcalc_exec_plus() {
    long        tempx, tempy;
    double      tempxf, tempyf;

    if (dcalc_is_inverted())
        return dcalc_exec_minus();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        tempx = tempx + tempy;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        tempxf = tempxf + tempyf;
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

/* case '-': */
int
dcalc_exec_minus() {
    long        tempx, tempy;
    double      tempxf, tempyf;

    if (dcalc_is_inverted())
        return dcalc_exec_plus();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        tempx = tempy - tempx;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        tempxf = tempyf - tempxf;
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

/* case '*': */
int
dcalc_exec_mult() {
    long        tempx, tempy;
    double      tempxf, tempyf;

    if (dcalc_is_inverted())
        return dcalc_exec_divide();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        tempx = tempx * tempy;
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        tempxf = tempxf * tempyf;
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

/* case '/': */
int
dcalc_exec_divide() {
    long        tempx, tempy;
    double      tempxf, tempyf;

    if (dcalc_is_inverted())
        return dcalc_exec_mult();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        if (tempx == 0) {
            dcalc_push(tempy);
            gui_msg(div_by_zero);
        } else {
            if (tempx != 0)
                tempx = tempy / tempx;
            else
                tempx = 0;
        }
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        if (tempxf == 0.0) {
            dcalc_pushf(tempyf);
            gui_msg(div_by_zero);
        } else {
            tempxf = tempyf / tempxf;
        }
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

long dcalc_xor(long x, long y) {
    return(x ^ y);
}

/* case '^': */
int
dcalc_exec_xor() {
    long        tempx, tempy;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        tempx ^= tempy;
        dcalc_push(tempx);
    } else {
    }
    gui_dispnums();
    return 0;
}

long dcalc_or(long x, long y) {
    return(x | y);
}

/* case '|': */
int
dcalc_exec_or() {
    long        tempx, tempy;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        tempx |= tempy;
        dcalc_push(tempx);
    } else {
    }
    gui_dispnums();
    return 0;
}

long dcalc_and(long x, long y) {
    printf("%ld %ld %ld\n", x, y, x & y);
    return(x & y);
}

/* case '&': */
int
dcalc_exec_and() {
    long        tempx, tempy;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        tempx &= tempy;
        dcalc_push(tempx);
    } else {
    }
    gui_dispnums();
    return 0;
}

long dcalc_shiftl(long x, long y) {
    return x << y;
}

long dcalc_shiftr(long x, long y) {
    return x >> y;
}

/* case '<': */
int
dcalc_exec_shiftyl() {
    long        tempx, tempy;

    if (dcalc_is_inverted())
        return dcalc_exec_shiftyr();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        tempy <<= tempx;
        tempx = tempy;
        dcalc_push(tempx);
    } else {
    }
    gui_dispnums();
    return 0;
}

/* case '>': */
int
dcalc_exec_shiftyr() {
    long        tempx, tempy;

    if (dcalc_is_inverted())
        return dcalc_exec_shiftyl();

    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        tempy >>= tempx;
        tempx = tempy;
        dcalc_push(tempx);
    } else {
    }
    gui_dispnums();
    return 0;
}

long dcalc_mod(long x, long y) {
    return(x % y);
}

int
dcalc_exec_modulus() {
    long        tempx, tempy;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        if (tempx == 0) {
            dcalc_push(tempx);
            gui_msg(div_by_zero);
        } else {
            tempy = dcalc_pop();
            tempy %= tempx;
            tempx = tempy;
            dcalc_push(tempx);
        }
    } else {
    }
    gui_dispnums();
    return 0;
}

long dcalc_power(long x, long n) {
    int i, p;
    switch (x) {
    case 0:
        return 0;
    case 1:
        return 1;
    default:
        switch (n) {
        case 0:
            return 1;
        case 1:
            return x;
        default:
            if (n < 0 || n > 32) {
                gui_msg("Overflow");
                return 0;
            }
            p = 1;
            for (i = 1; i <= n; ++i)
                p *= x;
            return p;
        }
    }
}

int
dcalc_exec_ytox() {
    long        tempx, tempy;
    double      tempxf, tempyf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        if (tempx < 0) {
            gui_msg("X cannot be < 0");
            dcalc_push(tempy);
        } else
            tempx = dcalc_power(tempy, tempx);
        dcalc_push(tempx);
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        tempxf = pow(tempyf, tempxf);
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_percent() {
    double      tempxf, tempyf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        dcalc_pushf(tempyf); /* dcalc_push y back on stack for future ops */
        if (dcalc_is_inverted()) {
            if (tempxf == 0.0) {
                gui_msg(div_by_zero);
                dcalc_pushf(tempxf);
            } else {
                tempxf = tempyf * 100.0 / tempxf;
            }
        } else {
            tempxf = tempxf * tempyf / 100.0;
        }
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

int dcalc_percentch_error;
double dcalc_percentch(double tempxf, double tempyf) {
    dcalc_percentch_error = 0;
    if (dcalc_is_inverted()) {
        if (tempxf == 0.0) {
            gui_msg(div_by_zero);
            dcalc_percentch_error = 1;
        } else {
            tempxf = tempyf * (1.0 + tempxf / 100.0);
        }
    } else {
        if (tempxf == 0.0) {
            gui_msg(div_by_zero);
            dcalc_percentch_error = 1;
        } else {
            tempxf = 100.0 * (tempxf - tempyf) / tempyf;
        }
    }
    return tempxf;
}

int dcalc_pluspercent_error;
double dcalc_pluspercent(double tempxf, double tempyf) {
    dcalc_pluspercent_error = 0;
    dcalc_invert = !dcalc_invert;
    tempxf = dcalc_percentch(tempxf, tempyf);
    if (dcalc_percentch_error)
        dcalc_pluspercent_error = TRUE;
    return tempxf;
}

int
dcalc_exec_percentch() {
    double      tempxf, tempyf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    if (dcalc_mode == PROG) {
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        dcalc_pushf(tempyf); /* dcalc_push y back on stack for future ops */
        tempxf = dcalc_percentch(tempxf, tempyf);
        dcalc_pushf(tempxf);
    }
    gui_dispnums();
    return 0;
}

double dcalc_sin(double x) {
    double retval;
    
    if (dcalc_is_inverted()) {
        retval = asin(x);
        if (dcalc_degree)
            retval *= r_to_d;
    } else {
        if (dcalc_degree)
            retval = sin(d_to_r * x);
        else
            retval = sin(x);
    }
    return retval;
}

double dcalc_asin(double x) {
    double retval;
    dcalc_invert = TRUE;
    retval = dcalc_sin(x);
    dcalc_invert = FALSE;
    return retval;
}

int
dcalc_exec_sin() {
    double      tempxf;
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    dcalc_pushf(dcalc_sin(tempxf));
    gui_dispnums();
    return 0;
}

int
dcalc_exec_asin() {
    dcalc_invert = TRUE;
    return(dcalc_exec_sin());
}

double dcalc_cos(double x) {
    double retval;
    
    if (dcalc_is_inverted()) {
        retval = acos(x);
        if (dcalc_degree)
            retval *= r_to_d;
    } else {
        if (dcalc_degree)
            retval = cos(d_to_r * x);
        else
            retval = cos(x);
    }
    return retval;
}

double dcalc_acos(double x) {
    double retval;
    dcalc_invert = TRUE;
    retval = dcalc_cos(x);
    dcalc_invert = FALSE;
    return retval;
}

int
dcalc_exec_cos() {
    double      tempxf;
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    dcalc_pushf(dcalc_cos(tempxf));
    gui_dispnums();
    return 0;
}

int
dcalc_exec_acos() {
    dcalc_invert = TRUE;
    return(dcalc_exec_cos());
}

double dcalc_tan(double x) {
    double retval;
    
    if (dcalc_is_inverted()) {
        retval = atan(x);
        if (dcalc_degree)
            retval *= r_to_d;
    } else {
        if (dcalc_degree)
            retval = tan(d_to_r * x);
        else
            retval = tan(x);
    }
    return retval;
}

double dcalc_atan(double x) {
    double retval;
    dcalc_invert = TRUE;
    retval = dcalc_tan(x);
    dcalc_invert = FALSE;
    return retval;
}

int
dcalc_exec_tan() {
    double      tempxf;
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    dcalc_pushf(dcalc_tan(tempxf));
    gui_dispnums();
    return 0;
}

int
dcalc_exec_atan() {
    dcalc_invert = TRUE;
    return(dcalc_exec_tan());
}

double dcalc_sinh(double x) {
    double retval;

    if (dcalc_is_inverted()) {
        retval = log(x+sqrt(x*x + 1));
        if (dcalc_degree)
            retval *= r_to_d;
    } else {
        if (dcalc_degree)
            x *= d_to_r;
        retval = 0.5*(exp(x) - exp(-x));
    }
    return retval;
}

double dcalc_asinh(double x) {
    double retval;
    dcalc_invert = TRUE;
    retval = dcalc_sinh(x);
    return retval;
}

int
dcalc_exec_sinh() {
    double      tempxf;
    if (dcalc_mode == PROG)
        return 9;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    dcalc_pushf(dcalc_sinh(tempxf));
    gui_dispnums();
    return 0;
}

int
dcalc_exec_asinh() {
    dcalc_invert = TRUE;
    return(dcalc_exec_sinh());
}

double dcalc_cosh(double x) {
    double retval = 0.0;

    if (dcalc_is_inverted()) {
        if (x < 1) {
            gui_msg("Illegal value");
            dcalc_pushf(x);
        } else {
            retval = log(x+sqrt(x*x - 1));
        if (dcalc_degree)
            retval *= r_to_d;
        }
    } else {
        if (dcalc_degree)
            x *= d_to_r;
        retval = 0.5*(exp(x) + exp(-x));
    }

    return retval;
}

double dcalc_acosh(double x) {
    double retval;
    dcalc_invert = TRUE;
    retval = dcalc_cosh(x);
    return retval;
}

int
dcalc_exec_cosh() {
    double      tempxf;
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    tempxf = dcalc_cosh(tempxf);
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_acosh() {
    dcalc_invert = TRUE;
    return(dcalc_exec_cosh());
}

int dcalc_tanh_error;
double dcalc_tanh(double x) {
    double retval = x;

    dcalc_tanh_error = 0;
    if (dcalc_is_inverted()) {
        if (x == 1) {
            gui_msg("Illegal value");
            dcalc_tanh_error = 1;
        } else {
            retval = 0.5*log((x+1)/(1-x));
            if (dcalc_degree)
                retval *= r_to_d;
        }
    } else {
        double p;
        if (dcalc_degree)
            x *= d_to_r;
        p = exp(2*x);
        retval = (p-1)/(p+1);
    }

    return retval;
}

double dcalc_atanh(double x) {
    double retval;
    dcalc_invert = TRUE;
    retval = dcalc_tanh(x);
    return retval;
}

int
dcalc_exec_tanh() {
    double      tempxf;
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    tempxf = dcalc_tanh(tempxf);
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_atanh() {
    dcalc_invert = TRUE;
    return(dcalc_exec_tanh());
}

int
dcalc_exec_etox() {
    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted())
        return dcalc_exec_loge();

    dcalc_last_was_fin_key = 0;
    save_x();
    dcalc_pushf(exp(dcalc_popf()));
    gui_dispnums();
    return 0;
}

int
dcalc_exec_loge() {
    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted())
        return dcalc_exec_etox();

    dcalc_last_was_fin_key = 0;
    save_x();
    dcalc_pushf(log(dcalc_popf()));
    gui_dispnums();
    return 0;
}

int
dcalc_exec_10tox() {
    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted())
        return dcalc_exec_log10();

    dcalc_last_was_fin_key = 0;
    save_x();
    dcalc_pushf(pow(10.0, dcalc_popf()));
    gui_dispnums();
    return 0;
}

int
dcalc_exec_log10() {
    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted())
        return dcalc_exec_10tox();

    dcalc_last_was_fin_key = 0;
    save_x();
    dcalc_pushf(log10(dcalc_popf()));
    gui_dispnums();
    return 0;
}

double dcalc_frc(double x) {
    long is_negative = 0;
    
    if (x < 0.0) {
        is_negative = 1;
        x = -x;
    }
    x = x - (double) floor(x);
    if (is_negative)
        x = -x;
    return x;
}

int
dcalc_exec_frc() {
    double      tempxf;

    dcalc_is_inverted();
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    tempxf = dcalc_frc(tempxf);
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

double dcalc_int(double x) {
    long is_negative = 0;
    
    if (dcalc_mode == PROG)
        return 0;
    dcalc_is_inverted();
    if (x < 0.0) {
        is_negative = 1;
        x = -x;
    }
    x = (double) floor(x);
    if (is_negative)
        x = -x;
    return x;
}

int
dcalc_exec_int() {
    double      tempxf;

    dcalc_is_inverted();
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    tempxf = dcalc_int(tempxf);
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_hms() {
    double      tempxf;
    int sign = 0;
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();

    if (tempxf < 0.0) {
        sign = 1;
        tempxf = -tempxf;
    }

    if (dcalc_is_inverted()) { /* convert h.mmss to hours */
        long h, m, s;

        h = tempxf;
        tempxf = (tempxf - h) * 100 + 1E-10;
        m = tempxf;
        tempxf = (tempxf - m) * 100 + 1E-10;
        s = tempxf;
        tempxf = h + m / 60.0 + s / 3600.0;
        gui_msg("HH.MMSS -> hours");
    } else { /* convert hours to h.mmss */
        long h, m, s;

        h = tempxf;
        tempxf = (tempxf - h) * 60  + 1E-10;
        m = tempxf;
        s = (tempxf - m) * 60 + 1E-10;
        tempxf = h + m / 100.0 + s / 10000.0;
        if (dcalc_decplaces < 4)
            dcalc_decplaces = 4;
        if (dcalc_floatmode != FIXFORMAT)
            dcalc_exec_fix();
        gui_msg("hours -> HH.MMSS");
    }
    if (sign)
        tempxf = -tempxf;
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_dtor() {
    double      tempxf;
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();

    if (dcalc_is_inverted()) {
        tempxf *= r_to_d;
        gui_msg("radians to degrees");
    } else {
        tempxf *= d_to_r;
        gui_msg("degrees to radians");
    }
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_rtod() {
    dcalc_invert = ~dcalc_invert;
    return(dcalc_exec_dtor());
}

int
dcalc_exec_rtop() {
    double      tempxf, tempyf;
    if (dcalc_mode == PROG)
        return 0;

    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();

    tempyf = dcalc_popf();
    if (dcalc_is_inverted()) { /*  convert (r, theta) to (x, y) */
        double theta, r;

        theta = tempyf;
        if (dcalc_degree)
            theta = d_to_r * theta;

        r = tempxf;
        tempyf = r * sin(theta);
        tempxf = r * cos(theta);
        gui_msg("polar to rectangular");
    } else { /*  convert (x, y) to (r, theta) */
        double theta, r;

        r = sqrt(tempxf * tempxf + tempyf * tempyf);
        theta = atan2(tempyf, tempxf);
        if (dcalc_degree)
            theta = r_to_d * theta;
        tempyf = theta;
        tempxf = r;
        gui_msg("rectangular to polar");
    }
    dcalc_pushf(tempyf);
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_ptor() {
    dcalc_invert = ~dcalc_invert;
    return(dcalc_exec_rtop());
}

static double fact(double from, double to) {
    double total, n, m;

    n = floor(from);
    m = floor(to);
    if (n <= 0 || m <= 0)
        return(1.0);

    total = 1.0;
    while (n > m) {
        total = total * n;
        n = n - 1;
    }
    return total;
}

double dcalc_factorial(double n) {
    return fact(n, 1.0);
}

/* static void summation(COMMAND c) { */

int
dcalc_exec_fact() {
    double      tempxf;

    if (dcalc_mode == PROG)
        return 0;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    save_x();
    tempxf = dcalc_popf();
    if (tempxf < 0) {
        gui_msg("Illegal value");
        dcalc_pushf(tempxf);
    } else {
        dcalc_pushf(dcalc_factorial(tempxf));
        gui_dispnums();

    }
    return 0;
}

int
dcalc_exec_sum() {
    double      tempxf, tempyf;

    if (dcalc_is_inverted())
        return dcalc_exec_sumr();

    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;

    save_x();
    tempxf = dcalc_popf();
    tempyf = dcalc_popf();
    dcalc_pushf(tempyf);
    dcalc_pushf(tempxf);
    dcalc_regf[STAT_NUM_REG]++;
    dcalc_regf[STAT_SUMX_REG] += tempxf;
    dcalc_regf[STAT_SUMX2_REG] += tempxf * tempxf;
    dcalc_regf[STAT_SUMY_REG] += tempyf;
    dcalc_regf[STAT_SUMY2_REG] += tempyf * tempyf;
    dcalc_regf[STAT_SUMXY_REG] += tempxf * tempyf;
    dcalc_pushf(dcalc_regf[STAT_NUM_REG]);
    gui_dispreg(STAT_NUM_REG);
    gui_dispreg(STAT_SUMX_REG);
    gui_dispreg(STAT_SUMY_REG);
    gui_dispreg(STAT_SUMX2_REG);
    gui_dispreg(STAT_SUMY2_REG);
    gui_dispreg(STAT_SUMXY_REG);
    gui_dispnums();
    gui_msg("X & Y accumulated");
    return 0;
}

int
dcalc_exec_sumr() {
    double      tempxf, tempyf;

    if (dcalc_is_inverted())
        return dcalc_exec_sum();

    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_regf[STAT_NUM_REG] > 0) {
        save_x();
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        dcalc_pushf(tempyf);
        dcalc_pushf(tempxf);
        dcalc_regf[STAT_NUM_REG]--;
        dcalc_regf[STAT_SUMX_REG] -= tempxf;
        dcalc_regf[STAT_SUMX2_REG] -= tempxf * tempxf;
        dcalc_regf[STAT_SUMY_REG] -= tempyf;
        dcalc_regf[STAT_SUMY2_REG] -= tempyf * tempyf;
        dcalc_regf[STAT_SUMXY_REG] -= tempxf * tempyf;
        dcalc_pushf(dcalc_regf[STAT_NUM_REG]);
        gui_dispreg(STAT_NUM_REG);
        gui_dispreg(STAT_SUMX_REG);
        gui_dispreg(STAT_SUMY_REG);
        gui_dispreg(STAT_SUMX2_REG);
        gui_dispreg(STAT_SUMY2_REG);
        gui_dispreg(STAT_SUMXY_REG);
        gui_dispnums();
        gui_msg("X & Y decremented");
    }
    return 0;
}

int
dcalc_exec_sum0() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;
    save_x();
    dcalc_regf[STAT_NUM_REG] = 0.0;
    dcalc_regf[STAT_SUMX_REG]  = 0.0;
    dcalc_regf[STAT_SUMX2_REG]  = 0.0;
    dcalc_regf[STAT_SUMY_REG]  = 0.0;
    dcalc_regf[STAT_SUMY2_REG]  = 0.0;
    dcalc_regf[STAT_SUMXY_REG]  = 0.0;
    gui_dispreg(STAT_NUM_REG);
    gui_dispreg(STAT_SUMX_REG);
    gui_dispreg(STAT_SUMY_REG);
    gui_dispreg(STAT_SUMX2_REG);
    gui_dispreg(STAT_SUMY2_REG);
    gui_dispreg(STAT_SUMXY_REG);
    gui_dispnums();
    gui_msg("Accumulator registers cleared");
    return 0;
}

int
dcalc_exec_nstat() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;
    dcalc_pushf(dcalc_regf[STAT_NUM_REG]);
    gui_msg("Number of entries");
    gui_dispnums();
    return 0;
}

int
dcalc_exec_means() { /* mean of x & y */
    double      tempxf, tempyf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_regf[STAT_NUM_REG] == 0.0)
        gui_msg("Nothing accumulated yet!");
    else {
        tempyf = dcalc_regf[STAT_SUMY_REG] / dcalc_regf[STAT_NUM_REG];
        dcalc_pushf(tempyf);
        tempxf = dcalc_regf[STAT_SUMX_REG] / dcalc_regf[STAT_NUM_REG];
        dcalc_pushf(tempxf);
        gui_dispnums();
        gui_msg("Mean of X's and of Y's");
    }
    return 0;
}

int
dcalc_exec_mean() {
    double      tempxf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_regf[STAT_NUM_REG] == 0.0)
        gui_msg(div_by_zero);
    else {
        tempxf = dcalc_regf[STAT_SUMX_REG] / dcalc_regf[STAT_NUM_REG];
        dcalc_pushf(tempxf);
        gui_dispnums();
        gui_msg("Mean of X's");
    }
    return 0;
}

int
dcalc_exec_meany() {
    double tempyf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_regf[STAT_NUM_REG] == 0.0)
        gui_msg(div_by_zero);
    else {
        tempyf = dcalc_regf[STAT_SUMY_REG] / dcalc_regf[STAT_NUM_REG];
        dcalc_pushf(tempyf);
        gui_dispnums();
        gui_msg("Mean of Y's");
    }
    return 0;
}

int
dcalc_exec_s_dev() {
    double      tempxf, tempyf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;

    /*  s.dev = sqrt((sum:(x^2) - (sum:(x))^2 /n) / n-1) */
    if (dcalc_regf[STAT_NUM_REG] == 0.0)
        gui_msg(div_by_zero);
    else {
        tempyf = sqrt((dcalc_regf[STAT_SUMY2_REG] - dcalc_regf[STAT_SUMY_REG]*dcalc_regf[STAT_SUMY_REG]/dcalc_regf[STAT_NUM_REG])/(dcalc_regf[STAT_NUM_REG] - 1));
        dcalc_pushf(tempyf);
        tempxf = sqrt((dcalc_regf[STAT_SUMX2_REG] - dcalc_regf[STAT_SUMX_REG]*dcalc_regf[STAT_SUMX_REG]/dcalc_regf[STAT_NUM_REG])/(dcalc_regf[STAT_NUM_REG] - 1));
        dcalc_pushf(tempxf);
        gui_dispnums();
        gui_msg("Std Dev of X's & Y's");
    }
    return 0;
}

int dcalc_perm_error;
double dcalc_perm(double tempyf, double tempxf) {
    double result = 0.0;
    dcalc_perm_error = 0;
    if (tempxf < 0 || tempyf < 0 || tempyf < tempxf) {
        gui_msg("Bad values for permutation");
        dcalc_perm_error = 1;
    } else {
        double n, r;

        n = floor(tempyf+.5);
        r = floor(tempxf+.5);
        result = fact(n, n - r);
        gui_dispnums();
    }
    return result;
}

int
dcalc_exec_perm() {
    double      tempxf, tempyf, result;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;
    save_x();
    tempxf = dcalc_popf();
    tempyf = dcalc_popf();
    result = dcalc_perm(tempyf, tempxf);
    if (dcalc_perm_error) {
        dcalc_pushf(tempyf);
        dcalc_pushf(tempxf);
    } else {
        gui_msg("Permutation");
        dcalc_pushf(result);
        gui_dispnums();
    }
    return 0;
}

int dcalc_comb_error;
double dcalc_comb(double tempyf, double tempxf) {
    double result = 0.0;
    dcalc_comb_error = 0;
    if (tempxf < 0 || tempyf < 0 || tempyf < tempxf) {
        dcalc_comb_error = 1;
        gui_msg("Bad values for combination");
    } else {
        double n, r;

        n = floor(tempyf+.5);
        r = floor(tempxf+.5);
        if (n-r > r)
            result = fact(n, n-r) / dcalc_factorial(r);
        else
            result = fact(n, r) / dcalc_factorial(n-r);
    }
    return result;
}

int
dcalc_exec_comb() {
    double      tempxf, tempyf, result;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;
    save_x();
    tempxf = dcalc_popf();
    tempyf = dcalc_popf();
    result = dcalc_comb(tempyf, tempxf);
    if (dcalc_comb_error) {
        dcalc_pushf(tempyf);
        dcalc_pushf(tempxf);
    } else {
        gui_msg("Combinations");
        dcalc_pushf(result);
        gui_dispnums();
    }
    return 0;
}

int
dcalc_exec_lr() {
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;
    if (dcalc_is_inverted()) {
        char buf[MAX_ENTRY];

        sprintf(buf, "Linear Regression formula is: Y= %f*X + %f",
                dcalc_regf[STAT_SLOPE], dcalc_regf[STAT_Y_INT]);
        gui_msg(buf);
    } else {
        if (dcalc_regf[STAT_NUM_REG] < 2)
            gui_msg("Not enough data points");
        else {
            double detx, dety, a, b, r2;

            detx = dcalc_regf[STAT_SUMX2_REG]*dcalc_regf[STAT_NUM_REG] -
                dcalc_regf[STAT_SUMX_REG]*dcalc_regf[STAT_SUMX_REG];
            dety = dcalc_regf[STAT_SUMY2_REG]*dcalc_regf[STAT_NUM_REG] -
                dcalc_regf[STAT_SUMY_REG]*dcalc_regf[STAT_SUMY_REG];
            a = (dcalc_regf[STAT_SUMXY_REG]*dcalc_regf[STAT_NUM_REG] -
                 dcalc_regf[STAT_SUMX_REG]*dcalc_regf[STAT_SUMY_REG])/detx;
            b = (dcalc_regf[STAT_SUMX2_REG]*dcalc_regf[STAT_SUMY_REG] -
                 dcalc_regf[STAT_SUMX_REG]*dcalc_regf[STAT_SUMXY_REG])/detx;
            r2 = a*a*detx/dety;
            dcalc_regf[STAT_SLOPE] = a;
            dcalc_regf[STAT_Y_INT] = b;
            dcalc_regf[STAT_R2] = b;
            dcalc_pushf(r2);
            dcalc_pushf(b);
            dcalc_pushf(a);
            gui_dispnums();
            gui_msg("Slope (X), Y-intersect (Y) & correlation (Z) calculated");
        }
    }
    return 0;
}

int
dcalc_exec_calcy() {
    double      tempxf, tempyf;

    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        return 0;
    if (dcalc_is_inverted()) {
        tempyf = dcalc_popf();
        tempxf = (tempyf -  dcalc_regf[STAT_Y_INT]) / dcalc_regf[STAT_SLOPE];
        gui_msg("X calculated from Y");
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_regf[STAT_SLOPE] * tempxf + dcalc_regf[STAT_Y_INT];
        gui_msg("Y calculated from X");
    }
    dcalc_pushf(tempyf);
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

/* static void financial(COMMAND c) { */
    /*
     * Financial function register allocation ...
     *
     * 5 n
     * 6 interest
     * 7 present value
     * 8 payment
     * 9 future value
     *
     * Formulae are :
     *
     * 0 = PVAL + (1 + I.S) PMT (1 - (1+I)**-N ) / I + FVAL * (1+I)**-N

     New Formula from psion version:
     y=Fval + Pval*(1 + i)**n + (1 + s*i)*pmt*((1 + i)**n - 1)/i
     y'=pmt*(1 + (1 + i)^(n-1) * n * (pval* i^2 / pmt - (1+i)/n + i + s*i^2)) / i^2
     *
     * where:
     *
     * N    = number of periods
     * PVAL = present value
     * I    = interest rate (as a fraction - user sees a percent)
     * PMT  = payment
     * FVAL = future value
     * S    = payment dcalc_mode factor. 0 for payment at end of period, 1 for start.
     */

static double
calc_fval(double pval,
          double n,
          double pmt,
          double interest) {

    double tempxf, tempyf;

    if (fabs(interest) < 1.0e-13) {
        tempxf = -(pval + (n + dcalc_finPayAt0) * pmt);
    } else {
        tempyf = pow(1.0 + interest, n);
#if 0
        if (errno) {
            printf("Error in calc_fval: pow(): interest=%.4f%% n=%.2f\n", interest*100.0, n);
            exit(1);
        }
#endif
        tempxf = -(pval*tempyf + (1 + dcalc_finPayAt0*interest)*pmt*(tempyf - 1.0)/interest);
    }
    return tempxf;
}

static double
calc_pval(double fval,
          double n,
          double pmt,
          double interest) {

    double tempxf, tempyf;

    if (fabs(interest) < 1.0e-13) {
        tempxf = -(fval + (n + dcalc_finPayAt0)*pmt);
    } else {
        tempyf = pow(1.0 + interest, -n);
#if 0
        if (errno) {
            printf("Error in calc_pval: pow(): interest=%.4f%% n=%.2f\n", interest*100.0, n);
            exit(1);
        }
#endif
        tempxf = -(fval*tempyf + (1 + dcalc_finPayAt0*interest)*pmt*(1.0 - tempyf)/interest);
    }
    return tempxf;
}

static double
calc_n(double fval,
       double pval,
       double pmt,
       double interest) {

    double tempxf, tempyf, den;

    tempxf = 0.0;
    if (fabs(interest) < 1.0e-13) {
        if (fabs(pmt) < 1.0e-13) {
            gui_msg(div_by_zero);
        } else {
            tempxf = -(fval + pval)/pmt - dcalc_finPayAt0;
        }
    } else {
        tempyf = 1.0 + interest*dcalc_finPayAt0;
        den = pval + tempyf*pmt/interest;
        if (fabs(den) < 1.0e-13) {
            gui_msg(div_by_zero);
        } else {
            tempxf = log((tempyf*pmt/interest - fval)/den)/log(1.0 + interest);
        }
    }
    return tempxf;
}

static double
calc_pmt(double fval,
         double pval,
         double n,
         double interest) {

    double tempxf, tempyf;

    if (fabs(interest) < 1.0e-13) {
        tempxf = -(fval + pval)/(n + dcalc_finPayAt0);
    } else {
        tempyf = pow(1.0 + interest, n);
#if 0
        if (errno) {
            printf("Error in calc_pmt: pow(): interest=%.4f%% n=%.2f\n", interest*100.0, n);
            exit(1);
        }
#endif
        tempxf = -interest*(fval + pval*tempyf)/((1.0 + interest*dcalc_finPayAt0)*(tempyf - 1.0));
    }
    return tempxf;
}

int
dcalc_exec_pval() {
    double      tempxf;
    double      interest, n, pmt, fval;

    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted()) {
        dcalc_pushf(dcalc_regf[FIN_PVAL_REG]);
        gui_dispnums();
        gui_msg("Present value recalled");
    } else if (!dcalc_last_was_fin_key) {
        save_x();
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
        dcalc_regf[FIN_PVAL_REG] = tempxf;
        gui_dispreg(FIN_PVAL_REG);
        gui_dispnums();
        dcalc_last_was_fin_key = TRUE;
        gui_msg("Present value entered");
    } else {
        n = dcalc_regf[FIN_NUM_REG];
        interest = dcalc_regf[FIN_INT_REG] / 100.0;
        if (n <= 0.0) {
            gui_msg("# payments must be > 0");
            return 0;
        }
        if (interest < 0.0) {
            gui_msg("Interest must be >= 0");
            return 0;
        }
        pmt = dcalc_regf[FIN_PMT_REG];
        fval = dcalc_regf[FIN_FVAL_REG];
        tempxf = calc_pval(fval, n, pmt, interest);
        dcalc_pushf(tempxf);
        dcalc_regf[FIN_PVAL_REG] = tempxf;
        gui_dispreg(FIN_PVAL_REG);
        gui_dispnums();
        gui_msg("Present value computed");
    }
    return 0;
}

int
dcalc_exec_fval() {
    double      tempxf;
    double      interest, n, pval, pmt;

    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted()) {
        dcalc_pushf(dcalc_regf[FIN_FVAL_REG]);
        gui_dispnums();
        gui_msg("Future value recalled");
    } else if (!dcalc_last_was_fin_key) {
        save_x();
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
        dcalc_regf[FIN_FVAL_REG] = tempxf;
        gui_dispreg(FIN_FVAL_REG);
        gui_dispnums();
        dcalc_last_was_fin_key = TRUE;
        gui_msg("Final value entered");
    } else {
        n = dcalc_regf[FIN_NUM_REG];
        interest = dcalc_regf[FIN_INT_REG] / 100.0;
        if (n <= 0.0) {
            gui_msg("# payments must be > 0");
            return 0;
        }
        if (interest < 0.0) {
            gui_msg("Interest must be >= 0");
            return 0;
        }
        pval = dcalc_regf[FIN_PVAL_REG];
        pmt = dcalc_regf[FIN_PMT_REG];
        tempxf = calc_fval(pval, n, pmt, interest);
        dcalc_pushf(tempxf);
        dcalc_regf[FIN_FVAL_REG] = tempxf;
        gui_dispreg(FIN_FVAL_REG);
        gui_dispnums();
        gui_msg("Final value calculated");
    }
    return 0;
}

int
dcalc_exec_pmt() {
    double      tempxf;
    double      interest, n, pval, fval;

    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted()) {
        dcalc_pushf(dcalc_regf[FIN_PMT_REG]);
        gui_dispnums();
        gui_msg("Payment recalled");
    } else if (!dcalc_last_was_fin_key) {
        save_x();
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
        dcalc_regf[FIN_PMT_REG] = tempxf;
        gui_dispreg(FIN_PMT_REG);
        gui_dispnums();
        dcalc_last_was_fin_key = TRUE;
        gui_msg("Payment entered");
    } else {
        n = dcalc_regf[FIN_NUM_REG];
        if (n <= 0.0) {
            gui_msg("# payments must be > 0");
            return 0;
        }
        interest = dcalc_regf[FIN_INT_REG] / 100.0;
        if (interest < 0.0) {
            gui_msg("Interest must be >= 0");
            return 0;
        }
        pval = dcalc_regf[FIN_PVAL_REG];
        fval = dcalc_regf[FIN_FVAL_REG];
        tempxf = calc_pmt(fval, pval, n, interest);
        dcalc_pushf(tempxf);
        dcalc_regf[FIN_PMT_REG] = tempxf;
        gui_dispreg(FIN_PMT_REG);
        gui_dispnums();
        gui_msg("Payment calculated");
    }
    return 0;
}

static double
newton_raphson_interest(double fval,
                        double pval,
                        double n,
                        double pmt,
                        double guess,
                        double max_guess,
                        int *converged) {

    double epsilon, last_epsilon, fy, dy, tempyf, intst;
    int count, epsilon_increasing;

    count = epsilon_increasing = 0;
    last_epsilon = epsilon = 1000.0;

    intst = 0.0;
    if (guess == 0.0)
        guess = 0.0001;

    if (fabs(fval - pval - n * pmt) > 1.0e-13) {
        while ((count < 10000) && (fabs(epsilon) > 1.0e-15)) {
            tempyf = pow(1.0 + guess, n);
#if 0
            if (errno) {
                printf("Error in newton_raphson_interest: pow(): guess=%.4f%% n=%.2f\n", guess*100.0, n);
                exit(1);
            }
#endif
            fy = fval + pval*tempyf + (1.0 + dcalc_finPayAt0*guess)*pmt*(tempyf - 1.0)/guess;

            tempyf = pow(1.0 + guess, n-1);
#if 0
            if (errno) {
                printf("Error in newton_raphson_interest: dy: pow(): guess=%.4f%% n=%.2f\n", guess*100.0, n);
                exit(1);
            }
#endif
            dy = (((pval + pmt*(double)dcalc_finPayAt0)*n*guess*guess + (n - 1.0)*guess*pmt - pmt)*tempyf + pmt)/(guess*guess);

            epsilon=fy/dy;
            guess -= epsilon;
            if (fabs(epsilon) > fabs(last_epsilon))
                epsilon_increasing++;
            else
                epsilon_increasing = 0;
            if (epsilon_increasing > 5) { /* give up */
                *converged = 0;
                return intst;
            }

            if (guess > max_guess) { /* give up */
                converged = 0;
                return intst;
            }

            last_epsilon = epsilon;
            count++;
        }
        intst = guess;
    }
    *converged = 1;
    return intst;
}

static double
init_guess(double fval, double pval, double n, double pmt) {
    double guess;

    if (fabs(pval) < 1.0e-11)
        /* guess = -2.0*(fval + pmt*n)/(pmt*(n + 1.0)*n); */
        guess = 2.0*(pow(-fval/(n*pmt),1.0/n) - 1.0);
    else
        guess = pow(-(fval + pmt*n)/pval, 1.0/n) - 1.0;
    return(guess);
}

/*
 * this is a financial calculation after all, so it is fair to assume
 * that interest is 0-1000% and that either pval or fval is
 * non-zero. Also n>=1.
 *
 * The initial guess must not blow up pow(1+guess,n) so initial guess < pow(L,1/n)-1
 */
static double
calc_intst(double fval,
           double pval,
           double n,
           double pmt,
           int *converged) {

    int count = 0, watch_pval;
    double guess, lower_guess, upper_guess, error, intst, max_guess, newguess;
    double calculated_val, good_enough_guess, val;

#define MAX_GUESSES 50
#define MAX_INTEREST_RATE 1.0e10 /* financial calculation, right? */

    lower_guess = intst = 0.0;
    upper_guess = pow(DBL_MAX,1.0/n)-1.0;

    if (upper_guess > MAX_INTEREST_RATE)
        upper_guess = MAX_INTEREST_RATE;
    max_guess = upper_guess;

    if (fabs(pval) < 1.0e-11 && fabs(pmt) < 1.0e-11) {
        converged = 0;
        return intst;
    }

    /* keep an eye on pval if fval is zero: */
    watch_pval = (fabs(fval) < 1.0e-11);
    if (watch_pval && fabs(pval) < 1.0e-11) {
        *converged = 0;
        return intst;
    }

    val = watch_pval? pval: fval;
    good_enough_guess = fabs(val) * 0.05;

    /* guess high in the 'normal' case to avoid exploring the looney
         * rates. This first recursion is from Steve Steps:
         */
    guess = init_guess(fval, pval, n, pmt);
    if (fabs(pval) > 1.0e-11) { /* tends to converge if pval != 0 */
        int rounds = 20;
        double newguess;

        while (rounds--) {
            if (guess > max_guess) {
                break;
            } else {
                newguess = (1.0+guess*dcalc_finPayAt0)*pmt*(1.0-pow(1.0+guess,n)) / (fval + pval*pow(1.0+guess,n));
            }
            if (newguess <= 0.0) { /* it failed */
                /* printf("Negative guess. Reverting to initial guess\n"); */
                guess = init_guess(fval, pval, n, pmt);
                break;
            }

            calculated_val = watch_pval?
                calc_pval(fval, n, pmt, guess):
                calc_fval(pval, n, pmt, guess);
            error = fabs(calculated_val - val);
            if (error < good_enough_guess)
                break; /* good enough */
            guess = newguess;
        }
    }

    if (guess > max_guess) {
        /* printf("Guess too high (%f%%)\n", guess*100.0); */
        guess = init_guess(fval, pval, n, pmt);
        if (guess > max_guess)
            guess = max_guess / 4.0;
    }

    /* Try and improve guess with a binary chop */
    while (count < MAX_GUESSES) {
        calculated_val = watch_pval?
            calc_pval(fval, n, pmt, guess):
            calc_fval(pval, n, pmt, guess);
        error = fabs(calculated_val - val);

        if (error < good_enough_guess) {
            intst = newton_raphson_interest(fval, pval, n, pmt, guess, max_guess, converged);

            if (!*converged) {
                *converged = 0;
                return intst;
            }

            calculated_val = watch_pval?
                calc_pval(fval, n, pmt, intst):
                calc_fval(pval, n, pmt, intst);

            if (fabs(val - calculated_val) > (fabs(val) / 1000.0)) {
                *converged = 0;
                return intst;
            }

            *converged = 1;
            return intst;

        }

        if (fabs(upper_guess - lower_guess) < 1.0e-15) {
            *converged = 0;
            break;
        }

        /* need a new guess ... */
        if (calculated_val < val)
            upper_guess = guess;
        else
            lower_guess = guess;
        count++;
        newguess = (upper_guess + lower_guess) / 2.0;
        if (fabs(newguess - guess) < 0.000001)
            break;
        guess = newguess;
    }

    /* Binary chop failed, go back to initial guess in desperation */
    guess = init_guess(fval, pval, n, pmt);
    intst = newton_raphson_interest(fval, pval, n, pmt, guess, max_guess, converged);

    if (!*converged) {
        return intst;
    }

    calculated_val = watch_pval?
        calc_pval(fval, n, pmt, intst):
        calc_fval(pval, n, pmt, intst);

    if (fabs(val - calculated_val) > (fabs(val) / 1000.0)) {
        *converged = 0;
        return intst;
    }

    *converged = 1;
    return intst;
}

int
dcalc_exec_intst() {
    double      tempxf;
    double      n, pval, pmt, fval, intst;
    int converged;

    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted()) {
        dcalc_pushf(dcalc_regf[FIN_INT_REG]);
        gui_dispnums();
        gui_msg("Interest recalled");
    } else if (!dcalc_last_was_fin_key) {
        save_x();
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
        if (tempxf < 0.0) {
            gui_msg("Interest cannot be < 0");
            return 0;
        }
        dcalc_regf[FIN_INT_REG] = tempxf;
        gui_dispreg(FIN_INT_REG);
        gui_dispnums();
        dcalc_last_was_fin_key = TRUE;
        gui_msg("Interest (%) entered");
    } else {
        n = dcalc_regf[FIN_NUM_REG];
        if (n <= 0.0) {
            gui_msg("# payments must be > 0");
            return 0;
        }
        pval = dcalc_regf[FIN_PVAL_REG];
        pmt = dcalc_regf[FIN_PMT_REG];
        fval = dcalc_regf[FIN_FVAL_REG];
        intst = 0.0;
        if (fabs(fval + pval + (n + dcalc_finPayAt0)*pmt) == 0.0) {
            tempxf = 0.0;
            converged = 1;
        } else {
            /* use Newton-Raphson iteration ...
               if we are seeking roots of f(x)=0 then
               x_n+1 = x_n - f(x_n) / f'(x_n)

               The tricky part is the initial guess. The relationship
               is very unstable in 'i' and you have to get quite close
               to have a chance of converging.

            */

            intst = calc_intst(fval, pval, n, pmt, &converged);
        }
        if (converged) {
            tempxf = intst * 100.0; /* Convert to percent */
            dcalc_pushf(tempxf);
            dcalc_regf[FIN_INT_REG] = tempxf;
            gui_dispreg(FIN_INT_REG);
            gui_dispnums();
            gui_msg("Interest (%) computed");
        } else
            gui_msg("Calculation does not converge!\007");
    }
    return 0;
}

int
dcalc_exec_nfin() {
    double      tempxf;
    double      interest, pval, pmt, fval;

    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_is_inverted()) {
        dcalc_pushf(dcalc_regf[FIN_NUM_REG]);
        gui_dispnums();
        gui_msg("# payments recalled");
    } else if (!dcalc_last_was_fin_key) {
        save_x();
        tempxf = dcalc_popf();
        dcalc_pushf(tempxf);
        if (tempxf <= 0.0) {
            gui_msg("# payments must be > 0");
            return 0;
        }
        dcalc_regf[FIN_NUM_REG] = tempxf;
        gui_dispreg(FIN_NUM_REG);
        gui_dispnums();
        dcalc_last_was_fin_key = TRUE;
        gui_msg("# payments entered");
    } else {
        interest = dcalc_regf[FIN_INT_REG] / 100.0;
        if (interest < 0.0) {
            gui_msg("Interest must be >= 0");
            return 0;
        }
        pval = dcalc_regf[FIN_PVAL_REG];
        pmt = dcalc_regf[FIN_PMT_REG];
        fval = dcalc_regf[FIN_FVAL_REG];
        tempxf = calc_n(fval, pval, pmt, interest);
        if (tempxf > 0) {
            dcalc_pushf(tempxf);
            dcalc_regf[FIN_NUM_REG] = tempxf;
            gui_dispreg(FIN_NUM_REG);
            gui_dispnums();
            gui_msg("# payments computed");
        }
    }
    return 0;
}

int
dcalc_exec_fclr() {
    int c;

    dcalc_is_inverted();
    if (dcalc_mode == PROG)
        return 0;

    c = gui_dialog("Clear all finance registers?" DIALOG_LETTERS);
    if (toupper(c) == 'Y') {
        int i;
        for (i=5; i< 10; i++)
            dcalc_regf[i] = 0;
        gui_msg("Fin registers cleared");
        gui_dispregs();
    }
    return 0;
}

int
dcalc_exec_begin() {
    dcalc_is_inverted();

    if (dcalc_mode == PROG)
        return 0;

    if (dcalc_finPayAt0) {
        dcalc_finPayAt0 = 0;
        gui_msg("Annuity in arrears");
    } else {
        dcalc_finPayAt0 = 1;
        gui_msg("Annuity in advance");
    }
    return 0;
}

static int dayFunction(double f) {
    long y, m, d, x, z;

    f += 0.00005; /* round up */
    y = f;
    f = (f - y) * 100;
    m = f;
    f = (f - m) * 100;
    d = f;
    if (m <= 2) {
        x = 0;
        z = y - 1;
    } else {
        x = (long) (0.4*m + 2.3);
        z = y;
    }
    return 365*y + 31 *(m - 1) + d + (int)(z/4) - x;
}

double dcalc_dateplus(double start, double days) {
    struct tm tm, *new_time;
    int y, m, d;
    time_t new_time_secs;
    double retval;

    start += 0.00005; /* round up */
    y = start;
    start = (start - y) * 100;
    m = start;
    start = (start - m) * 100;
    d = start;

    tm.tm_sec = 0;
    tm.tm_min = 0;
    tm.tm_hour = 0;
    tm.tm_mday = d;
    tm.tm_mon = m - 1;
    tm.tm_year = y - 1900;
    tm.tm_isdst = 0;
    new_time_secs = days; // convert to time_t from double
    new_time_secs = mktime(&tm) + new_time_secs * 24 * 60 * 60;
    new_time = localtime(&new_time_secs);
    
    retval = new_time->tm_year + 1900 + (new_time->tm_mon + 1) / 100.0 + new_time->tm_mday / 10000.0;
    return(retval);
}

double dcalc_datedelta(double x, double y) {
    return dayFunction(y) - dayFunction(x);
}

int
dcalc_exec_dys() {
    double      tempxf, tempyf;
    if (dcalc_mode == PROG)
        return 0;

    save_x();
    tempxf = dcalc_popf();
    tempyf = dcalc_popf();
          
    if (dcalc_is_inverted()) {
        tempxf = dcalc_dateplus(tempyf, tempxf);
        gui_msg("date (y) plus days (x) calculated");
    } else {
        tempxf = dcalc_datedelta(tempxf, tempyf);
        gui_msg("days between two dates calculated");
    }
    dcalc_pushf(tempxf);
    
    if (dcalc_decplaces < 4)
        dcalc_decplaces = 4;
    if (dcalc_floatmode != FIXFORMAT)
        dcalc_exec_fix();
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_tdy() {
    double      tempxf;
    struct tm *tmbuf;
    time_t t;

    dcalc_is_inverted();
    if (dcalc_mode == PROG)
        return 0;
    save_x();

    t = time(NULL);
    tmbuf = localtime(&t);
    tempxf = tmbuf->tm_year + 1900;
    tempxf += (tmbuf->tm_mon + 1) / 100.0;
    tempxf += tmbuf->tm_mday / 10000.0;
    dcalc_pushf(tempxf);
    if (dcalc_decplaces != 4)
        dcalc_decplaces = 4;
    if (dcalc_floatmode != FIXFORMAT)
        dcalc_exec_fix();
    gui_dispnums();
    gui_msg("Today's date");
    return 0;
}

int
dcalc_exec_times12() {
    double      tempxf;

    if (dcalc_is_inverted())
        return dcalc_exec_divide12();

    if (dcalc_mode == PROG)
        return 0;
    save_x();

    tempxf = dcalc_popf();
    tempxf *= 12.0;
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_divide12() {
    double      tempxf;

    if (dcalc_is_inverted())
        return dcalc_exec_times12();

    if (dcalc_mode == PROG)
        return 0;

    save_x();
    tempxf = dcalc_popf();
    tempxf /= 12.0;
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}


static void process_digit(COMMAND c) {
    switch (toupper(c)) {
    case BACKSPACE:
    case '0':
    case '1':
        echo_char(c);
        break;

    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
        if (dcalc_mode == PROG && dcalc_intmode == BIN)
            gui_put_a_char(BELL);
        else
            echo_char(c);
        break;

    case '8':
    case '9':
        if ((dcalc_mode == PROG) && ((dcalc_intmode == BIN) || (dcalc_intmode == OCT)))
            gui_put_a_char(BELL);
        else
            echo_char(c);
        break;

    case 'A':
    case 'B':
    case 'C':
    case 'D':
    case 'F':
        if (dcalc_mode != PROG || dcalc_intmode != HEX)
            gui_put_a_char(BELL);
        else
            echo_char(c);
        break;

    case 'E':
        if (dcalc_mode == PROG) {
            if (dcalc_intmode == HEX) {
                echo_char(c);
            } else {
                gui_put_a_char(BELL);
            }
        } else {
            if (strchr(dcalc_inbuf, 'E') == NULL) {
                if (strlen(dcalc_inbuf) == 0)
                    echo_char('1');
                echo_char(c);
            } else {
                gui_msg("Can't put another E in the number!");
                gui_put_a_char(BELL);
            }
        }
        break;

    case '.':
        if (dcalc_mode == PROG) { /* ASCIIM never reaches here! */
            if (dcalc_intmode == IP) {
                echo_char('.');
            } else {
                gui_put_a_char(BELL);
            }
        } else if (dcalc_entering && strchr(dcalc_inbuf, '.'))
            gui_put_a_char(BELL);
        else
            echo_char(c);
        break;

    default:
        gui_put_a_char(BELL);
        break;
    }
}

/* void storeOp(int op, int i) { */

static void
stoplus(int i) {
    if ((i >= 0) && (i < NUMREGS)) {
        char buf[MAX_ENTRY], *s;
        if (dcalc_mode == PROG)
            dcalc_reg[i] += dcalc_xiReg;
        else
            dcalc_regf[i] += dcalc_xfReg;
        strcpy(buf, "Added to R");
        gui_dispreg(i);
        s = buf + strlen(buf);
        sprintf(s, "%d", i);
        gui_msg(buf);
    }
}

static void
stominus(int i) {

    if ((i >= 0) && (i < NUMREGS)) {
        char buf[MAX_ENTRY], *s;
        if (dcalc_mode == PROG)
            dcalc_reg[i] -= dcalc_xiReg;
        else
            dcalc_regf[i] -= dcalc_xfReg;
        strcpy(buf, "Subtracted from R");
        gui_dispreg(i);
        s = buf + strlen(buf);
        sprintf(s, "%d", i);
        gui_msg(buf);
    }
}

static void
stotimes(int i) {
    if ((i >= 0) && (i < NUMREGS)) {
        char buf[MAX_ENTRY], *s;
        if (dcalc_mode == PROG)
            dcalc_reg[i] *= dcalc_xiReg;
        else
            dcalc_regf[i] *= dcalc_xfReg;
        strcpy(buf, "Multiplied into R");
        gui_dispreg(i);
        s = buf + strlen(buf);
        sprintf(s, "%d", i);
        gui_msg(buf);
    }
}

static void
stodivide(int i) {
    if ((i >= 0) && (i < NUMREGS)) {
        char buf[MAX_ENTRY], *s;
        if (dcalc_mode == PROG)
            dcalc_reg[i] /= dcalc_xiReg;
        else
            dcalc_regf[i] /= dcalc_xfReg;
        strcpy(buf, "Divided into R");
        gui_dispreg(i);
        s = buf + strlen(buf);
        sprintf(s, "%d", i);
        gui_msg(buf);
    }
}

int
dcalc_exec_store() {
    int i, j;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    dcalc_stop_entering();
    i = gui_store("Store in register [0-9] (or [+-*/] reg.)");
    j = i;
    if ((j=='+') || (j=='-') || (j=='*') || (j=='/')) {
        char buf[MAX_ENTRY];
        sprintf(buf,"Store %c in register [0-9]", j);
        i = gui_store(buf);
        if (isdigit(i))
            i -= '0';
        else
            return 0;
    } else {
        j = 0;
        if (isdigit(i))
            i -= '0';
        else
            return 0;
    }
    dcalc_stop_entering();

    if ((i >= 0) && (i < NUMREGS)) {
        char buf[MAX_ENTRY], *s;
        switch (j) {
        case '+': stoplus(i); break;
        case '-': stominus(i); break;
        case '*': stotimes(i); break;
        case '/': stodivide(i); break;
        case 0:
            if (dcalc_mode == PROG)
                dcalc_reg[i] = dcalc_xiReg;
            else
                dcalc_regf[i] = dcalc_xfReg;
            strcpy(buf, "Stored in R");
            gui_dispreg(i);
            s = buf + strlen(buf);
            sprintf(s, "%d", i);
            gui_msg(buf);
            break;
        }
    }
    return 0;
}

int
dcalc_exec_stoplus() {
    int i;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    dcalc_stop_entering();
    i = gui_store("Store + in register [0-9]");
    if (isdigit(i))
        i -= '0';
    else
        return 0;
    dcalc_stop_entering();

    stoplus(i);
    return 0;
}

int
dcalc_exec_stominus() {
    int i, j;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    dcalc_stop_entering();
    i = gui_store("Store in register [0-9] (or [+-*/] reg.)");
    j = i;
    if ((j=='+') || (j=='-') || (j=='*') || (j=='/'))
        i = gui_store("Store in register [0-9]");
    else {
        j = 0;
        if (isdigit(i))
            i -= '0';
        else
            return 0;
    }
    dcalc_stop_entering();
    stominus(i);
    return 0;
}

int
dcalc_exec_stomultiply() {
    int i, j;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    dcalc_stop_entering();
    i = gui_store("Store in register [0-9] (or [+-*/] reg.)");
    j = i;
    if ((j=='+') || (j=='-') || (j=='*') || (j=='/'))
        i = gui_store("Store in register [0-9]");
    else {
        j = 0;
        if (isdigit(i))
            i -= '0';
        else
            return 0;
    }
    dcalc_stop_entering();
    stotimes(i);
    return 0;
}

int
dcalc_exec_stodivide() {
    int i, j;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    dcalc_stop_entering();
    i = gui_store("Store in register [0-9] (or [+-*/] reg.)");
    j = i;
    if ((j=='+') || (j=='-') || (j=='*') || (j=='/'))
        i = gui_store("Store in register [0-9]");
    else {
        j = 0;
        if (isdigit(i))
            i -= '0';
        else
            return 0;
    }
    dcalc_stop_entering();
    stodivide(i);
    return 0;
}

int
dcalc_exec_rolldown() {
    long tempx;
    double tempxf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        dcalc_tiReg = tempx;
    } else {
        tempxf = dcalc_popf();
        dcalc_tfReg = tempxf;
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_clx() {
    long tempx;
    double tempxf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG) {
        dcalc_liReg = dcalc_pop();
        tempx = 0;
        dcalc_push(tempx);
    } else {
        dcalc_lfReg = dcalc_popf();
        tempxf = 0.0;
        dcalc_pushf(tempxf);
    }
    dcalc_entering = TRUE; /* to stop stack lift */
    inptr = dcalc_inbuf;
    *inptr = 0;
    gui_dispnums();
    return 0;
}

/* Clear stack (INV = clear Registers) */
int
dcalc_exec_clr() {
    int c;

    if (dcalc_is_inverted()) {
        c = gui_dialog("Clear all registers?" DIALOG_LETTERS);
        if (toupper(c) == 'Y') {
            int i;
            for (i=0; i< NUMREGS; i++)
                if (dcalc_mode == PROG)
                    dcalc_reg[i] = 0;
                else
                    dcalc_regf[i] = 0;
            gui_msg("Registers cleared");
            gui_dispregs();
        }
    } else {
        dcalc_last_was_fin_key = 0;
        if (dcalc_mode == PROG) {
            dcalc_xiReg = dcalc_yiReg = dcalc_ziReg = dcalc_tiReg = dcalc_liReg = 0;
        } else {
            dcalc_xfReg = dcalc_yfReg = dcalc_zfReg = dcalc_tfReg = dcalc_lfReg = 0;
        }
        gui_dispnums();
    }
    return 0;
}

int
dcalc_exec_toggle_deg() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    dcalc_degree = !dcalc_degree;
    gui_print_deg();
    return 0;
}

int
dcalc_exec_degree() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    dcalc_degree = 1;
    gui_print_deg();
    gui_msg("Angles are in degrees");
    return 0;
}

int
dcalc_exec_radian() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    dcalc_degree = 0;
    gui_print_deg();
    gui_msg("Angles are in radians");
    return 0;
}

int
dcalc_exec_e() {
    double tempxf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    tempxf = exp(1.0);
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_pi() {
    double tempxf;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    tempxf = pi;
    dcalc_pushf(tempxf);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_enter() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;

    if (!dcalc_stop_entering()) {
        dcalc_push(dcalc_xiReg);
        dcalc_pushf(dcalc_xfReg);
    }
    gui_dispnums();
    return 0;
}

#ifdef HAS_ALGEBRAIC_MODE

int
dcalc_exec_rpn() {
    dcalc_is_inverted();
    dcalc_stop_entering();
    dcalc_last_was_fin_key = 0;
    dcalc_algebraic_mode = 0;
    return 0;
}

int
dcalc_exec_algebraic() {
    dcalc_is_inverted();
    dcalc_stop_entering();
    dcalc_last_was_fin_key = 0;
    dcalc_algebraic_mode = 1;
    return 0;
}

static void
saveStack() {
    dcalc_xfSave = dcalc_xfReg;
    dcalc_yfSave = dcalc_yfReg;
    dcalc_zfSave = dcalc_zfReg;
    dcalc_tfSave = dcalc_tfReg;
    dcalc_lfSave = dcalc_lfReg;
    dcalc_xSave = dcalc_xiReg;
    dcalc_ySave = dcalc_yiReg;
    dcalc_zSave = dcalc_ziReg;
    dcalc_tSave = dcalc_tiReg;
    dcalc_lSave = dcalc_liReg;
}

/*
  could be a custom button
  could be algebraic dcalc_mode or rpn
  could be dcalc_entering
*/
void
dcalc_perform_eval(char *expr) {
    char *s, *d;
    int retval;

    /* copy to dcalc_last_eval, removing commas, shift to lowercase */
    for (s = expr, d = dcalc_last_eval; *s; s++)
        if (*s != ',') 
            *d++ = tolower(*s);
    *d = 0;
    
    saveStack();
    yyinit(dcalc_last_eval);
    retval = yyparse();
    if (retval != 0)
        gui_msg("Syntax error");

    dcalc_entering = FALSE; /* dcalc_xfReg now has the value calculated by yyparse() */
    gui_dispnums();
}

int
dcalc_exec_eval() {
    char expr[MAX_ENTRY];

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if ((gui_get_eval_expression(dcalc_last_eval, expr) == 0)) {
        dcalc_perform_eval(expr);
        dcalc_cpy(dcalc_last_eval, expr, MAX_ENTRY);
    }
    return 0;
}

int
dcalc_exec_equals() {
    dcalc_is_inverted();
    /* dcalc_last_was_fin_key = 0; */
    if (strlen(dcalc_last_eval) == 0) {
        return 0;
    } else { /* add a missing closing brace: */
        int leftBraces = 0, rightBraces = 0;
        char *s = dcalc_last_eval;
        while (s && *s && (s = strchr(s, '('))) {
            leftBraces++;
            s++;
        }
        s = dcalc_last_eval;
        while (s && *s && (s = strchr(s, ')'))) {
            rightBraces++;
            s++;
        }
        /* add a missing 'x' */
        if (dcalc_last_eval[strlen(dcalc_last_eval) - 1] == '(')
            strcat(dcalc_last_eval, "x");

        /* close braces */
        while (rightBraces < leftBraces) {
            strcat(dcalc_last_eval, ")");
            rightBraces++;
        }
    }

    dcalc_perform_eval(dcalc_last_eval);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_leftbrace() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    gui_dispnums();
    return 0;
}

int
dcalc_exec_rightbrace() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    gui_dispnums();
    return 0;
}
#endif /* HAS_ALGEBRAIC_MODE */

int
dcalc_exec_lastx() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG)
        dcalc_push(dcalc_liReg);
    else
        dcalc_pushf(dcalc_lfReg);
    gui_dispnums();
    return 0;
}

int
dcalc_exec_quit() {
    int c = QUIT;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
#ifndef HAVE_CONFIG_H /* gtk */
    c = gui_dialog("Really quit?" DIALOG_LETTERS);
    if (toupper(c) == 'Y') {
        c = QUIT;
        dcalc_terminate();
    } else
        c = 0;
    gui_clear_msg();
#else
    dcalc_terminate();
#endif
    return c;
}

/* For evaluation in algebraic expressions */
long dcalc_ircl(long i) {
    return dcalc_reg[i];
}

double dcalc_rcl(double i) {
    return dcalc_regf[(int)i];
}

int
dcalc_exec_recall() {
    int i;

    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;

    i = gui_recall("Recall from register" DIALOG_NUMBERS);

    if (isdigit(i)) {
        i -= '0';
        if ((i >= 0) && (i < NUMREGS)) {
            char buf[MAX_ENTRY];

#ifdef HAS_ALGEBRAIC_MODE
            if (dcalc_algebraic_mode) {
                sprintf(buf, " RCL(%d)", i);
                gui_add_x(buf);
            } else
#endif /* HAS_ALGEBRAIC_MODE */
            {
                (dcalc_mode == PROG)? dcalc_push(dcalc_reg[i]): dcalc_pushf(dcalc_regf[i]);
                gui_dispnums();
            }
            sprintf(buf, "Recalled from R%d", i);
            gui_msg(buf);
        } else
            gui_msg("index out of bounds");
    }
    return 0;
}

int
dcalc_exec_xny() {
    long tempx, tempy;
    double tempxf, tempyf;

    dcalc_is_inverted();
    dcalc_stop_entering();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG) {
        tempx = dcalc_pop();
        tempy = dcalc_pop();
        dcalc_push(tempx);
        dcalc_push(tempy);
    } else {
        tempxf = dcalc_popf();
        tempyf = dcalc_popf();
        dcalc_pushf(tempxf);
        dcalc_pushf(tempyf);
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_xnl() {
    long tempx;
    double tempxf;

    dcalc_is_inverted();
    dcalc_stop_entering();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG) {
        tempx = dcalc_xiReg;
        dcalc_xiReg = dcalc_liReg;
        dcalc_liReg = tempx;
    } else {
        tempxf = dcalc_xfReg;
        dcalc_xfReg = dcalc_lfReg;
        dcalc_lfReg = tempxf;
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_xnt() {
    long tempx;
    double tempxf;

    dcalc_is_inverted();
    dcalc_stop_entering();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG) {
        tempx = dcalc_xiReg;
        dcalc_xiReg = dcalc_tiReg;
        dcalc_tiReg = tempx;
    } else {
        tempxf = dcalc_xfReg;
        dcalc_xfReg = dcalc_tfReg;
        dcalc_tfReg = tempxf;
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_xnz() {
    long tempx;
    double tempxf;

    dcalc_is_inverted();
    dcalc_stop_entering();
    dcalc_last_was_fin_key = 0;
    if (dcalc_mode == PROG) {
        tempx = dcalc_xiReg;
        dcalc_xiReg = dcalc_ziReg;
        dcalc_ziReg = tempx;
    } else {
        tempxf = dcalc_xfReg;
        dcalc_xfReg = dcalc_zfReg;
        dcalc_zfReg = tempxf;
    }
    gui_dispnums();
    return 0;
}

int
dcalc_exec_help() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    gui_pop_up_help();
    return 0;
}

int
dcalc_exec_reg() {
    dcalc_is_inverted();
    dcalc_last_was_fin_key = 0;
    gui_pop_up_reg();
    return 0;
}

int
dcalc_exec_inv() {
    dcalc_last_was_fin_key = 0;
    dcalc_invert = !dcalc_invert;
    gui_print_inv();
    gui_clear_msg();
    return 0;
}

int dcalc_process(COMMAND c) {
    gui_clear_msg();
#ifdef HAS_ALGEBRAIC_MODE
    if (dcalc_algebraic_mode)
        if (c >= ' ' && c <= '~') {
            dcalc_last_was_fin_key = 0;
            echo_char(c);
            return 0;
        } /* else a command */
#endif /* HAS_ALGEBRAIC_MODE */

    /* ASCII MODE - almost any character can be echo'ed */
    if ((c < MIN_COMMAND) && (dcalc_mode == PROG && dcalc_intmode == ASCIIM)) {
        dcalc_last_was_fin_key = 0;
        echo_char(c); /* An ordinary character was typed */
        return 0;
    }

    if (c == INV) {
        dcalc_exec_inv();
        return 0;
    }

    if (c == BACKSPACE && dcalc_is_inverted()) {
        dcalc_exec_clx();
        return 0;
    }

    if (((c >= '0') && (c <= '9')) ||
        ((c >= 'a') && (c <= 'f')) ||
        ((c >= 'A') && (c <= 'F')) ||
        (c == BACKSPACE) ||
        (c == '.')) {
        process_digit(c);
        dcalc_last_was_fin_key = 0;
        return 0;
    }

    switch (c) {
    case CHS:           dcalc_exec_chs();             break;
    case PLUS:          dcalc_exec_plus();            break;
    case MINUS:         dcalc_exec_minus();           break;
    case DIVIDE:        dcalc_exec_divide();          break;
    case MULT:          dcalc_exec_mult();            break;
    case dAND:          dcalc_exec_and();             break;
    case dOR:           dcalc_exec_or();              break;
    case dXOR:          dcalc_exec_xor();             break;
    case dNOT:          dcalc_exec_not();             break;
    case MODULUS:       dcalc_exec_modulus();         break;
    case '%':
    case PERCENT:       dcalc_exec_percent();         break;
    case PERCENTCH:     dcalc_exec_percentch();       break;
    case YTOX:          dcalc_exec_ytox();            break;
    case SHIFTL:        dcalc_exec_shiftl();          break;
    case SHIFTR:        dcalc_exec_shiftr();          break;
    case SHIFTYL:       dcalc_exec_shiftyl();         break;
    case SHIFTYR:       dcalc_exec_shiftyr();         break;
    case PRIMEF:        dcalc_exec_primef();          break;
    case RECI:          dcalc_exec_reci();            break;
    case SQR:           dcalc_exec_sqr();             break;
    case ROOT:          dcalc_exec_root();            break;
    case CUBE:          dcalc_exec_cube();            break;
    case CROOT:         dcalc_exec_croot();           break;
    case PVAL:          dcalc_exec_pval();            break;
    case FVAL:          dcalc_exec_fval();            break;
    case PMT:           dcalc_exec_pmt();             break;
    case INTST:         dcalc_exec_intst();           break;
    case FCLR:          dcalc_exec_fclr();            break;
    case NFIN:          dcalc_exec_nfin();            break;
    case TIMES12:       dcalc_exec_times12();         break;
    case DIVIDE12:      dcalc_exec_divide12();        break;
    case BEGIN:         dcalc_exec_begin();           break;
    case DYS:           dcalc_exec_dys();             break;
    case TDY:           dcalc_exec_tdy();             break;
    case ASCIIM:        dcalc_exec_asciim();          break;
    case BIN:           dcalc_exec_bin();             break;
    case IP:            dcalc_exec_ip();              break;
    case OCT:           dcalc_exec_oct();             break;
    case DEC:           dcalc_exec_dec();             break;
    case HEX:           dcalc_exec_hex();             break;
    case FIXFORMAT:     dcalc_exec_fix();             break;
    case ENGFORMAT:     dcalc_exec_eng();             break;
    case SCIFORMAT:     dcalc_exec_sciformat();       break;
    case PLACES:        dcalc_exec_places();          break;
    case FIN:           dcalc_exec_fin();             break;
    case STAT:          dcalc_exec_stat();            break;
    case SCI:           dcalc_exec_sci();             break;
    case PROG:          dcalc_exec_prog();            break;
    case ROLLDOWN:      dcalc_exec_rolldown();        break;
    case 'x':
    case 'X':
    case CLX:           dcalc_exec_clx();             break;
    case CLR:           dcalc_exec_clr();             break;
    case DEGREE:        dcalc_exec_degree();          break;
    case RADIAN:        dcalc_exec_radian();          break;
    case E:             dcalc_exec_e();               break;
    case PI:            dcalc_exec_pi();              break;
    case SIN:           dcalc_exec_sin();             break;
    case COS:           dcalc_exec_cos();             break;
    case TAN:           dcalc_exec_tan();             break;
    case SINH:          dcalc_exec_sinh();            break;
    case COSH:          dcalc_exec_cosh();            break;
    case TANH:          dcalc_exec_tanh();            break;
    case LOGE:          dcalc_exec_loge();            break;
    case LOG10:         dcalc_exec_log10();           break;
    case FRC:           dcalc_exec_frc();             break;
    case INT:           dcalc_exec_int();             break;
    case ETOX:          dcalc_exec_etox();            break;
    case HMS:           dcalc_exec_hms();             break;
    case RTOP:          dcalc_exec_rtop();            break;
    case DTOR:          dcalc_exec_dtor();            break;
    case SUMR:          dcalc_exec_sumr();            break;
    case SUM0:          dcalc_exec_sum0();            break;
    case NSTAT:         dcalc_exec_nstat();           break;
    case MEAN:          dcalc_exec_mean();            break;
    case MEANS:         dcalc_exec_means();           break;
    case MEANY:         dcalc_exec_meany();           break;
    case S_DEV:         dcalc_exec_s_dev();           break;
    case SUM:           dcalc_exec_sum();             break;
    case FACT:          dcalc_exec_fact();            break;
    case COMB:          dcalc_exec_comb();            break;
    case PERM:          dcalc_exec_perm();            break;
    case LR:            dcalc_exec_lr();              break;
    case CALCY:         dcalc_exec_calcy();           break;
    case ENTER:         dcalc_exec_enter();           break;
    case LASTX:         dcalc_exec_lastx();           break;
    case QUIT:          c = dcalc_exec_quit();        break;
    case 'r':
    case 'R':
    case RECALL:        dcalc_exec_recall();          break;
    case 's':
    case 'S':
    case STORE:         dcalc_exec_store();           break;
    case STOPLUS:       dcalc_exec_stoplus();         break;
    case STOMINUS:      dcalc_exec_stominus();        break;
    case STOMULTIPLY:   dcalc_exec_stomultiply();     break;
    case STODIVIDE:     dcalc_exec_stodivide();       break;
    case 'y':
    case 'Y':
    case XNY:           dcalc_exec_xny();             break;
    case 'l':
    case 'L':
    case XNL:           dcalc_exec_xnl();             break;
    case 't':
    case 'T':
    case XNT:           dcalc_exec_xnt();             break;
    case 'z':
    case 'Z':
    case XNZ:           dcalc_exec_xnz();             break;
    case HELP:          dcalc_exec_help();            break;
    case REGISTER:      dcalc_exec_reg();             break;
#ifdef HAS_ALGEBRAIC_MODE
    case RPN:           dcalc_exec_rpn();             break;
    case ALGEBRAIC:     dcalc_exec_algebraic();       break;
    case EVAL:          dcalc_exec_eval();            break;
    case '=':
    case EQUALS:        dcalc_exec_equals();          break;
#endif /* HAS_ALGEBRAIC_MODE */
    case NOP:
        break;

    default:
        dcalc_last_was_fin_key = 0;
        gui_put_a_char(BELL);
        break;
    }
    return c;
}

static void getReg(char *b) {
    long l;
    int i;

    if (sscanf(b, "%d %ld", &i, &l))
        if (i >= 0 && i < NUMREGS)
            dcalc_reg[i] = l;
}

static void getRegf(char *b) {
    double l;
    int i;

    if (sscanf(b, "%d %lg", &i, &l))
        if (i >= 0 && i < NUMREGS)
            dcalc_regf[i] = l;
}

static void getMode(char *b) {
    dcalc_mode =
        (strcmp(b, "sci")==0)  ? SCI:
        (strcmp(b, "fin")==0)  ? FIN:
        (strcmp(b, "stat")==0) ? STAT:
        (strcmp(b, "prog")==0) ? PROG: SCI;
}

static void getIntmode(char *b) {
    dcalc_intmode =
        strcmp(b, "asci")==0? ASCIIM:
        strcmp(b, "ip")==0? IP:
        strcmp(b, "bin")==0? BIN:
        strcmp(b, "oct")==0? OCT:
        strcmp(b, "dec")==0? DEC:
        strcmp(b, "hex")==0? HEX: DEC;
}

static void getFloatmode(char *b) {
    dcalc_floatmode =
        strcmp(b, "FIX")==0? FIXFORMAT:
        strcmp(b, "ENG")==0? ENGFORMAT:
        strcmp(b, "SCI")==0? SCIFORMAT: FIXFORMAT;
}

/* path is an absolute path terminating in '/' ie path == '/foo/bar/' */
void mkPath(char *path) {
    char *p = path + 1;

    while ((p = strchr(p, '/'))) {
        *p = 0;
        mkdir(path, 0775);
        *p++ = '/';
    }
}

FILE *dcalc_open_config_file(char *name, char *dcalc_mode) {
    char *homeDir = NULL;
    char fileName[MAXPATHLEN];
    FILE *retval = NULL;
    
    homeDir = getenv("HOME");
    if (homeDir) {
        dcalc_cpy(fileName, homeDir, MAXPATHLEN);
        if (fileName[strlen(fileName) - 1] != '/')
            strcat(fileName, "/");
        strcat(fileName, ".local/share/dcalc/");
        if (*dcalc_mode == 'w')
            mkPath(fileName);
        strcat(fileName, name);
        retval = fopen(fileName, dcalc_mode);
    }
    return retval;
}
    
static void readSettings() {
    char buf[MAX_ENTRY];
    FILE *f = NULL;
    
    f = dcalc_open_config_file("dcalcrc", "r");
    if (f) {
        while (fgets(buf, MAX_ENTRY, f) != NULL) {
            char *b = buf;

            if (*b && buf[strlen(buf) - 1] == '\n')
                buf[strlen(buf) - 1] = 0;

            if (*b == '#')
                continue;

            while (!isspace(*b))
                b++;
            *b++ = 0;
            if (strcmp(buf, "x") == 0) dcalc_xiReg = atol(b);
            if (strcmp(buf, "y") == 0) dcalc_yiReg = atol(b);
            if (strcmp(buf, "z") == 0) dcalc_ziReg = atol(b);
            if (strcmp(buf, "t") == 0) dcalc_tiReg = atol(b);
            if (strcmp(buf, "l") == 0) dcalc_liReg = atol(b);
            if (strcmp(buf, "xf") == 0) dcalc_xfReg = atof(b);
            if (strcmp(buf, "yf") == 0) dcalc_yfReg = atof(b);
            if (strcmp(buf, "zf") == 0) dcalc_zfReg = atof(b);
            if (strcmp(buf, "tf") == 0) dcalc_tfReg = atof(b);
            if (strcmp(buf, "lf") == 0) dcalc_lfReg = atof(b);
            if (strcmp(buf, "reg") == 0) getReg(b);
            if (strcmp(buf, "regf") == 0) getRegf(b);
            if (strcmp(buf, "angularMode") == 0) dcalc_degree = atoi(b);
            if (strcmp(buf, "places") == 0) dcalc_decplaces = atoi(b);
            if (strcmp(buf, "mode") == 0) getMode(b);
            if (strcmp(buf, "intmode") == 0) getIntmode(b);
            if (strcmp(buf, "floatmode") == 0) getFloatmode(b);
            if (strcmp(buf, "annuitymode") == 0) dcalc_finPayAt0 = atol(b);
        }
        fclose(f);
    }
}

void saveSettings(void) {
    FILE *f = NULL;
    char buf[MAX_ENTRY];
    char lFormat[] = "%s %.25lg\n";
    char dFormat[] = "%s %ld\n";
    char iFormat[] = "%s %d\n";
    int i;

    dcalc_stop_entering();
    f = dcalc_open_config_file("dcalcrc", "w");
    if (f) {
        fprintf(f, "# %s\n", "DCALC Version " VERSION);
        fprintf(f, "# don't edit this file - dcalc will overwrite it\n");
        fprintf(f, dFormat, "x", dcalc_xiReg);
        fprintf(f, dFormat, "y", dcalc_yiReg);
        fprintf(f, dFormat, "z", dcalc_ziReg);
        fprintf(f, dFormat, "t", dcalc_tiReg);
        fprintf(f, dFormat, "l", dcalc_liReg);
        fprintf(f, lFormat, "xf", dcalc_xfReg);
        fprintf(f, lFormat, "yf", dcalc_yfReg);
        fprintf(f, lFormat, "zf", dcalc_zfReg);
        fprintf(f, lFormat, "tf", dcalc_tfReg);
        fprintf(f, lFormat, "lf", dcalc_lfReg);
        for (i = 0; i < NUMREGS; i++) {
            sprintf(buf, "reg %d", i);
            fprintf(f, dFormat, buf, dcalc_reg[i]);
        }
        for (i = 0; i < NUMREGS; i++) {
            sprintf(buf, "regf %d", i);
            fprintf(f, lFormat, buf, dcalc_regf[i]);
        }
        fprintf(f, iFormat, "angularMode", dcalc_degree);
        fprintf(f, iFormat, "places", dcalc_decplaces);
        fprintf(f, "mode %s\n",
                dcalc_mode==SCI? "sci":
                dcalc_mode==FIN? "fin":
                dcalc_mode==STAT? "stat":
                dcalc_mode==PROG? "prog":"sci");
        fprintf(f, "intmode %s\n",
                dcalc_intmode==ASCIIM? "asci":
                dcalc_intmode==IP? "ip":
                dcalc_intmode==BIN? "bin":
                dcalc_intmode==OCT? "oct":
                dcalc_intmode==DEC? "dec":
                dcalc_intmode==HEX? "hex": "dec");
        fprintf(f, "floatmode %s\n",
                dcalc_floatmode==FIXFORMAT? "FIX": "ENG");
        /* Rev 2.7 on: */
        fprintf(f, "annuitymode %d\n", dcalc_finPayAt0? 1: 0);
        fclose(f);
    }
}

void dcalc_initialise(void) {
    int i;

    if (!was_initialised) {
        was_initialised = 1;
        gui_init();

        dcalc_xiReg = dcalc_yiReg = dcalc_ziReg = dcalc_tiReg = dcalc_liReg = 0;
        dcalc_xfReg = dcalc_yfReg = dcalc_zfReg = dcalc_tfReg = dcalc_lfReg = 0.0;
        dcalc_decplaces = 2;
        dcalc_mode = SCI;
        dcalc_intmode = DEC;
        dcalc_floatmode = FIXFORMAT;
        dcalc_entering = FALSE;
        dcalc_invert = FALSE;
        dcalc_degree = TRUE;
        dcalc_last_was_fin_key = FALSE;
        inptr = dcalc_inbuf;
        *inptr = ASCIIZ;
        max_digits = 20;
        pi = 4.0 * atan(1.0);
        d_to_r = pi / 180.0;
        r_to_d = 180.0 / pi;
        dcalc_finPayAt0 = 0;
#ifdef HAS_ALGEBRAIC_MODE
        dcalc_last_eval[0] = 0;
#endif /* HAS_ALGEBRAIC_MODE */

        for (i = 0; i < NUMREGS; i++) {
            dcalc_reg[i] = 0;
            dcalc_regf[i] = 0.0;
        }

        readSettings();
        gui_read_settings();
        gui_read_custom_settings();
        if ((dcalc_mode == PROG) && (dcalc_intmode == ASCIIM))
            gui_raw_mode(1);
    }
}

extern void dcalc_terminate() {
    saveSettings();
    gui_save_settings();
    gui_save_custom_settings();
    gui_term();
}

/* For emacs: */

/* Local Variables: */
/* eval:(setq tab-width 8) */
/* End: */
