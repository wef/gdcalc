#include <gtk/gtk.h>

extern int on_register_window_activate_enabled;
extern int on_entry_X_insert_text_enabled;

gboolean
on_custom_button_release_event         (GtkWidget *widget,
                                        GdkEventButton *event,
                                        gpointer user_data);

gboolean
on_mem_dialog_key_press_event         (GtkWidget       *widget,
                                       GdkEventKey     *event,
                                       gpointer         user_data);
