/* $Id: gui_gtk_main.h,v 1.8 2004/07/11 08:45:56 bhepple Exp $ */

#define BUTTON_LABEL_LEN 10
#define BUTTON_TOOLTIP_LEN 200
#define BUTTON_TEXT_LEN 50

typedef struct {
	int		i, j;
	COMMAND 	command;
	char		label[BUTTON_LABEL_LEN];
	char		tooltip[BUTTON_TOOLTIP_LEN];
	char		text[BUTTON_TEXT_LEN];
	char		invtext[BUTTON_TEXT_LEN];
} gtk_dcalc_button;

extern gtk_dcalc_button *gtk_dcalc_l_buttons;
extern gtk_dcalc_button *gtk_dcalc_r_buttons;
extern gtk_dcalc_button  gtk_dcalc_int_buttons[];
extern gtk_dcalc_button  gtk_dcalc_float_buttons[];
extern gtk_dcalc_button  gtk_dcalc_sci_buttons[];
extern gtk_dcalc_button  gtk_dcalc_fin_buttons[];
extern gtk_dcalc_button  gtk_dcalc_stat_buttons[];
extern gtk_dcalc_button  gtk_dcalc_prog_buttons[];

extern GtkWidget *gtk_dcalc_mem_dialog;
extern GtkWidget *gtk_dcalc_registers_popup;
extern GtkEntry  *gtk_dcalc_entry_x;
extern int        gtk_dcalc_mem_value;

extern GtkWidget  *gtk_dcalc_get_widget(char *location, char *widget_name);
extern void        gtk_dcalc_display_buttons(int c);
extern int         gtk_dcalc_lookup_and_process_button_at(gtk_dcalc_button *b, int i, int j);
extern void        gtk_dcalc_toggle_registers(void);
extern void        gtk_dcalc_set_x(char *dcalc_inbuf);
extern void        gtk_dcalc_append_x(char *dcalc_inbuf);
extern const char *gtk_dcalc_get_x(void);
extern char       *gtk_dcalc_registers_label;
extern void        gtk_dcalc_customise_button(gtk_dcalc_button *b);
