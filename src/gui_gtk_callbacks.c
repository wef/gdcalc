/* $Id: callbacks.c,v 1.11 2006/12/30 19:21:04 bhepple Exp $ */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <ctype.h>
#include <sys/types.h> /* for wait */
#include <sys/wait.h> /* for wait */
#include <sys/param.h>
#include <math.h>

#include "gui_gtk_callbacks.h"
#include "dcalc.h"
#include "gui_gtk_main.h"

#define _(x) x

void
on_exit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  dcalc_process(QUIT);
}

void
on_edit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
}

void
on_settings_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
}

void
on_degree_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  dcalc_process(DEGREE);
}

void
on_radians_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  dcalc_process(RADIAN);
}

void
on_scientific_mode_activate            (GtkMenuItem     *menuitem,
										gpointer         user_data)
{
  dcalc_process(SCI);
  gtk_dcalc_display_buttons(SCI);
  gui_dispregs();
}

void
on_financial_mode_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  dcalc_process(FIN);
  gtk_dcalc_display_buttons(FIN);
  gui_dispregs();
}

void
on_statistics_mode_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  dcalc_process(STAT);
  gtk_dcalc_display_buttons(STAT);
  gui_dispregs();
}

void
on_programming_mode_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  dcalc_process(PROG);
  gtk_dcalc_display_buttons(PROG);
  gui_dispregs();
}

void
on_convert_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
}

/* structure to hold GUI settings: */
struct assoc_s {
	char *name;
	char *value;
};
typedef struct assoc_s *assoc;
static assoc *assocList = NULL;

static assoc *getNextNameValue(assoc *p, char **name, char **value) {
    assoc *a = (assoc *) p;
    if (a == NULL)
		a = assocList;
    else
		a++;

    if (a == NULL || *a == NULL)
		return NULL;

	*name = (*a)->name;
	*value = (*a)->value;
    return a;
}

static char *getNameString(char *name) {
    assoc *a;

    if (assocList == NULL) {
	assocList = malloc(sizeof(assoc));
	*assocList = NULL;
    }
    for (a = assocList; *a != NULL; a++)
	if (strcmp((*a)->name, name) == 0)
	    return((*a)->value);
    return "";
}

static void saveNameString(char *name, char *value) {
    assoc *a;
    int listSize = 0;

    if (assocList == NULL) {
	assocList = malloc(sizeof(assoc));
	*assocList = NULL;
    }
    for (a = assocList; *a != NULL; a++) {
	listSize++;
	if (strcmp((*a)->name, name) == 0) {
	    (*a)->value = realloc((*a)->value, strlen(value) + 1);
	    strcpy((*a)->value, value);
	    return;
	}
    }
    listSize++;
    assocList = realloc(assocList, sizeof(assoc) * (listSize + 1));
    assocList[listSize] = NULL;
    a = assocList + listSize - 1;
    *a = assocList[listSize - 1] = malloc(sizeof(struct assoc_s));
    (*a)->name = strdup(name);
    (*a)->value = strdup(value);
}

#define MAX_OTHERS 30
static char otherConversionsTable[MAX_OTHERS][MAX_ENTRY] = {
    ""
};

static int addToOtherConversionsTable(char *buf) {
    int i, retval = -1;
    
    for (i = 0; i < MAX_OTHERS - 1 && *otherConversionsTable[i]; i++) {
        if (strcmp(otherConversionsTable[i], buf) == 0) {
            retval = i;
            goto done;
        }
    }

    if (i < MAX_OTHERS - 1) {
        dcalc_cpy(otherConversionsTable[i], buf, MAX_ENTRY);
        *otherConversionsTable[i+1] = 0;
        retval = i;
    } else { /* overwrite the oldest */
        dcalc_cpy(otherConversionsTable[0], buf, MAX_ENTRY);
        retval = 0;
    }
done:
    return retval;
}

static char *nextToken(char *b) {
    while (!isspace(*b))
        b++;
    while (isspace(*b))
        *b++ = 0;
    return b;
}

void gui_read_custom_settings(void) {
    FILE *f = NULL;
	char *name;

    f = dcalc_open_config_file("custom", "r");
    if (f) {
        char buf[MAX_ENTRY], orig_buf[MAX_ENTRY];
        while (fgets(buf, MAX_ENTRY, f) != NULL) {
            char *b = buf;

            dcalc_cpy(orig_buf, buf, MAX_ENTRY);
            
            if (*b && buf[strlen(buf) - 1] == '\n')
                buf[strlen(buf) - 1] = 0;

            if (*b == '#')
                continue;

            name = b;
            if (*b) {
                b = nextToken(b);
                if (strcmp(name, "button") == 0) {
                    // button int|float|sci|fin|sta|pro i j label expr
                    char *ichar, *jchar, *button_group_name, *label, *expr;
                    gtk_dcalc_button *button_group = NULL;
                    int i, j;
                    
                    button_group_name = b;
                    b = nextToken(b);
                    ichar = b;
                    b = nextToken(b);
                    jchar = b;
                    b = nextToken(b);
                    label = b;
                    b = nextToken(b);
                    expr = b;

                    if (strcmp("int", button_group_name) == 0)
                        button_group = gtk_dcalc_int_buttons;
                    else if (strcmp("float", button_group_name) == 0)
                        button_group = gtk_dcalc_float_buttons;
                    else if (strcmp("sci", button_group_name) == 0)
                        button_group = gtk_dcalc_sci_buttons;
                    else if (strcmp("fin", button_group_name) == 0)
                        button_group = gtk_dcalc_fin_buttons;
                    else if (strcmp("sta", button_group_name) == 0)
                        button_group = gtk_dcalc_stat_buttons;
                    else if (strcmp("pro", button_group_name) == 0)
                        button_group = gtk_dcalc_prog_buttons;

                    i = atoi(ichar);
                    j = atoi(jchar);
                    
                    if (i < 0 || i > 10 || j < 0 || j > 10) {
                        fprintf(stderr, "gdcalc: button coords are out of range in '%s'\n", orig_buf);
                        continue;
                    }

                    if (button_group == NULL) {
                        fprintf(stderr, "gdcalc: unknown button group '%s' in '%s'\n", button_group_name, orig_buf);
                        continue;
                    }

                    for (gtk_dcalc_button *b = button_group; b->i >= 0; b++) {
                        if (b->i == i && b->j == j) {
                            if (b->command != CUSTOM) {
                                fprintf(stderr, "gdcalc: can't override a builtin button in '%s'\n", orig_buf);
                            } else {
                                dcalc_cpy(b->label, label, BUTTON_LABEL_LEN);
                                dcalc_cpy(b->tooltip, expr, BUTTON_TOOLTIP_LEN);
                            }
                        }
                    }
                }
            }
        }
        fclose(f);
    }
}
 
void gui_read_settings(void) {
    FILE *f = NULL;
	char *name, *value;

    f = dcalc_open_config_file("gdcalcrc", "r");
    if (f) {
        char buf[MAX_ENTRY];
        while (fgets(buf, MAX_ENTRY, f) != NULL) {
            char *b = buf;
            
            if (*b && buf[strlen(buf) - 1] == '\n')
                buf[strlen(buf) - 1] = 0;

            if (*b == '#')
                continue;

            name = b;
            b = nextToken(b);
            if (*b) {
                if (strcmp(name, "registers") == 0) {
                    gtk_dcalc_registers_popup = (GtkWidget *) (long) atoi(b); /* I know, it's a clunker, but it works */
#ifdef HAS_ALGEBRAIC_MODE
                } else if (strcmp(name, "algebraicmode") == 0) {
                    dcalc_algebraic_mode = atol(b);
                } else if (strcmp(name, "lasteval") == 0) {
                    dcalc_cpy(dcalc_last_eval, b, MAX_ENTRY);
#endif /* HAS_ALGEBRAIC_MODE */
                } else if (strcmp(name, "OtherConversion") == 0) {
                    addToOtherConversionsTable(b);
                } else {
                    value = b;
                    saveNameString(name, value);
                }
            }
        }
        fclose(f);
	}
}

void save_custom_buttons(FILE *f, gtk_dcalc_button *b, char *group) {
    for (; b->i >= 0; b++) {
        if ((b->command == CUSTOM) && (b->label[0] != 0) && (b->tooltip[0] != 0))
            fprintf(f, "button %s %d %d %s %s\n", group, b->i, b->j, b->label, b->tooltip);
    }
}

void gui_save_custom_settings() {
    FILE *f = NULL;

    f = dcalc_open_config_file("custom", "w");
    if (f) {
        fprintf(f, "# %s\n", "GDCALC Version " VERSION);
        save_custom_buttons(f, gtk_dcalc_int_buttons, "int");
        save_custom_buttons(f, gtk_dcalc_float_buttons, "float");
        save_custom_buttons(f, gtk_dcalc_prog_buttons, "prog");
        save_custom_buttons(f, gtk_dcalc_sci_buttons, "sci");
        save_custom_buttons(f, gtk_dcalc_fin_buttons, "fin");
        save_custom_buttons(f, gtk_dcalc_stat_buttons, "stat");
        fclose(f);
    }
}

void gui_save_settings() {
    FILE *f = NULL;
	assoc *a = NULL;
	char *name, *value;

    f = dcalc_open_config_file("gdcalcrc", "w");
    if (f) {
        fprintf(f, "# %s\n", "GDCALC Version " VERSION);
        fprintf(f, "# don't edit this file - gdcalc will overwrite it\n");
		while ((a = getNextNameValue(a, &name, &value)) != NULL)
			fprintf(f, "%s %s\n", name, value);
        for (int i = 0; *otherConversionsTable[i]; i++)
            fprintf(f, "OtherConversion %s\n", otherConversionsTable[i]);

        fprintf(f, "registers %d\n", gtk_dcalc_registers_popup? 1: 0);
        /* Rev 2.8 on: */
#ifdef HAS_ALGEBRAIC_MODE
        fprintf(f, "algebraicmode %d\n", dcalc_algebraic_mode? 1: 0);
        fprintf(f, "lasteval %s\n", dcalc_last_eval);
#endif /* HAS_ALGEBRAIC_MODE */

        fclose(f);
	}
}

/* trim all white space from buf (for units) */
static void trim_all_spaces(char const *s) {
    char *d = (char *)s;
    while (*s) {
        if (!isspace(*s))
            *d++ = *s;
        s++;
    }
    *d = 0;
}

static void convertX(char const *from_in, char const *to_in) {
#ifdef unix
    int fd[2], pid;
#endif
    char from[MAX_ENTRY], to[MAX_ENTRY];

    if (dcalc_mode == PROG) {
        gui_msg("No conversions in PROG mode");
        return;
    }
    
    dcalc_cpy(from, from_in, MAX_ENTRY);
    dcalc_cpy(to, to_in, MAX_ENTRY);
    
    trim_all_spaces(from);
    trim_all_spaces(to);

    if (strlen(from) == 0 || strlen(to) == 0) {
        gui_msg("Bad units");
        return;
    }

    dcalc_stop_entering();

#ifdef unix
    if (pipe(fd) < 0)
        gui_msg("Can't open a pipe.");
    else {
        double oldX = dcalc_xfReg;
        char *argv[5];
        char fromarg[MAX_ENTRY * 2];
        char toarg[MAX_ENTRY];
        BOOLEAN is_neg = FALSE;

        if (oldX < 0.0) {
            is_neg = TRUE;
            oldX = -oldX;
        }

        sprintf(fromarg, "%f %s", oldX, from);
        dcalc_cpy(toarg, to, MAX_ENTRY);

        /* temperature special case - "non-linear" */
        if (strncmp(from, "deg", 3) == 0) {
            is_neg = FALSE;
            if (strcmp(from, "degcelsius") == 0) {
                sprintf(fromarg, "tempC(%f)", dcalc_xfReg);
            } else if (strcmp(from, "degfahrenheit") == 0) {
                sprintf(fromarg, "tempF(%f)", dcalc_xfReg);
            } else {
                sprintf(fromarg, "tempK(%f)", dcalc_xfReg);
            }
            if (strcmp(to, "degcelsius") == 0) {
                strcpy(toarg, "tempC");
            } else if (strcmp(to, "degfahrenheit") == 0) {
                strcpy(toarg, "tempF");
            } else {
                strcpy(toarg, "tempK");
            }
            
        }
        
        argv[0] = "/usr/bin/units";
        argv[1] = "-o%.16g";
        argv[2] = fromarg;
        argv[3] = toarg;
        argv[4] = 0;
        if ((pid = vfork()) == -1) {
            gui_msg("Fork failed.");
        } else if (pid == 0) { /* child - put stdout on pipe */
            close(fd[0]); /* child does not need to read from pipe */
            close(1); /* set stdout to the pipe */
            if (dup(fd[1]) == -1) _exit(1);
            close(fd[1]);
            execvp(argv[0], argv);
            _exit(1); /* abandon hope all ye who get here */
        } else { /* PARENT - read from pipe */
            FILE *p;
            close(fd[1]); /* parent does not write to pipe */
            if ((p = fdopen(fd[0], "r")) == NULL) {
                gui_msg("Can't read from pipe");
            } else {
                double newX;
                int numRead = 0, status, bytesRead = 0;
                char dcalc_inbuf[MAX_ENTRY * 3]; /* *3 to shut the compiler up! */

                while (fgets(dcalc_inbuf, MAX_ENTRY, p) != NULL) {
                    bytesRead += strlen(dcalc_inbuf);
                    if (!numRead) {
                        if (strchr(dcalc_inbuf, '*') == 0)
                            numRead = sscanf(dcalc_inbuf, "%lf", &newX);
                        else
                            numRead = sscanf(dcalc_inbuf, " * %lf", &newX);
                    }
                }
                wait(&status);
                if (is_neg)
                    newX = -newX;

                if (numRead == 1 &&
                    WIFEXITED(status) &&
                    WEXITSTATUS(status) == 0) {
                    double xf;

                    xf = dcalc_popf();
                    dcalc_pushf(newX);
                    if (fabs(xf) < 0.0001)
                        sprintf(dcalc_inbuf, "Conversion factor %s to %s", from, to);
                    else
                        sprintf(dcalc_inbuf, "Converted %s to %s", from, to);
                    gui_msg(dcalc_inbuf);
                    gui_dispnums();
                } else if (bytesRead)
                    /* Note: if you are tempted to catch units's error
                       message be aware that as of version 1.55 it
                       does not use stderr! */
                    gui_msg("Bad units");
                else
                    gui_msg("Can't find the units program - please install it!");

                fclose(p); /* no need to close fd[1] too */
            }
        }
    }
#else
    gui_msg("Conversions not supported here");
#endif
}

/* ref: https://stackoverflow.com/questions/16630528/trying-to-populate-a-gtkcombobox-with-model-in-c
   https://python-gtk-3-tutorial.readthedocs.io/en/latest/combobox.html */
static GtkComboBox *createCombo(char *combo_name,
                                char table[][MAX_ENTRY],
                                char *item,
                                int allow_freeform) {
    GtkListStore *liststore;
    GtkComboBox *retval;
    int index, i;
    GtkEntry *entry;
    
    liststore = gtk_list_store_new(1, G_TYPE_STRING);
	retval = (GtkComboBox *) gtk_dcalc_get_widget("createCombo", combo_name);

    index = 1;
	for (i = 0; *table[i] != 0; i++) {
        if (strcmp(item, table[i]) == 0)
            index = i;
		gtk_list_store_insert_with_values(liststore, NULL, -1,
                                          0, table[i],
                                          -1);
    }
    
    gtk_combo_box_set_model(retval, GTK_TREE_MODEL(liststore));
    g_object_unref(liststore);

#ifdef i_think_maybe_this_isnt_needed // FIXME
    GtkCellRenderer *column;
    column = gtk_cell_renderer_text_new();
    gtk_cell_layout_clear(GTK_CELL_LAYOUT(retval));
    gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(retval), column, TRUE);
    gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(retval), column,
                                   "text", 0,
                                   NULL);
#endif

    gtk_combo_box_set_entry_text_column(GTK_COMBO_BOX(retval), 0);
    gtk_combo_box_set_active(GTK_COMBO_BOX(retval), index);
    entry = (GtkEntry *)gtk_bin_get_child(GTK_BIN(retval));
    if (allow_freeform) {
        gtk_widget_set_can_focus(GTK_WIDGET(entry), 1);
    } else {
        gtk_widget_set_can_focus(GTK_WIDGET(entry), 0);
    }

    return retval;
}

static char *get_combo_value(char table[][MAX_ENTRY], GtkComboBox *combo) {
    char *retval = NULL;
    int active;
    
    active = gtk_combo_box_get_active(combo);
    if (active < 0) {
        GtkEntry *entry = (GtkEntry *)gtk_bin_get_child(GTK_BIN(combo));
        retval = (char *)gtk_entry_get_text(entry);
    } else
        retval = table[active];
    return retval;
}

static int doConvert(char table[][MAX_ENTRY],
                     char *title,
                     char *pFrom,
                     char *pTo,
                     int allow_freeform) {
	GtkWidget *convert_dialog;
	GtkLabel *convert_label;
	GtkComboBox *from_combo, *to_combo;
	int reply, retval = 1;
	const char *from, *to;
    
    gui_msg("");
	convert_dialog = gtk_dcalc_get_widget("doConvert", "convert_dialog");

    /* "from" combo box */
    from_combo = createCombo("convert_from_combo", table, pFrom, allow_freeform);
    
    /* "to" combo box */
    to_combo = createCombo("convert_to_combo", table, pTo, allow_freeform);
    
	convert_label = (GtkLabel *) gtk_dcalc_get_widget("doConvert", "convert_label");
	gtk_label_set_text(convert_label, title);

	reply = gtk_dialog_run(GTK_DIALOG(convert_dialog));
	from = get_combo_value(table, from_combo);
	to   = get_combo_value(table, to_combo);
	if (reply == 2)
        retval = 0; /* CANCEL */
    else {
        dcalc_cpy(pFrom, from, MAX_ENTRY);
        dcalc_cpy(pTo, to, MAX_ENTRY);
        if (reply == 0)
            convertX(from, to);
        else /* INVERSE */
            convertX(to, from);
	}

	gtk_widget_hide(convert_dialog);
	return retval;
}

void
on_length_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	char table[][MAX_ENTRY] = {
		"mile",
		"nautical mile",
		"furlong",
		"chain",
		"yard",
		"feet",
		"inch",
		"thou",
		"kilometer",
		"metre",
		"cm",
		"mm",
		"micron",
		"",
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("LengthConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("LengthConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Length Conversion", from, to, 0)) {
		saveNameString("LengthConversionFrom", from);
		saveNameString("LengthConversionTo", to);
	}
}

void
on_area_activate(GtkMenuItem     *menuitem,
				 gpointer         user_data)
{
	char table[][MAX_ENTRY] = {
		"mile^2",
		"acre",
		"yard^2",
		"feet^2",
		"inch^2",
		"km^2",
		"hectare",
		"metres^2",
		"cm^2",
		"mm^2",
		""
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("AreaConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("AreaConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Area conversion", from, to, 0)) {
		saveNameString("AreaConversionFrom", from);
		saveNameString("AreaConversionTo", to);
	}
}

void
on_volume_activate(GtkMenuItem     *menuitem,
				   gpointer         user_data)
{
	char table[][MAX_ENTRY] = {
		"br gallons",
		"br pints",
		"gallons",
		"pints",
		"feet^3",
		"inches^3",
		"fluid ounce",
		"metre^3",
		"litres",
		"cc",
		""
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("VolumeConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("VolumeConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Volume conversion", from, to, 0)) {
		saveNameString("VolumeConversionFrom", from);
		saveNameString("VolumeConversionTo", to);
	}

}

void
on_mass_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	char table[][MAX_ENTRY] = {
		"tons",
		"cwts",
		"stones",
		"pounds",
		"ounces",
		"grains",
		"tonnes",
		"kg",
		"grams",
		""
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("MassConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("MassConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Mass conversion", from, to, 0)) {
		saveNameString("MassConversionFrom", from);
		saveNameString("MassConversionTo", to);
	}

}

void
on_speed_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	char table[][MAX_ENTRY] = {
		"knots",
		"mph",
		"feet/s",
		"kph",
		"meters/s",
		""
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("SpeedConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("SpeedConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Speed conversion", from, to, 0)) {
		saveNameString("SpeedConversionFrom", from);
		saveNameString("SpeedConversionTo", to);
	}

}

void
on_fuel_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	char table[][MAX_ENTRY] = {
		"miles / gallon",
		"miles / br gallon",
		"litres/100 km",
		""
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("FuelConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("FuelConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Fuel conversion", from, to, 0)) {
		saveNameString("FuelConversionFrom", from);
		saveNameString("FuelConversionTo", to);
	}

}

void
on_pressure_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	char table[][MAX_ENTRY] = {
		"atmospheres",
		"psi",
		"millibars",
		"mm Hg",
		"pascals",
		""
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("PressureConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("PressureConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Convert pressure", from, to, 0)) {
		saveNameString("PressureConversionFrom", from);
		saveNameString("PressureConversionTo", to);
	}

}

void
on_temperature_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	char table[][MAX_ENTRY] = {
		"deg fahrenheit",
		"deg celsius",
		"deg kelvin",
		""
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("TempConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("TempConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Convert temperature range", from, to, 0)) {
		saveNameString("TempConversionFrom", from);
		saveNameString("TempConversionTo", to);
	}

}

void
on_currency_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

	char table[][MAX_ENTRY] = {
		"australia dollar",
		"newzealand dollar",
		"united kingdom pound",
		"united states dollar",
		"- Europe -",
		"austria schilling",
		"belgium franc",
		"britain pound",
		"czechko runa",
		"denmark krone",
		"euro",
		"finland markka",
		"france franc",
		"germany mark",
		"great britain pound",
		"greece drachma",
		"hungary forint",
		"ireland punt",
		"israel shekel",
		"italy lira",
		"netherlands guilder",
		"norway krone",
		"poland zloty",
		"portugal escudo",
		"russia ruble",
		"slovakia koruna",
		"south africa rand",
		"spain peseta",
		"sweden krona",
		"switzerland franc",
		"united kingdom pound",
		"- Asia -",

		"china yuan",
		"hongkong dollar",
		"india rupee",
		"indonesia rupiah",
		"japan yen",
		"malaysia ringgit",
		"pakistan rupee",
		"philippines peso",
		"singapore dollar",
		"south korea won",
		"taiwan dollar",
		"thailand baht",
		"- Middle East -",

		"egypt pound",
		"jordan dinar",
		"lebanon pound",
		"saudi arabia riyal",
		"turkey lira",
		"united arab dirham",
		"- America -",
		"argentina peso",
		"brazil real",
		"canada dollar",
		"chile peso",
		"colombia peso",
		"ecuador sucre",
		"mexico peso",
		"peru new sol",
		"united states dollar",
		"uruguay new peso",
		"venezuela bolivar",
		""
	};
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("MoneyConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("MoneyConversionTo"), MAX_ENTRY);
	if (doConvert(table, "Convert currency", from, to, 0)) {
		saveNameString("MoneyConversionFrom", from);
		saveNameString("MoneyConversionTo", to);
	}
}
       
void
on_other_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	char from[MAX_ENTRY], to[MAX_ENTRY];

	dcalc_cpy(from, getNameString("OtherConversionFrom"), MAX_ENTRY);
	dcalc_cpy(to, getNameString("OtherConversionTo"), MAX_ENTRY);
	doConvert(otherConversionsTable, "Convert more or less anything (see units(1))", from, to, 1);
    saveNameString("OtherConversionFrom", from);
    saveNameString("OtherConversionTo", to);
    addToOtherConversionsTable(from);
    addToOtherConversionsTable(to);
}

void
on_help_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}

void
on_documentation_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}

void
on_manual_activate                      (GtkMenuItem     *menuitem,
                                         gpointer         user_data)
{
    dcalc_stop_entering();

    char *argv[3];
    int pid;
    
    argv[0] = "xdg-open";
    argv[1] = PACKAGE_DATA_DIR "/doc/gdcalc/manual_en.html";
    argv[2] = NULL;
    if ((pid = vfork()) == -1) {
        gui_msg("Fork failed.");
    } else if (pid == 0) { /* child */
        execvp(argv[0], argv);
        _exit(1); /* abandon hope all ye who get here */
    }
}

void
on_website_activate                      (GtkMenuItem     *menuitem,
                                          gpointer         user_data)
{
    dcalc_stop_entering();

    char *argv[3];
    int pid;
    
    argv[0] = "xdg-open";
    argv[1] = "https://gitlab.com/wef/gdcalc";
    argv[2] = NULL;
    if ((pid = vfork()) == -1) {
        gui_msg("Fork failed.");
    } else if (pid == 0) { /* child */
        execvp(argv[0], argv);
        _exit(1); /* abandon hope all ye who get here */
    }
}

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget *about;

  about = gtk_dcalc_get_widget("on_about_activate", "about_dialog");
  gtk_dialog_run(GTK_DIALOG(about));
  gtk_widget_hide(about);
}

#define LCLICK(x,y) void on_L ## x ## _ ## y ## _clicked \
	(GtkButton *button, gpointer user_data) \
    { dcalc_process(gtk_dcalc_lookup_and_process_button_at(gtk_dcalc_l_buttons, x, y)); }

LCLICK(0,0)
LCLICK(0,1)
LCLICK(0,2)
LCLICK(0,3)
LCLICK(0,4)
LCLICK(0,5)
LCLICK(0,6)
LCLICK(0,7)

LCLICK(1,0)
LCLICK(1,1)
LCLICK(1,2)
LCLICK(1,3)
LCLICK(1,4)
LCLICK(1,5)
LCLICK(1,6)
LCLICK(1,7)

LCLICK(2,0)
LCLICK(2,1)
LCLICK(2,2)
LCLICK(2,3)
LCLICK(2,4)
LCLICK(2,5)
LCLICK(2,6)
LCLICK(2,7)

void
on_R0_0_clicked                        (GtkButton       *button,
                                        gpointer         user_data)
{
	int b = gtk_dcalc_lookup_and_process_button_at(gtk_dcalc_r_buttons, 0, 0);
	GtkWidget *w;

	w = gtk_dcalc_get_widget("on_R0_0_clicked",
							(b==SCI)? "scientific_mode":
							(b==FIN)? "financial_mode":
							(b==STAT)? "statistics_mode":
							"programming_mode");
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(w), 1);
}

void
on_R0_1_clicked                        (GtkButton       *button,
                                        gpointer         user_data)
{
	int b = gtk_dcalc_lookup_and_process_button_at(gtk_dcalc_r_buttons, 0, 1);
	GtkWidget *w;

	w = gtk_dcalc_get_widget("on_R0_1_clicked",
							(b==SCI)? "scientific_mode":
							(b==FIN)? "financial_mode":
							(b==STAT)? "statistics_mode":
							"programming_mode");
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(w), 1);
}

void
on_R0_2_clicked                          (GtkButton       *button,
                                        gpointer         user_data)
{
	int b = gtk_dcalc_lookup_and_process_button_at(gtk_dcalc_r_buttons, 0, 2);
	GtkWidget *w;

	w = gtk_dcalc_get_widget("on_R0_2_clicked",
							(b==SCI)? "scientific_mode":
							(b==FIN)? "financial_mode":
							(b==STAT)? "statistics_mode":
							"programming_mode");
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(w), 1);
}

#define RCLICK(x,y) void on_R ## x ## _ ## y ## _clicked \
	(GtkButton *button, gpointer user_data) \
    { dcalc_process(gtk_dcalc_lookup_and_process_button_at(gtk_dcalc_r_buttons, x, y)); }

RCLICK(0,3)
RCLICK(0,4)
RCLICK(0,5)
RCLICK(1,0)
RCLICK(1,1)
RCLICK(1,2)
RCLICK(1,3)
RCLICK(1,4)
RCLICK(1,5)
RCLICK(2,0)
RCLICK(2,1)
RCLICK(2,2)
RCLICK(2,3)
RCLICK(2,4)
RCLICK(2,5)
RCLICK(3,0)
RCLICK(3,1)
RCLICK(3,2)
RCLICK(3,3)
RCLICK(3,4)
RCLICK(3,5)
RCLICK(4,0)
RCLICK(4,1)
RCLICK(4,2)
RCLICK(4,3)
RCLICK(4,4)
RCLICK(4,5)
RCLICK(5,0)
RCLICK(5,1)
RCLICK(5,2)
RCLICK(5,3)
RCLICK(5,4)
RCLICK(5,5)

void
on_Minus_clicked                       (GtkButton       *button,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode) {
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("ans ");
		}
		gtk_dcalc_append_x("-");
	} else
		dcalc_process(MINUS);

}

void
on_Mult_clicked                        (GtkButton       *button,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode) {
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("ans ");
		}
		gtk_dcalc_append_x("*");
	} else
		dcalc_process(MULT);

}

void
on_Divide_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode) {
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("ans ");
		}
		gtk_dcalc_append_x("/");
	} else
		dcalc_process(DIVIDE);

}

void
on_Plus_clicked                        (GtkButton       *button,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode) {
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("ans ");
		}
		gtk_dcalc_append_x("+");
	} else
		dcalc_process(PLUS);
}

void
on_Enter_clicked                       (GtkButton       *button,
                                        gpointer         user_data)
{
  dcalc_process(ENTER);
}

void
on_LeftBrace_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode) {
        if (dcalc_start_entering()) {
            gtk_dcalc_set_x("");
        }
        gtk_dcalc_append_x("(");
    }
}


void
on_RightBrace_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode) {
        if (dcalc_start_entering()) {
            gtk_dcalc_set_x("");
        }
        gtk_dcalc_append_x(")");
    }
}


void
on_Equals_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{
	dcalc_cpy(dcalc_last_eval, gtk_dcalc_get_x(), MAX_ENTRY);
	if (dcalc_mode == PROG)
		dcalc_pop();
	else
		dcalc_popf();
	dcalc_process(EQUALS);
}

void
on_Ans_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode) {
        if (dcalc_start_entering()) {
            gtk_dcalc_set_x("ans ");
        } else {
            gtk_dcalc_append_x(" ans ");
        }
    }
}


void
on_places_ok_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

}

void
on_places_cancel_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}

void
on_convert_ok_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}

void
on_convert_reverse_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{

}

void
on_convert_cancel_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}

void
on_about_ok_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{

}

void
on_dialog_ok_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
}

void
on_dialog_cancel_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
}


void
on_mem_cancel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
}

static void mem_button(int i) {
    if (gtk_dcalc_registers_popup) {
        GtkLabel *mem_label = GTK_LABEL(gtk_dcalc_get_widget("mem_button", "mem_label"));
        gtk_label_set_text(mem_label, gtk_dcalc_registers_label);
        g_signal_emit_by_name(G_OBJECT(gtk_dcalc_mem_dialog), "response");
    } else
        gtk_widget_hide(gtk_dcalc_mem_dialog);
    gtk_dcalc_mem_value = i;
}

gboolean
on_custom_button_release_event         (GtkWidget *widget,
                                        GdkEventButton *event,
                                        gpointer user_data)
{
    gtk_dcalc_button *b = (gtk_dcalc_button *)user_data;
    if (event->button == 3) {
        gtk_dcalc_customise_button(b);
        return TRUE;
    }
    return FALSE;
}

gboolean
on_mem0_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('0');
  return FALSE;
}

gboolean
on_mem1_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('1');
  return FALSE;
}

gboolean
on_mem2_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('2');
  return FALSE;
}

gboolean
on_mem3_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('3');
  return FALSE;
}

gboolean
on_mem4_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('4');
  return FALSE;
}

gboolean
on_mem5_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('5');
  return FALSE;
}

gboolean
on_mem6_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('6');
  return FALSE;
}

gboolean
on_mem7_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('7');
  return FALSE;
}

gboolean
on_mem8_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('8');
  return FALSE;
}

gboolean
on_mem9_button_release_event           (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  mem_button('9');
  return FALSE;
}

static void
do_copy() {
    int start, end;
    if (gtk_editable_get_selection_bounds( GTK_EDITABLE(gtk_dcalc_entry_x), &start, &end) == FALSE) {
        gtk_editable_select_region( GTK_EDITABLE(gtk_dcalc_entry_x), 0, -1);
    }
	g_signal_emit_by_name( G_OBJECT(gtk_dcalc_entry_x), "copy-clipboard");
    gui_msg("X copied to clipboard");
}

static void
do_paste() {
	g_signal_emit_by_name( G_OBJECT(gtk_dcalc_entry_x), "paste-clipboard");
    gui_msg("Clipboard copied to X");
}

/*
  return TRUE means 'we've handled it'
  return FALSE means 'pass it to the next handler to dcalc_process
*/
gboolean
on_mem_dialog_key_press_event         (GtkWidget       *widget,
                                       GdkEventKey     *event,
                                       gpointer         user_data)
{
	int inval = event->keyval;
    if ((inval >= GDK_KEY_0) && (inval <= GDK_KEY_9)) {
        mem_button(inval - GDK_KEY_0 + '0');
        return TRUE;
    }
    switch(inval) {
    case GDK_KEY_plus:     mem_button('+'); return TRUE;
    case GDK_KEY_minus:    mem_button('-'); return TRUE;
    case GDK_KEY_asterisk: mem_button('*'); return TRUE;
    case GDK_KEY_slash:    mem_button('/'); return TRUE;
    case GDK_KEY_Escape:   mem_button(0);   return TRUE;
    }
    return FALSE;
}

/*
  return TRUE means 'we've handled it'
  return FALSE means 'pass it to the next handler to dcalc_process
*/
gboolean
on_main_window_key_press_event         (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{
	int inval;

#ifdef DEBUG
      printf("on_main_window_key_press_event got ");
      if (event->state & GDK_SHIFT_MASK)
      printf("S-");
      if (event->state & GDK_CONTROL_MASK)
      printf("C-");
      if (event->state & GDK_MOD1_MASK)
      printf("A-");
      if (event->state & GDK_MOD2_MASK)
      printf("M-");
      if (event->keyval >= 0x20 && event->keyval<=0xff)
      printf("%c", event->keyval);
      else
      printf("0x%x", event->keyval);
      printf("\n");
#endif
	inval = event->keyval;

	if (dcalc_mode == PROG && dcalc_intmode == ASCIIM) {

        /* raw_mode: dcalc_process the keycode & eat the event */
        if (event->state & GDK_CONTROL_MASK) {
            if (inval >= 'a' && inval <= 'z')
                inval -= 0x20;
            inval -= 0x40;
        }

        if (event->state & GDK_MOD1_MASK)
            inval |= 0x80;

		switch (inval) {
		case GDK_KEY_Escape:    	inval = 27; break;
		case GDK_KEY_KP_Enter:
		case GDK_KEY_Return:		inval = 10; break;
		case GDK_KEY_Tab:	    	inval = 9;  break;
		case GDK_KEY_BackSpace: 	inval = 8;  break;
		case GDK_KEY_KP_Add:		inval = '+'; break;
		case GDK_KEY_KP_Subtract:	inval = '-'; break;
		case GDK_KEY_KP_Multiply:	inval = '*'; break;
		case GDK_KEY_KP_Divide:		inval = '/'; break;
		default:	break;
		}
	} else {
        /* accelerators:*/
        if (event->state & (GDK_CONTROL_MASK|GDK_MOD1_MASK))
            return FALSE;
            
		inval = toupper(inval);
		switch (inval) {
		case GDK_KEY_KP_Enter:
		case GDK_KEY_Return:
		case '=':
		case 10: /* newline */
            if (dcalc_algebraic_mode && dcalc_entering)
                inval = EQUALS;
            else
                inval = ENTER;
            break;

		case 3:		/* ^c - copy X to clipboard */
			do_copy();
			inval = NOP;
			break;
		case 22:	/* ^v - paste from clipboard to X */
			do_paste();
			inval = NOP;
			break;
		case 17:	/* ^q - quit */
			inval = QUIT;
			break;
		default:	
			if (!dcalc_algebraic_mode)
				switch (inval) {
				case '+':	inval = PLUS;		break;
				case '-':	inval = MINUS;		break;
				case '*':	inval = MULT;		break;
				case '/':	inval = DIVIDE;		break;
				case '%':	inval = PERCENT;	break;
				case 'p':	inval = PI;			break;
				case '^':	inval = YTOX;		break;
				case ',':	inval = CHS;		break;
				}
			break;
		}
	}

	/* map input characters to commands */
	switch(inval) {
	case GDK_KEY_Escape:    	inval = INV; break;
	case GDK_KEY_Tab:	    	inval = 9;  break;
	case GDK_KEY_Up:
	case GDK_KEY_Page_Up:		inval = ENTER; break;
	case GDK_KEY_Down:
	case GDK_KEY_Page_Down:		inval = ROLLDOWN; break;

	default:	
		if (!dcalc_algebraic_mode)
			switch(inval) {
			case GDK_KEY_Left:
			case GDK_KEY_BackSpace: 	inval = 8;  break;
			case GDK_KEY_KP_Add:		inval = PLUS; break;
			case GDK_KEY_KP_Subtract:	inval = MINUS; break;
			case GDK_KEY_KP_Multiply:	inval = MULT; break;
			case GDK_KEY_KP_Divide:		inval = DIVIDE; break;
			case GDK_KEY_KP_Insert:
			case GDK_KEY_KP_0:			inval = '0'; break;
			case GDK_KEY_KP_End:
			case GDK_KEY_KP_1:			inval = '1'; break;
			case GDK_KEY_KP_Down:
			case GDK_KEY_KP_2:			inval = '2'; break;
			case GDK_KEY_KP_Page_Down:
			case GDK_KEY_KP_3:			inval = '3'; break;
			case GDK_KEY_KP_Left:
			case GDK_KEY_KP_4:			inval = '4'; break;
			case GDK_KEY_KP_Begin:
			case GDK_KEY_KP_5:			inval = '5'; break;
			case GDK_KEY_KP_Right:
			case GDK_KEY_KP_6:			inval = '6'; break;
			case GDK_KEY_KP_Home:
			case GDK_KEY_KP_7:			inval = '7'; break;
			case GDK_KEY_KP_Up:
			case GDK_KEY_KP_8:			inval = '8'; break;
			case GDK_KEY_KP_Page_Up:
			case GDK_KEY_KP_9:			inval = '9'; break;
			case GDK_KEY_KP_Delete:
			case GDK_KEY_KP_Decimal:	inval = '.'; break;
				/* ignore these: */
			case GDK_KEY_Shift_L:
			case GDK_KEY_Shift_R:
			case GDK_KEY_Control_L:
			case GDK_KEY_Control_R:
			case GDK_KEY_Meta_L:
			case GDK_KEY_Meta_R:
			case GDK_KEY_Alt_L:
			case GDK_KEY_Alt_R:
			case GDK_KEY_Num_Lock:
			case GDK_KEY_Caps_Lock:
			case GDK_KEY_Shift_Lock:
				return TRUE;
			}
		break;
	}

	if (!dcalc_algebraic_mode) {
		dcalc_process(inval);
        return TRUE;
    }

    /* algebraic dcalc_mode */
    switch (inval) {
    case ENTER:
        dcalc_exec_enter();
        break;
            
    case EQUALS:
        dcalc_last_was_fin_key = 0;
        // g_signal_emit_stop_by_name(G_OBJECT (widget), "key_press_event");
        on_Equals_clicked(NULL, NULL);
        return TRUE;
    default:
        if (dcalc_start_entering()) {
            gtk_dcalc_set_x("");
        }
        dcalc_last_was_fin_key = 0;
        return FALSE; /* ... just echo to the buffer */
    }
	return TRUE;
}

void
on_close_registers(GtkButton       *button,
                   gpointer         user_data) {
    if (gtk_dcalc_registers_popup)
        gtk_dcalc_toggle_registers();
    else
        gtk_widget_hide(gtk_dcalc_get_widget("on_close_registers", "gtk_dcalc_mem_dialog"));
    gui_msg("");
}

/* to prevent recursion as we can't get at the signal handler: */
int on_register_window_activate_enabled = 1;

void
on_register_window_activate(GtkMenuItem     *menuitem,
							gpointer         user_data) {

    if (on_register_window_activate_enabled)
        gtk_dcalc_toggle_registers();
}

gboolean
on_entry_L_button_release_event       (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode && dcalc_entering)
		gtk_dcalc_append_x("L");
	else
		dcalc_process(XNL);
	return 0;
}

gboolean
on_entry_T_button_release_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode && dcalc_entering) {
		gtk_dcalc_append_x("T");
	} else
		dcalc_process(XNT);
	return 0;
}

gboolean
on_entry_Z_button_release_event       (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode && dcalc_entering) {
		gtk_dcalc_append_x("Z");
	} else
		dcalc_process(XNZ);
	return 0;
}

gboolean
on_entry_Y_button_release_event       (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
	if (dcalc_algebraic_mode && dcalc_entering) {
		gtk_dcalc_append_x("ans ");
	} else
		dcalc_process(XNY);
	return 0;
}

void
on_annuity_in_advance_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	dcalc_process(BEGIN);
}

void
on_fixed_point_numbering_activate      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	dcalc_process(FIXFORMAT);
}

void
on_engineering_numbering_activate      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	dcalc_process(ENGFORMAT);
}

void
on_scientific_numbering_activate        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	dcalc_process(SCIFORMAT);
}


void
on_places_activate                    (GtkMenuItem     *menuitem,
									   gpointer         user_data)
{
	dcalc_process(PLACES);
}

void
on_rpn_activate                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	dcalc_process(RPN);
    gtk_dcalc_display_buttons(dcalc_mode);
}

void
on_algebraic_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	dcalc_process(ALGEBRAIC);
    gtk_dcalc_display_buttons(dcalc_mode);
}

gboolean
on_eval_key_press_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

	int inval;
	GtkWidget *eval_ok;

	inval = event->keyval;
	/* printf("on_eval_key_press_event: inval = %d\n", inval); */

	if (inval == GDK_KEY_Return) {
		// gtk_signal_emit_stop_by_name(G_OBJECT (widget), "key_press_event");
		eval_ok = gtk_dcalc_get_widget("on_eval_key_press_event",_("eval_ok"));
		gtk_button_clicked(GTK_BUTTON(eval_ok));
		return TRUE;
	}
	return FALSE;
}

static void
cleanup(char *buf, int i) {
	long lval;
	double dval;
	char *s, *d;

	dcalc_parse_buffer(buf, &lval, &dval);
	dcalc_fmt_base(buf, lval, dval);
	/* remove leading whitespace: */
	for (s = buf; *s && isspace(*s); s++) 
		;
	if (s > buf) {
		for (d = buf; *s; )
			*d++ = *s++;
		*d = 0;
	}
}

/* Yes this is more ugliness - due to ugliness in the way gtk handles
 * copy/paste etc. We get here every time text is put into gtk_dcalc_entry_x. If
 * we are doing it from dCalc we set the global flag
 * on_entry_X_insert_text_enabled. Otherwise we assume we are doing a paste.
 * We also get a chance to tidy up the paste (syncronise with dCalc's
 * understanding of the contents of gtk_dcalc_entry_x) in the _post routine.
 * Yuck.
 */
#define MAX_PASTE 45
char pasteBuf[MAX_PASTE];
int on_entry_X_insert_text_enabled = 1;
void
on_entry_X_insert_text(GtkEditable     *editable,
					   gchar           *new_text,
					   gint             new_text_length,
					   gint            *position,
					   gpointer         user_data)
{
	int i;
	char *s;

	if (!on_entry_X_insert_text_enabled)
		return;

	i = (new_text_length > MAX_PASTE)? MAX_PASTE: new_text_length;
	strncpy(pasteBuf, (char *)new_text, i);
	pasteBuf[i] = 0;

	if (dcalc_algebraic_mode) {
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("");
		}
		gtk_dcalc_append_x(pasteBuf);
	} else {
		cleanup(pasteBuf, 0);

		if (pasteBuf[0] == 0)
			return;

		for (s = pasteBuf; *s; s++) {
			dcalc_process(toupper(*s));
		}
	}
	dcalc_cpy(pasteBuf, gtk_dcalc_get_x(), MAX_PASTE);
}

void
on_entry_X_insert_text_post            (GtkEditable     *editable,
                                        gchar           *new_text,
                                        gint             new_text_length,
                                        gint            *position,
                                        gpointer         user_data)
{

	if (on_entry_X_insert_text_enabled) {
        if (pasteBuf[0])
            gtk_dcalc_set_x(pasteBuf);
    }
}

void
on_copy_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	do_copy();
}


void
on_paste_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{	
	do_paste();
}

void
on_main_window_destroy                 (GtkWidget       *object,
                                        gpointer         user_data)
{
	dcalc_process(QUIT);
}


/* For emacs: */
/* Local Variables: */
/* eval:(setq tab-width 4) */
/* End: */
