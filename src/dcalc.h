/* $Id: dcalc.h,v 1.13 2006/12/30 22:46:07 bhepple Exp $ */

/* These flags control compilation. Define either PC, OPENVIEW or UNIX.
 * For PCs define either CURSES or PCSPECIFIC.
 * For the PC, the best combination is PC and PCSPECIFIC although PC
 * and CURSES allows the curses version to be tested.
 *
 * For non-OPENVIEW UNIX, you must have CURSES set.
 * 
 * 1999.2.3 - the PC version needs 'Turbo C Tools version 5.00' by Blaise
 * Computing, which is unlikely to still be available. IT also needs the
 * PC-dependent source 'ibm.c'. This was last compiled by me in 1994 (???)
 * with Borland's Turbo-C 1.5. As this is just too hard and unlikely to
 * be of further interest, I will no longer support the DOS version.
 * If this is a problem, please contact me:

    Contact info:
    bob.hepple@gmail.com
    https://gitlab.com/wef/gdcalc

    Copyright (C) 1999-2018 Bob Hepple

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING. If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth
    Floor, Boston, MA 02110-1301 USA

 */

#ifdef PC
#undef   CURSES
#undef UNIX
#define  PCSPECIFIC      1
/* Set this if you want a TSR function with mouse capture */
#undef   TSR
#endif

#ifndef HAVE_CONFIG_H
#  define VERSION "3.0"
#endif

/* If you set PCSPECIFIC then you need to compile and link dcalc.c with
 * ibm.c as well as the special version of ivctrl.obj
 * (munged to support the 101-key keyboard) as well as Blaise's tct_t1s.lib.
 * Compile everything with TurboC 1.5 in the small model.
 *
 * For CURSES, you need to compile and link curse.c and dcalc.c with the curses
 * library.
 *
 * For OPENVIEW, you need to compile and link openview.c, ov_ui.c, ov_stubs.c
 * and dcalc.c with the appropriate libraries.
 */

/* End of flags. The previous flags are all you should have to touch. */
/**********************************************************************/

#ifdef UNIX
#define CURSES          1
#undef PC
#undef  PCSPECIFIC
#undef  OPENVIEW
#endif

#define MAX_ENTRY 200

#ifdef __TURBOC__
#define REVERSE_BYTES   1
#else
#define REVERSE_BYTES   0
#endif

#undef REVERSE_BYTES
#define REVERSE_BYTES   1

#define ASCIIZ '\0'
#define SPECIAL ASCIIZ

#ifndef TRUE
#define TRUE    1
#define FALSE   0
#endif

/* Keys we are interested in ... */
#define BELL    7
#define SPACE   ' '
#define COMMA   ','
#define BACKSPACE 8
#define ESCAPE  27

#define COMMAND int

/* COMMANDS (in addition to 'normal' keys ... */
#define MIN_COMMAND 0x200
#define ASCIIM          0x200
#define DEGREE          0x201
#define RADIAN          0x202
#define MODE            0x203
#define PROG            0x204
#define SCI             0x205
#define FIN             0x206
#define STAT            0x207
#define PLACES          0x208
#define FIXFORMAT       0x209
#define ENGFORMAT       0x20a   
#define SCIFORMAT       0x20b
#define BIN             0x20c   
#define OCT             0x20d
#define DEC             0x210
#define HEX             0x211
#define IP              0x212
#define INV             0x213   
#define SIN             0x214
#define COS             0x215
#define TAN             0x216
#define SINH            0x217
#define COSH            0x218
#define TANH            0x219
#define LOGE            0x220
#define LOG10           0x221
#define SQR             0x222
#define ROOT            0x223
#define CUBE            0x224
#define CROOT           0x225
#define FRC             0x226
#define INT             0x227
#define ETOX            0x228
#define RECI            0x229
#define PI              0x22a
#define E               0x22b
#define HMS             0x22c
#define RTOP            0x230
#define DTOR            0x231
#define SUM             0x232
#define SUM0            0x233
#define SUMR            0x234
#define MEAN            0x235
#define MEANY           0x236
#define MEANS           0x237
#define S_DEV           0x238
#define NSTAT           0x239
#define FACT            0x23a
#define PERM            0x23b
#define COMB            0x240
#define LR              0x241
#define CALCY           0x242
#define NFIN            0x243
#define INTST           0x244
#define PMT             0x245
#define PVAL            0x246
#define FVAL            0x247
#define FCLR            0x248
#define BEGIN           0x249
#define DYS             0x24a
#define TDY             0x250   
#define TIMES12         0x251
#define DIVIDE12        0x252
#define LENGTH          0x253   
#define AREA            0x254
#define VOLUME          0x255
#define MASS            0x256
#define SPEED           0x257
#define FUEL            0x260
#define PRES            0x261
#define TEMP            0x262
#define dAND            0x263
#define dOR             0x264
#define dNOT            0x265
#define dXOR            0x266
#define SHIFTYL         0x267
#define SHIFTYR         0x268
#define SHIFTL          0x280
#define SHIFTR          0x281
#define PRIMEF          0x282
#define PLUS            0x283
#define MINUS           0x284
#define MULT            0x285
#define DIVIDE          0x286
#define PERCENT         0x287   
#define PERCENTCH       0x288
#define MODULUS         0x290
#define CHS             0x291
#define YTOX            0x292
#define ROLLDOWN        0x293
#define CLX             0x294
#define QUIT            0x295
#define NEXTKEYS        0x296
#define PREVKEYS        0x297
#define ENTER           0x298
#define XNL             0x299
#define XNT             0x29a
#define XNZ             0x29b
#define XNY             0x29c
#define LASTX           0x29d
#define STORE           0x29e
#define STOPLUS         0x29f
#define STOMINUS        0x2a0
#define STOMULTIPLY     0x2a1
#define STODIVIDE       0x2a2
#define RECALL          0x2b0 
#define COPY            0x2b1 
#define PASTE           0x2b2
#define CLU             0x2b3
#define CLR             0x2b4
#define HELP            0x2b5  
#define REGISTER        0x2b6
#define EXPLAIN         0x2b7
#define CVT             0x2b8
#define RPN             0x2ba
#define ALGEBRAIC       0x2bb
#define POPUP_REG       0x2bc
#define EVAL            0x2bd
#define EQUALS          0x2be
#define LEFTBRACE       0x2bf
#define RIGHTBRACE      0x2c0
#define CUSTOM          0x2c1
#define ANS             0x2c2
#define NOP             0x2d0 /* must be the last in the list */
/* ... END OF COMMANDS */

#define BOOLEAN int

#define NUMREGS 10
#define STAT_R2         1
#define STAT_Y_INT      2
#define STAT_SLOPE      3
#define STAT_NUM_REG    4
#define STAT_SUMX_REG   5
#define STAT_SUMY_REG   6
#define STAT_SUMX2_REG  7
#define STAT_SUMY2_REG  8
#define STAT_SUMXY_REG  9
#define FIN_NUM_REG     5
#define FIN_INT_REG     6
#define FIN_PVAL_REG    7
#define FIN_PMT_REG     8
#define FIN_FVAL_REG    9
#define MSG_SIZE        75
#define KMaxPrimes      9

#define DEF_SIG(x) "DCALC Version " x " - by Bob Hepple"

/* Function prototypes: */

/* imported by dcalc - supplied by curses, hpcalc, ibm, gui_gtk_main, gui_curses etc */
#ifdef __cplusplus
extern "C" {
#endif
    extern void gui_print_string(char *buf);
    extern void gui_print_base(void);
    extern void gui_print_inv(void);
    extern void gui_print_deg(void);
    extern void gui_print_x(char *buf);
#ifdef HAS_ALGEBRAIC_MODE
    extern void gui_add_x(char *buf);
#endif /* HAS_ALGEBRAIC_MODE */
    extern void gui_dispnums(void);
    extern void gui_dispreg(int);
    extern void gui_dispregs(void);
    extern void gui_put_a_char(int i);
    extern int gui_get_a_char(int *);
    extern void gui_msg(char *buf);
    extern void gui_clear_msg(void);
    extern void gui_pop_up_reg(void);
    extern void gui_pop_up_help(void);
    extern void gui_raw_mode(int);
    extern void gui_init(void);
    extern void gui_term(void);
    extern int gui_dialog(char *buf);
    extern int gui_places(char *buf);
    extern int gui_store(char *buf);
    extern int gui_recall(char *buf);
    extern void gui_save_settings(void);
    extern void gui_save_custom_settings(void);
    extern void gui_read_settings(void);
    extern void gui_read_custom_settings(void);
    extern int gui_get_eval_expression(char *, char *);
#ifdef __cplusplus
}
#endif

/* exported by dcalc.c: */
#ifdef __cplusplus
extern "C" {
#endif
    extern char *dcalc_cpy(char *dest, const char *src, size_t n);
    extern void dcalc_fmt_base(char *s, long x, double xf);
    extern void dcalc_prep_for_output(char *string);
    extern void dcalc_parse_buffer(char *buf, long *longResult, double *doubleResult);
    extern void dcalc_initialise(void);
    extern int dcalc_process(COMMAND c);
    extern void dcalc_terminate(void);
    extern void dcalc_display(void);
    extern long dcalc_asc2int(char *s);
    extern long dcalc_bin2int(char *s);
    extern long dcalc_ip2int(char *s);
    extern void dcalc_push(long inx);
    extern void dcalc_pushf(double inx);
    extern long dcalc_pop(void);
    extern double dcalc_popf(void);
    extern int dcalc_is_inverted(void);
    extern int dcalc_start_entering(void);
    extern int dcalc_stop_entering(void);
    extern FILE *dcalc_open_config_file(char *name, char *dcalc_mode);
    extern void dcalc_perform_eval(char *expr);

    extern int dcalc_base, dcalc_decplaces, dcalc_entering, dcalc_invert, dcalc_degree, dcalc_mode, dcalc_intmode, dcalc_floatmode, dcalc_finPayAt0, dcalc_algebraic_mode, dcalc_last_was_fin_key;
    extern long dcalc_xiReg, dcalc_yiReg, dcalc_ziReg, dcalc_tiReg, dcalc_liReg, dcalc_reg[NUMREGS];
    extern double dcalc_xfReg, dcalc_yfReg, dcalc_zfReg, dcalc_tfReg, dcalc_lfReg, dcalc_regf[NUMREGS];
    extern char dcalc_inbuf[MAX_ENTRY];

#ifdef HAS_ALGEBRAIC_MODE
/* for the algebraic parser: */
    extern double  dcalc_xfSave,dcalc_yfSave,dcalc_zfSave,dcalc_tfSave,dcalc_ufSave,dcalc_lfSave;
    extern long dcalc_xSave, dcalc_ySave, dcalc_zSave, dcalc_tSave, dcalc_uSave, dcalc_lSave;
    extern char dcalc_last_eval[MAX_ENTRY];
#endif /* HAS_ALGEBRAIC_MODE */

    extern int dcalc_exec_chs(void);          
    extern int dcalc_exec_plus(void);         
    extern int dcalc_exec_minus(void);                
    extern int dcalc_exec_divide(void);               
    extern int dcalc_exec_mult(void);         
    extern int dcalc_exec_and(void);          
    extern int dcalc_exec_or(void);           
    extern int dcalc_exec_xor(void);          
    extern int dcalc_exec_not(void);          
    extern int dcalc_exec_modulus(void);              
    extern int dcalc_exec_percent(void);              
    extern int dcalc_exec_percentch(void);    
    extern int dcalc_exec_ytox(void);         
    extern int dcalc_exec_shiftl(void);               
    extern int dcalc_exec_shiftr(void);               
    extern int dcalc_exec_shiftyl(void);              
    extern int dcalc_exec_shiftyr(void);              
    extern int dcalc_exec_primef(void);               
    extern int dcalc_exec_reci(void);         
    extern int dcalc_exec_sqr(void);          
    extern int dcalc_exec_root(void);         
    extern int dcalc_exec_cube(void);         
    extern int dcalc_exec_croot(void);                
    extern int dcalc_exec_pval(void);         
    extern int dcalc_exec_fval(void);         
    extern int dcalc_exec_pmt(void);          
    extern int dcalc_exec_intst(void);                
    extern int dcalc_exec_fclr(void);         
    extern int dcalc_exec_nfin(void);         
    extern int dcalc_exec_times12(void);              
    extern int dcalc_exec_divide12(void);     
    extern int dcalc_exec_begin(void);                
    extern int dcalc_exec_dys(void);          
    extern int dcalc_exec_tdy(void);                  
    extern int dcalc_exec_asciim(void);                       
    extern int dcalc_exec_bin(void);          
    extern int dcalc_exec_ip(void);           
    extern int dcalc_exec_oct(void);          
    extern int dcalc_exec_dec(void);          
    extern int dcalc_exec_hex(void);          
    extern int dcalc_exec_fix(void);          
    extern int dcalc_exec_eng(void);          
    extern int dcalc_exec_sciformat(void);    
    extern int dcalc_exec_places(void);                       
    extern int dcalc_exec_fin(void);          
    extern int dcalc_exec_stat(void);         
    extern int dcalc_exec_sci(void);          
    extern int dcalc_exec_prog(void);         
    extern int dcalc_exec_rolldown(void);     
    extern int dcalc_exec_clx(void);          
    extern int dcalc_exec_clr(void);          
    extern int dcalc_exec_degree(void);               
    extern int dcalc_exec_radian(void);               
    extern int dcalc_exec_e(void);            
    extern int dcalc_exec_pi(void);           
    extern int dcalc_exec_sin(void);                  
    extern int dcalc_exec_asin(void);                 
    extern int dcalc_exec_cos(void);          
    extern int dcalc_exec_acos(void);         
    extern int dcalc_exec_tan(void);          
    extern int dcalc_exec_atan(void);         
    extern int dcalc_exec_sinh(void);         
    extern int dcalc_exec_asinh(void);                
    extern int dcalc_exec_cosh(void);         
    extern int dcalc_exec_acosh(void);                
    extern int dcalc_exec_tanh(void);         
    extern int dcalc_exec_atanh(void);                
    extern int dcalc_exec_loge(void);         
    extern int dcalc_exec_log10(void);                
    extern int dcalc_exec_frc(void);          
    extern int dcalc_exec_int(void);          
    extern int dcalc_exec_etox(void);         
    extern int dcalc_exec_10tox(void);                
    extern int dcalc_exec_hms(void);          
    extern int dcalc_exec_rtop(void);         
    extern int dcalc_exec_ptor(void);         
    extern int dcalc_exec_rtod(void);         
    extern int dcalc_exec_dtor(void);         
    extern int dcalc_exec_sumr(void);                 
    extern int dcalc_exec_sum0(void);         
    extern int dcalc_exec_nstat(void);                
    extern int dcalc_exec_mean(void);         
    extern int dcalc_exec_means(void);                
    extern int dcalc_exec_meany(void);                
    extern int dcalc_exec_s_dev(void);                
    extern int dcalc_exec_sum(void);          
    extern int dcalc_exec_fact(void);         
    extern int dcalc_exec_comb(void);         
    extern int dcalc_exec_perm(void);         
    extern int dcalc_exec_lr(void);           
    extern int dcalc_exec_calcy(void);                
    extern int dcalc_exec_enter(void);                
    extern int dcalc_exec_lastx(void);
    extern int dcalc_exec_quit(void); 
    extern int dcalc_exec_recall(void);               
    extern int dcalc_exec_store(void);                
    extern int dcalc_exec_stoplus(void);              
    extern int dcalc_exec_stominus(void);     
    extern int dcalc_exec_stomultiply(void);  
    extern int dcalc_exec_stodivide(void);    
    extern int dcalc_exec_xny(void);          
    extern int dcalc_exec_xnl(void);          
    extern int dcalc_exec_xnt(void);                  
    extern int dcalc_exec_xnz(void);          
    extern int dcalc_exec_help(void);         
    extern int dcalc_exec_reg(void);          
    extern int dcalc_exec_equals(void);               
    extern int dcalc_exec_leftbrace(void);            
    extern int dcalc_exec_rightbrace(void);           
    extern int dcalc_exec_toggle_deg(void);           
    extern int dcalc_exec_inv(void);          
#ifdef HAS_ALGEBRAIC_MODE
    extern int dcalc_exec_rpn(void);
    extern int dcalc_exec_algebraic(void);
    extern int dcalc_exec_eval(void);
#endif /* HAS_ALGEBRAIC_MODE */

    extern double dcalc_sin(double x);
    extern double dcalc_asin(double x);
    extern double dcalc_cos(double x);
    extern double dcalc_acos(double x);
    extern double dcalc_tan(double x);
    extern double dcalc_atan(double x);
    extern double dcalc_sinh(double x);
    extern double dcalc_asinh(double x);
    extern double dcalc_cosh(double x);
    extern double dcalc_acosh(double x);
    extern double dcalc_tanh(double x);
    extern double dcalc_atanh(double x);
    extern double dcalc_factorial(double x);
    extern double dcalc_int(double x);
    extern double dcalc_frc(double x);
    extern double dcalc_datedelta(double x, double y);
    extern double dcalc_dateplus(double x, double y);
    extern int dcalc_perm_error;
    extern double dcalc_perm(double tempxf, double tempyf);
    extern int dcalc_comb_error;
    extern double dcalc_comb(double tempxf, double tempyf);
    extern int dcalc_percentch_error;
    extern double dcalc_percentch(double tempxf, double tempyf);
    extern int dcalc_pluspercent_error;
    extern double dcalc_pluspercent(double tempxf, double tempyf);
    extern long dcalc_and(long x, long y);
    extern long dcalc_or(long x, long y);
    extern long dcalc_xor(long x, long y);
    extern long dcalc_mod(long x, long y);
    extern long dcalc_shiftl(long x, long y);
    extern long dcalc_shiftr(long x, long y);
    extern long dcalc_power(long x, long n);
    extern long dcalc_factors(long orig);
    extern long dcalc_ircl(long i);
    extern double dcalc_rcl(double i);

#ifdef __cplusplus
}
#endif
/* For emacs: */

/* Local Variables: */
/* eval:(setq tab-width 8) */
/* End: */
