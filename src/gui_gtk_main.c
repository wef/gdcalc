/*
  gtk_dcalc_main_window.c: gtk driver for dcalc

    Contact info:
    bob.hepple@gmail.com
    https://gitlab.com/wef/gdcalc

    Copyright (C) 1999-2018 Bob Hepple

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING. If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth
    Floor, Boston, MA 02110-1301 USA

 */

/* $Id: gtk_dcalc_main_window.c,v 1.15 2006/12/31 09:06:53 bhepple Exp $ */

/* To build:
   ./autogen.sh
   ./configure
   (the above may happen automagically by typing 'make')
   make
   make install
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>
#include <gtk/gtk.h>
#include <sys/param.h>

#include "dcalc.h"
#include "gui_gtk_callbacks.h"
#include "gui_gtk_main.h"

GtkWidget *main_window;
/* naughty global exported to callbacks.c: */
GtkBuilder *builder = NULL;
GtkWidget *gtk_dcalc_registers_popup = NULL;
GtkEntry  *gtk_dcalc_entry_x = NULL;
GtkWidget *gtk_dcalc_mem_dialog = NULL;
typedef char Filename[MAXPATHLEN];
char *css_filename = NULL;

int gtk_dcalc_mem_value;
char *gtk_dcalc_registers_label = "gdcalc registers";

gtk_dcalc_button *gtk_dcalc_l_buttons, *gtk_dcalc_r_buttons;

gtk_dcalc_button gtk_dcalc_sci_buttons[] = {
  { 0, 0, SIN, "SIN", "Sine of X (or INV)", "sin(", "asin(" },
  { 0, 1, COS, "COS", "Cosine of X (or INV)", "cos(", "acos(" },
  { 0, 2, TAN, "TAN", "Tangent of X (or INV)", "tan(", "atan(" },
  { 0, 3, HMS, "h.ms", "Convert X to Hours.MinsSecs (or INV)", "", "" },
  { 0, 4, RTOP, "r->p", "Convert (X,Y) to polar coordinates (or INV)", "", "" },
  { 0, 5, DTOR, "d->r", "Convert degrees to radians (or INV)", "", "" },
  { 0, 6, LOG10, "LOG10", "Logarithm (base 10) (or INV)", "log10(", "10^" },
  { 0, 7, LOGE, "LOGe", "Natural logarithm (base e) (or INV)", "loge(", "e^" },
  { 1, 0, SINH, "SINh", "Hyperbolic sine (or INV)", "sinh(", "asinh(" },
  { 1, 1, COSH, "COSh", "Hyperbolic cosine (or INV)", "cosh(", "acosh(" },
  { 1, 2, TANH, "TANh", "Hyperbolic tangent (or INV)", "tanh(", "atanh(" },
  { 1, 3, CUSTOM, "", "", "", "" },
  { 1, 4, CUSTOM, "", "", "", "" },
  { 1, 5, PI, "PI", "PI", "pi ", "" },
  { 1, 6, INT, "INT", "Integer part of X", "int(", "" },
  { 1, 7, FRC, "FRC", "Fractional part of X", "frc(", "" },
  { 2, 0, INV, "INV", "Invert functions", "", "" },
  { 2, 1, CUSTOM, "", "", "", "" },
  { 2, 2, CUSTOM, "", "", "", "" },
  { 2, 3, CUSTOM, "", "", "", "" },
  { 2, 4, CUSTOM, "", "", "", "" },
  { 2, 5, E, "e", "Base of natural logs", " e ", "" },
  { 2, 6, CUSTOM, "", "", "", "" },
  { 2, 7, CUSTOM, "", "", "", "" },
  { -1, -1, 0, "", "", "", "" }
};

gtk_dcalc_button gtk_dcalc_fin_buttons[] = {
  { 0, 0, NFIN, "n", "Number of periods (or INV)", "", "" },
  { 0, 1, INTST, "i", "Interest rate in % per period (or INV)", "", "" },
  { 0, 2, PVAL, "PV", "Present value (or INV)", "", "" },
  { 0, 3, PMT, "PMT", "Payment (or INV)", "", "" },
  { 0, 4, FVAL, "FV", "Future value (or INV)", "", "" },
  { 0, 5, FCLR, "CLf", "Clear compound interest registers (5-9)", "", "" },
  { 0, 6, LOG10, "LOG10", "Logarithm (base 10) (or INV)", "log10(", "10^" },
  { 0, 7, LOGE, "LOGe", "Natural logarithm (base e)", "loge(", "e^" },
  { 1, 0, TIMES12, "x12", "X times 12", "*12 ", "/12 " },
  { 1, 1, DIVIDE12, "/12", "X divided by 12", "/12 ", "*12 " },
  { 1, 2, BEGIN, "B/E", "Toggle Annuity at beginning of period", "", "" },
  { 1, 3, DYS, "DYS", "Days between dates x & y as YYYY.MMDD (or INV: date(y) + days(x))", "datedelta ", "dateplus " },
  { 1, 4, TDY, "TDY", "Today's date as YYYY.MMDD", "", "" },
  { 1, 5, PI, "PI", "PI", "pi ", "" },
  { 1, 6, INT, "INT", "Integer part of X", "int(", "" },
  { 1, 7, FRC, "FRC", "Fractional part of X", "frc(", "" },
  { 2, 0, INV, "INV", "Invert functions", "", "" },
  { 2, 1, CUSTOM, "", "", "", "" },
  { 2, 2, CUSTOM, "", "", "", "" },
  { 2, 3, CUSTOM, "", "", "", "" },
  { 2, 4, CUSTOM, "", "", "", "" },
  { 2, 5, E, "e", "Base of natural logs", " e ", "" },
  { 2, 6, CUSTOM, "", "", "", "" },
  { 2, 7, CUSTOM, "", "", "", "" },
  { -1, -1, 0, "", "", "", "" }
};
  
gtk_dcalc_button gtk_dcalc_stat_buttons[] = {
  { 0, 0, SUM, "SUM+", "Add X and Y to the accumulators (register 5 & 6)", "", "" },
  { 0, 1, MEAN, "m(X)", "Calculate the mean of X", "", "" },
  { 0, 2, MEANY, "m(Y)", "Calculate the mean of Y", "", "" },
  { 0, 3, S_DEV, "StdD", "Calculate the standard deviations", "", "" },
  { 0, 4, NSTAT, "N", "Number of samples", "", "" },
  { 0, 5, FACT, "x!", "X factorial", "!", "" },
  { 0, 6, LOG10, "LOG10", "Logarithm (base 10)", "log10(", " 10^" },
  { 0, 7, LOGE, "LOGe", "Natural logarithm (base e)", "loge(", " e^" },
  { 1, 0, SUMR, "SUM-", "subtract X and Y from the accumulators (register 5 & 6)", "", "" },
  { 1, 1, PERM, "yPx", "x permutations of y: y!/(y-x)!", " perm ", "" },
  { 1, 2, COMB, "yCx", "x combinations of y: y!/( (y-x)!x! )", " comb ", "" },
  { 1, 3, LR, "LR", "Linear Regression", "", "" },
  { 1, 4, CALCY, "f(x)", "Calculate Y from X using linear regression results (Inv: calculate X from Y)", "", "" },
  { 1, 5, PI, "PI", "PI", "pi ", "" },
  { 1, 6, INT, "INT", "Integer part of X", "int(", "" },
  { 1, 7, FRC, "FRC", "Fractional part of X", "frc(", "" },
  { 2, 0, INV, "INV", "Invert functions", "", "" },
  { 2, 1, CUSTOM, "", "", "", "" },
  { 2, 2, CUSTOM, "", "", "", "" },
  { 2, 3, CUSTOM, "", "", "", "" },
  { 2, 4, CUSTOM, "", "", "", "" },
  { 2, 5, E, "e", "e", " e ", "" },
  { 2, 6, CUSTOM, "", "", "", "" },
  { 2, 7, CUSTOM, "", "", "", "" },
  { -1, -1, 0, "", "", "", "" }
};

gtk_dcalc_button gtk_dcalc_prog_buttons[] = {
  { 0, 0, BIN, "BIN", "Binary mode", "", "" },
  { 0, 1, OCT, "OCT", "Octal mode", "", "" },
  { 0, 2, DEC, "DEC", "Decimal mode", "", "" },
  { 0, 3, HEX, "HEX", "Hexadecimal mode", "", "" },
  { 0, 4, ASCIIM, "ASC", "Ascii mode", "", "" },
  { 0, 5, IP, "IP", "IP address mode", "", "" },
  { 0, 6, CUSTOM, "", "", "", "" },
  { 0, 7, CUSTOM, "", "", "", "" },
  { 1, 0, dAND, "AND", "x and y", "AND ", "" },
  { 1, 1, dOR, "OR", "y or x", "OR ", "" },
  { 1, 2, dXOR, "XOR", "y xor x", "XOR ", "" },
  { 1, 3, MODULUS, "MOD", "y mod x", "MOD ", "" },
  { 1, 4, dNOT, "~X", "not X (complement)", "~", "~" },
  { 1, 5, SHIFTYL, "y<<x", "Shift Y, X bits left", "lshift ", "rshift " },
  { 1, 6, SHIFTYR, "y>>x", "Shift Y, X bits right", "rshift ", "lshift " },
  { 1, 7, CUSTOM, "", "", "", "" },
  { 2, 0, INV, "INV", "Invert functions", "", "" },
  { 2, 1, PRIMEF, "PF", "Prime factors of X", "PF(", "" },
  { 2, 2, CUSTOM, "", "", "", "" },
  { 2, 3, CUSTOM, "", "", "", "" },
  { 2, 4, CUSTOM, "", "", "", "" },
  { 2, 5, SHIFTL, "<<", "Shift X left 1 bit", "lshift 1 ", "rshift 1 " },
  { 2, 6, SHIFTR, ">>", "Shift X right 1 bit", "rshift 1 ", "lshift 1 " },
  { 2, 7, CUSTOM, "", "", "", "" },
  { -1, -1, 0, "", "", "", "" }
};

gtk_dcalc_button gtk_dcalc_float_buttons[] = {
  { 0, 0, FIN, "FIN", "Finance mode", "", "" },
  { 0, 1, STAT, "STA", "Statistics mode", "", "" },
  { 0, 2, PROG, "PRO", "Programmers mode", "", "" },
  { 0, 3, CUSTOM, "", "", "", "" },
  { 0, 4, 'E', "E", "", "E", "" },
  { 0, 5, PERCENT, "%", "X percent of Y (or INV)", " % ", "" },
  { 1, 0, YTOX, "y^X", "Y to the power of X", "^", "" },
  { 1, 1, SQR, "x2", "X squared", "^2 ", "" },
  { 1, 2, ROOT, "SQRT", "Square root of X", "sqrt(", "" },
  { 1, 3, CUSTOM, "", "", "", "" },
  { 1, 4, RECI, "1/X", "Reciprocal of X", " 1/", "" },
  { 1, 5, PERCENTCH, "%CH", "Percentage change (or INV)", " percentch ", " pluspercent " },
  { 2, 0, FIXFORMAT, "FIX", "Floating point notation", "", "" },
  { 2, 1, SCIFORMAT, "SCI", "Scientific notation", "", "" },
  { 2, 2, ENGFORMAT, "ENG", "Engineering notation", "", "" },
  { 2, 3, '7', "7", "", "7", "" },
  { 2, 4, '8', "8", "", "8", "" },
  { 2, 5, '9', "9", "", "9", "" },
  { 3, 0, STORE, "STO", "Store X to a register", "", "" },
  { 3, 1, STOPLUS, "ST+", "Add X to a register", "", "" },
  { 3, 2, STOMINUS, "ST-", "Subtract X from a register", "", "" },
  { 3, 3, '4', "4", "", "4", "" },
  { 3, 4, '5', "5", "", "5", "" },
  { 3, 5, '6', "6", "", "6", "" },
  { 4, 0, RECALL, "RCL", "Recall from a register", "", "" },
  { 4, 1, CLR, "CLs", "Clear stack (INV= clear registers)", "", "" },
  { 4, 2, EVAL, "Eval", "Last algebraic expression", "", "" },
  { 4, 3, '1', "1", "", "1", "" },
  { 4, 4, '2', "2", "", "2", "" },
  { 4, 5, '3', "3", "", "3", "" },
  { 5, 0, ROLLDOWN, "ROLL", "Roll stack down", "", "" },
  { 5, 1, CLX, "CLx", "Clear X", "", "" },
  { 5, 2, BACKSPACE, "BSP", "Backspace", "", "" },
  { 5, 3, '0', "0", "", "0", "" },
  { 5, 4, '.', ".", "", ".", "" },
  { 5, 5, CHS, "CHS", "Change sign of X", "-", "" },
  { -1, -1, 0, "", "", "", "" }
};

gtk_dcalc_button gtk_dcalc_int_buttons[] = {
  { 0, 0, SCI, "SCI", "Scientific mode", "", "" },
  { 0, 1, FIN, "FIN", "Finance mode", "", "" },
  { 0, 2, STAT, "STA", "Statistics mode", "", "" },
  { 0, 3, 'D', "D", "", "D", "" },
  { 0, 4, 'E', "E", "", "E", "" },
  { 0, 5, 'F', "F", "", "F", "" },
  { 1, 0, YTOX, "y^X", "Y to the power of X", "^", "" },
  { 1, 1, SQR, "x2", "X squared", "^2 ", "" },
  { 1, 2, CUSTOM, "", "", "", "" },
  { 1, 3, 'A', "A", "", "A", "" },
  { 1, 4, 'B', "B", "", "B", "" },
  { 1, 5, 'C', "C", "", "C", "" },
  { 2, 0, CUSTOM, "", "", "", "" },
  { 2, 1, CUSTOM, "", "", "", "" },
  { 2, 2, CUSTOM, "", "", "", "" },
  { 2, 3, '7', "7", "", "7", "" },
  { 2, 4, '8', "8", "", "8", "" },
  { 2, 5, '9', "9", "", "9", "" },
  { 3, 0, STORE, "STO", "Store X to a register", "", "" },
  { 3, 1, STOPLUS, "ST+", "Add X to a register", "", "" },
  { 3, 2, STOMINUS, "ST-", "Subtract X from a register", "", "" },
  { 3, 3, '4', "4", "", "4", "" },
  { 3, 4, '5', "5", "", "5", "" },
  { 3, 5, '6', "6", "", "6", "" },
  { 4, 0, RECALL, "RCL", "Recall from a register", "", "" },
  { 4, 1, CLR, "CLs", "Clear stack (INV= clear registers)", "", "" },
  { 4, 2, EVAL, "Eval", "Last algebraic expression", "", "" },
  { 4, 3, '1', "1", "", "1", "" },
  { 4, 4, '2', "2", "", "2", "" },
  { 4, 5, '3', "3", "", "3", "" },
  { 5, 0, ROLLDOWN, "ROLL", "Roll stack down", "", "" },
  { 5, 1, CLX, "CLx", "Clear X", "", "" },
  { 5, 2, BACKSPACE, "BSP", "Backspace", "", "" },
  { 5, 3, '0', "0", "", "0", "" },
  { 5, 4, '.', ".", "", ".", "" },
  { 5, 5, CHS, "CHS", "Change sign of X", "", "" },
  { -1, -1, 0, "", "", "", "" }
};

void
gtk_dcalc_set_x(char *dcalc_inbuf) {
	on_entry_X_insert_text_enabled = 0;
	gtk_entry_set_text(gtk_dcalc_entry_x, dcalc_inbuf);
    gtk_editable_set_position(GTK_EDITABLE(gtk_dcalc_entry_x), -1);
	on_entry_X_insert_text_enabled = 1;
}

const char *gtk_dcalc_get_x() {
	return(gtk_entry_get_text(gtk_dcalc_entry_x));
}

void
gtk_dcalc_append_x(char *dcalc_inbuf) {
    const char *oldbuf;
    char newbuf[MAX_ENTRY];
    
	on_entry_X_insert_text_enabled = 0;
    oldbuf = gtk_dcalc_get_x();
    dcalc_cpy(newbuf, oldbuf, MAX_ENTRY);
    strncat(newbuf, dcalc_inbuf, MAX_ENTRY - 1 - strlen(oldbuf));
	gtk_entry_set_text(gtk_dcalc_entry_x, newbuf);
    gtk_editable_set_position(GTK_EDITABLE(gtk_dcalc_entry_x), -1);
	on_entry_X_insert_text_enabled = 1;
}

void
gtk_dcalc_customise_button(gtk_dcalc_button *b) {
    GtkWidget *customise_popup = GTK_WIDGET(gtk_dcalc_get_widget("gtk_dcalc_customise_button", "custom_dialog"));
    GtkWidget *title_w = GTK_WIDGET(gtk_dcalc_get_widget("gtk_dcalc_customise_button", "custom_title"));
    GtkWidget *label_w = GTK_WIDGET(gtk_dcalc_get_widget("gtk_dcalc_customise_button", "custom_label_entry"));
    GtkWidget *expr_w = GTK_WIDGET(gtk_dcalc_get_widget("gtk_dcalc_customise_button", "custom_expr_entry"));
    char title[MAXPATHLEN];
    int reply;
    
    sprintf(title, "Customise button at (%d %d)", b->i, b->j);
	gtk_label_set_text(GTK_LABEL(title_w), title);
	gtk_entry_set_text(GTK_ENTRY(label_w), b->label);
	gtk_entry_set_text(GTK_ENTRY(expr_w), b->tooltip);
	reply = gtk_dialog_run(GTK_DIALOG(customise_popup));
	gtk_widget_hide(customise_popup);
    if (reply == 0) {
        if (*(gtk_entry_get_text(GTK_ENTRY(label_w))) &&
            *(gtk_entry_get_text(GTK_ENTRY(label_w)))) {
            dcalc_cpy(b->label, gtk_entry_get_text(GTK_ENTRY(label_w)), BUTTON_LABEL_LEN);
            dcalc_cpy(b->tooltip, gtk_entry_get_text(GTK_ENTRY(expr_w)), BUTTON_TOOLTIP_LEN);
            gtk_dcalc_display_buttons(dcalc_mode);
        }
    } else if (reply == 1) {
        // delete it
        b->label[0] = 0;
        b->tooltip[0] = 0;
        gtk_dcalc_display_buttons(dcalc_mode);
    }
	return;
}

/* Process a command at the GUI level if possible. Return NOP if
 * there's nothing else to dcalc_process. This is mostly for algebraic
 * dcalc_mode. */
static int
process_command_for_button(gtk_dcalc_button *b) {
    if (b->command == CUSTOM) {
        if (dcalc_is_inverted() || (b->tooltip[0] == 0)) {
            gtk_dcalc_customise_button(b);
        } else {
            if (dcalc_algebraic_mode && dcalc_entering) {
                dcalc_cpy(dcalc_last_eval, gtk_dcalc_get_x(), MAX_ENTRY);
                dcalc_exec_equals();
            }
            dcalc_stop_entering();
            dcalc_perform_eval(b->tooltip);
        }
        return(NOP);
    }
    
	if (!dcalc_algebraic_mode) {
		return(b->command); /* RPN is too easy! */
    }

    /* ALGEBRAIC MODE */
    
	switch (b->command) {
		/* These needY: */
	case COMB:
	case DIVIDE12:
	case DIVIDE:
	case SHIFTL:
	case SHIFTR:
	case SHIFTYL:
	case SHIFTYR:
	case MINUS:
	case MULT:
	case PERM:
	case PLUS:
	case SQR:
	case TIMES12:
	case dAND:
	case dOR:
	case dXOR:
    case DYS:
    case PERCENT:
    case PERCENTCH:
		dcalc_last_was_fin_key = 0;
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("ans ");
		}
		if (dcalc_is_inverted())
			gtk_dcalc_append_x(b->invtext);
		else
			gtk_dcalc_append_x(b->text);
		return(NOP);

	case BACKSPACE:
	{
		char buf[MAX_ENTRY];
		int len;
		dcalc_is_inverted();
		dcalc_last_was_fin_key = 0;
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("");
			return(BACKSPACE);
		}
		dcalc_cpy(buf, gtk_dcalc_get_x(), MAX_ENTRY);
		len = strlen(buf);
		if (len) {
			buf[len - 1] = 0;
			gtk_dcalc_set_x(buf);
		}
	}
	return(NOP);

	case CLR:
		dcalc_process(b->command);
		gtk_dcalc_set_x("");
		return(NOP);

	case CLX:
        dcalc_start_entering();
		gtk_dcalc_set_x("");
		dcalc_last_was_fin_key = 0;
		return(NOP);
        
	case 'F':
	case 'f':
	case 'E':
	case 'e':
	case 'D':
	case 'd':
	case 'C':
	case 'c':
	case 'B':
	case 'b':
	case 'A':
	case 'a':
		if ((dcalc_mode == PROG && dcalc_intmode == HEX) ||
			(dcalc_mode != PROG && toupper(b->command) == 'E'))
			/* is OK */;
		else
			return(NOP);
	case '9':
	case '8':
		if (dcalc_mode == PROG && ((dcalc_intmode == BIN) || (dcalc_intmode == OCT)))
			return(NOP);
	case '7':
	case '6':
	case '5':
	case '4':
	case '3':
	case '2':
		if (dcalc_mode == PROG && dcalc_intmode == BIN)
			return(NOP);
	case '1':
	case '0':
	case '.':
		if ((b->command == '.') && (dcalc_mode == PROG) && (dcalc_intmode != IP))
			return(NOP);
		dcalc_last_was_fin_key = 0;
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("");
		}
		if (dcalc_is_inverted() && strlen(b->invtext))
			gtk_dcalc_append_x(b->invtext);
		else
			gtk_dcalc_append_x(b->text);
		return(NOP);

    case STOPLUS:
    case STOMINUS:
    case ROLLDOWN:
    case EVAL:
	case INV:
		return(b->command);

	case RECALL:
		if (dcalc_start_entering()) {
			gtk_dcalc_set_x("");
		}
		dcalc_exec_recall();
		return NOP;

    case YTOX:
        if (!dcalc_entering)
            return YTOX;
        /* else FALLTHROUGH */
	default:
		if (strlen(b->text)) {
            if (dcalc_start_entering()) {
                gtk_dcalc_set_x("");
            }

			dcalc_last_was_fin_key = 0;
			if (dcalc_is_inverted() && strlen(b->invtext))
				gtk_dcalc_append_x(b->invtext);
			else
				gtk_dcalc_append_x(b->text);
		} else { /* just do the command or key: */
/*
  WTF is this for?
            
            int saveInv = dcalc_invert;
			strcpy(dcalc_last_eval, gtk_dcalc_get_x());
			if (dcalc_mode == PROG)
				dcalc_pop();
			else
				dcalc_popf();
			dcalc_exec_equals();
			dcalc_invert = saveInv;
*/
			return(b->command);
		}
		return(NOP);
	}
	return(NOP);
}

int 
gtk_dcalc_lookup_and_process_button_at(gtk_dcalc_button *b, int i, int j) {
	while (b != NULL && b->i >= 0)
		if (b->i == i && b->j == j)
			return(process_command_for_button(b));
		else
			b++;
	return(NOP);
}

GtkWidget *gtk_dcalc_get_widget(char *location, char *widget_name) {
    GtkWidget *retval = (GtkWidget *) gtk_builder_get_object(builder, widget_name);
    if (!retval) {
        fprintf(stderr, "gdcalc: %s(): Can't find widget %s\n", location, widget_name);
        exit(1);
    }
    return retval;
}

static void display_button(char which_panel, gtk_dcalc_button *b) {
	GtkWidget *g;
	char widget_name[MAX_ENTRY];

    snprintf(widget_name, 20, "%c%d_%d", which_panel, b->i, b->j);
    g = gtk_dcalc_get_widget("display_button", widget_name);
    if (g) {
        if (GTK_IS_LABEL (g)) {
            gtk_label_set_text (GTK_LABEL (g), b->label);
        } else { // a button
            gtk_button_set_label(GTK_BUTTON(g), b->label);
        }
        if (b->tooltip[0] == 0) {
            gtk_widget_set_has_tooltip(g, FALSE);
        } else {
            char custom_label[] = "CUSTOM: ";
            char *tooltip = calloc(strlen(custom_label) + strlen(b->tooltip) + 1, 1);
            if (b->command == CUSTOM) {
                strcat(tooltip, custom_label);
                gtk_style_context_add_class(gtk_widget_get_style_context(g), "custom_button_class");
            } else {
                gtk_style_context_remove_class(gtk_widget_get_style_context(g), "custom_button_class");
            }

            strcat(tooltip, b->tooltip);
            gtk_widget_set_has_tooltip(g, TRUE);
            gtk_widget_set_tooltip_text(g, tooltip);
            free(tooltip);
        }
        if (b->command == CUSTOM) {
            g_signal_connect(G_OBJECT(g), "button-press-event", G_CALLBACK(on_custom_button_release_event), b);
        }
    }
}

static void display_panel(char which_panel, gtk_dcalc_button *b) {
    gtk_dcalc_button *button_iter = b;
    
	if (which_panel == 'R') {
		gtk_dcalc_r_buttons = b;
	} else {
		gtk_dcalc_l_buttons = b;
    }

    for (button_iter = b; button_iter && button_iter->i >= 0; button_iter++) {
        display_button(which_panel, button_iter);
	}
}

static void
showWidget(char *name) {
	gpointer g;

	g = gtk_dcalc_get_widget("showWidget",name);
    gtk_widget_show(g);
}

static void
hideWidget(char *name) {
	gpointer g;

	g = gtk_dcalc_get_widget("hideWidget",name);
    gtk_widget_hide(g);
}

static
void display_enter_keys() {
    if (dcalc_algebraic_mode) {
        hideWidget("Enter");
        showWidget("LeftBrace");
        showWidget("RightBrace");
        showWidget("Equals");
        showWidget("Ans");
    } else {
        showWidget("Enter");
        hideWidget("LeftBrace");
        hideWidget("RightBrace");
        hideWidget("Equals");
        hideWidget("Ans");
    }
}

void gtk_dcalc_display_buttons(int c) {
	GtkWidget *w;

    display_enter_keys();

	if (c == PROG) {
		display_panel('R', gtk_dcalc_int_buttons);
		display_panel('L', gtk_dcalc_prog_buttons);
	} else {
		switch (c) {
		case STAT:
			gtk_dcalc_float_buttons[0].command = PROG;
			strcpy(gtk_dcalc_float_buttons[0].label, "PRO");
			strcpy(gtk_dcalc_float_buttons[0].tooltip, "Programmers mode");
			gtk_dcalc_float_buttons[1].command = SCI;
			strcpy(gtk_dcalc_float_buttons[1].label, "SCI");
			strcpy(gtk_dcalc_float_buttons[1].tooltip, "Scientific mode");
			gtk_dcalc_float_buttons[2].command = FIN;
			strcpy(gtk_dcalc_float_buttons[2].label, "FIN");
			strcpy(gtk_dcalc_float_buttons[2].tooltip, "Finance mode");
			display_panel('L', gtk_dcalc_stat_buttons);	
			break;
		case FIN:
			gtk_dcalc_float_buttons[0].command = STAT;
			strcpy(gtk_dcalc_float_buttons[0].label, "STA");
			strcpy(gtk_dcalc_float_buttons[0].tooltip, "Statistics mode");
			gtk_dcalc_float_buttons[1].command = PROG;
			strcpy(gtk_dcalc_float_buttons[1].label, "PRO");
			strcpy(gtk_dcalc_float_buttons[1].tooltip, "Programmers mode");
			gtk_dcalc_float_buttons[2].command = SCI;
			strcpy(gtk_dcalc_float_buttons[2].label, "SCI");
			strcpy(gtk_dcalc_float_buttons[2].tooltip, "Scientific mode");
			display_panel('L', gtk_dcalc_fin_buttons);
			break;
		default: /* SCI */
			gtk_dcalc_float_buttons[0].command = FIN;
			strcpy(gtk_dcalc_float_buttons[0].label, "FIN");
			strcpy(gtk_dcalc_float_buttons[0].tooltip, "Finance mode");
			gtk_dcalc_float_buttons[1].command = STAT;
			strcpy(gtk_dcalc_float_buttons[1].label, "STA");
			strcpy(gtk_dcalc_float_buttons[1].tooltip, "Statistics mode");
			gtk_dcalc_float_buttons[2].command = PROG;
			strcpy(gtk_dcalc_float_buttons[2].label, "PRO");
			strcpy(gtk_dcalc_float_buttons[2].tooltip, "Programmers mode");
			display_panel('L', gtk_dcalc_sci_buttons);
			break;
		}
		display_panel('R', gtk_dcalc_float_buttons);
	}      
	w = gtk_dcalc_get_widget("gtk_dcalc_display_buttons","degree");
    gtk_widget_set_sensitive(w, (c==PROG)? 0: 1);

	w = gtk_dcalc_get_widget("gtk_dcalc_display_buttons","radians");
    gtk_widget_set_sensitive(w, (c==PROG)? 0: 1);

	w = gtk_dcalc_get_widget("gtk_dcalc_display_buttons","annuity_in_advance");
    gtk_widget_set_sensitive(w, (c==FIN)? 1: 0);

	w = gtk_dcalc_get_widget("gtk_dcalc_display_buttons","convert_dialog");
    gtk_widget_set_sensitive(w, (c==PROG)? 0: 1);

	w = gtk_dcalc_get_widget("gtk_dcalc_display_buttons","fixed_point_numbering");
    gtk_widget_set_sensitive(w, (c==PROG)? 0: 1);

	w = gtk_dcalc_get_widget("gtk_dcalc_display_buttons","engineering_numbering");
    gtk_widget_set_sensitive(w, (c==PROG)? 0: 1);

	w = gtk_dcalc_get_widget("gtk_dcalc_display_buttons","scientific_numbering");
    gtk_widget_set_sensitive(w, (c==PROG)? 0: 1);

	w = gtk_dcalc_get_widget("gtk_dcalc_display_buttons","places");
    gtk_widget_set_sensitive(w, (c==PROG)? 0: 1);
}

static void print_number(char *buf, char *name) 
{
	GtkWidget *g;
	g = gtk_dcalc_get_widget("print_number", name);

	gtk_entry_set_text (GTK_ENTRY (g), buf);
}

void gui_print_x(char *buf) {
	gtk_dcalc_set_x(buf);
}

void
gui_add_x(char *buf) {
	gtk_dcalc_append_x(buf);
}

void gui_dispnums() {
    char 	outbuf[MAX_ENTRY];

    if (dcalc_entering) {
		dcalc_cpy(outbuf, dcalc_inbuf, MAX_ENTRY);
    } else
		dcalc_fmt_base(outbuf, dcalc_xiReg, dcalc_xfReg);

    dcalc_prep_for_output(outbuf);
    gui_print_x(outbuf);
    
    dcalc_fmt_base(outbuf, dcalc_yiReg, dcalc_yfReg);
    dcalc_prep_for_output(outbuf);
    print_number(outbuf, "entry_Y");

    dcalc_fmt_base(outbuf, dcalc_ziReg, dcalc_zfReg);
    dcalc_prep_for_output(outbuf);
    print_number(outbuf, "entry_Z");

    dcalc_fmt_base(outbuf, dcalc_tiReg, dcalc_tfReg);
    dcalc_prep_for_output(outbuf);
    print_number(outbuf, "entry_T");

    dcalc_fmt_base(outbuf, dcalc_liReg, dcalc_lfReg);
    dcalc_prep_for_output(outbuf);
    print_number(outbuf, "entry_L");
}

/* only used for the bell ... */
void gui_put_a_char(int i) {
	// gdk_beep();
}

static char *printBase() {
    switch (dcalc_mode) {
    case PROG:
		switch (dcalc_intmode) {
		case ASCIIM:
			return("prog: asc: ");
		case BIN:
			return("prog: bin: ");
		case OCT:
			return("prog: oct: ");
		case DEC:
			return("prog: dec: ");
		case HEX:
			return("prog: hex: ");
        case IP:
            return("prog: ip: ");
        }
    case SCI:
			return "sci: ";
    case FIN:
			return "fin: ";
    case STAT:
			return "stats: ";
    }
	return "";
}

static char *printInv() {
    if (dcalc_invert)
		return("inv: ");
    else
		return("");
}

static char *printDeg(){
    if (dcalc_degree)
		return("deg: ");
    else
		return("rad: ");
}

void gui_msg(char *buf) {
	gint context_id;
	gpointer status_bar;
	char status[MAX_ENTRY];

	dcalc_cpy(status, printBase(), MAX_ENTRY);
	if (dcalc_mode != PROG) {
		strcat(status, printDeg());
	}
	strcat(status, printInv());
	strcat(status, " ");
	strcat(status, buf);
	status_bar = gtk_dcalc_get_widget("gui_msg", "statusbar");
	context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(status_bar), 
											  "Statusbar example");
	gtk_statusbar_push( GTK_STATUSBAR(status_bar), context_id, status);
}

void gui_clear_msg() {
	gui_msg("");
}

void gui_print_inv() {
    GtkButton *invert_button;

	invert_button = (GtkButton *) gtk_dcalc_get_widget("gui_print_inv", "L2_0");
	gtk_button_set_relief(invert_button, dcalc_invert? GTK_RELIEF_NONE: GTK_RELIEF_NORMAL);
}

/* These not needed for GTK version: */
void gui_print_base() {};
void gui_print_string(char *buf) {}
void gui_print_deg(){}
void gui_pop_up_help(void) {}
void gui_raw_mode(int i) {}
void gui_init(void) {}
void gui_term(void) {gtk_main_quit();}
int gui_get_a_char(int *c) {return 0;}

int gui_dialog(char *buf) {
	GtkWidget *dialog_popup;
	GtkWidget *g;
	int reply;

	dialog_popup = gtk_dcalc_get_widget("gui_dialog", "gen_dialog");
	g = gtk_dcalc_get_widget("gui_dialog", "msg");
	gtk_label_set_text(GTK_LABEL(g), buf);
	reply = gtk_dialog_run(GTK_DIALOG(dialog_popup));
	gtk_widget_hide(dialog_popup);
	return (reply==0)? 'Y': 'N';
}

int gui_places(char *buf) {
	GtkWidget *places_dialog;
	GtkSpinButton *spinbutton;
	int reply;
	gfloat decplacesf = dcalc_decplaces;

	places_dialog = gtk_dcalc_get_widget("places", "places_dialog");
	spinbutton = (GtkSpinButton *) gtk_dcalc_get_widget("places", "places_spinbutton");
	gtk_spin_button_set_value(spinbutton, decplacesf);

	reply = gtk_dialog_run(GTK_DIALOG(places_dialog));
	if (reply == 0) {
		reply = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinbutton));
        
        dcalc_decplaces = reply;
        gui_dispnums();
	} else
		reply = -1;

	gtk_widget_hide(places_dialog);

	return reply;
}

int gui_get_eval_expression(char *buf, char *expr) {
	GtkEntry *eval_expr_entry;
    GtkWidget *eval_dialog;
	int reply;

	eval_dialog = gtk_dcalc_get_widget("gui_get_eval_expression", "eval_dialog");
	eval_expr_entry = (GtkEntry *) gtk_dcalc_get_widget("gui_get_eval_expression", "eval_expr");
	gtk_entry_set_text( eval_expr_entry, buf);
	reply = gtk_dialog_run(GTK_DIALOG(eval_dialog));
	if (reply == 0) {
		dcalc_cpy(expr, gtk_entry_get_text(eval_expr_entry), MAX_ENTRY);
	} else
		reply = -1;

	gtk_widget_hide(eval_dialog);

	return reply;
}

static void xdispreg(GtkWidget *gui_dialog, int i) {
	char labelname[MAX_ENTRY], outbuf[MAX_ENTRY];
	GtkWidget *entry;

	if (gui_dialog) {
		sprintf(labelname, "mem%d", i);
		entry = gtk_dcalc_get_widget("xdispreg", labelname);
		dcalc_fmt_base(outbuf, dcalc_reg[i], dcalc_regf[i]);
		dcalc_prep_for_output(outbuf);
		if (gtk_dcalc_l_buttons == gtk_dcalc_fin_buttons)
			switch (i) {
			case FIN_NUM_REG: strcat(outbuf, " = periods"); break;
			case FIN_INT_REG: strcat(outbuf, " = interest (%)"); break;
			case FIN_PVAL_REG: strcat(outbuf, " = PVal"); break;
			case FIN_PMT_REG: strcat(outbuf, " = payment"); break;
			case FIN_FVAL_REG: strcat(outbuf, " = FVal"); break;
			}
		else if (gtk_dcalc_l_buttons == gtk_dcalc_stat_buttons)
			switch (i) {
			case STAT_R2: strcat(outbuf, " = correlation (1=good 0=poor)"); break;
			case STAT_Y_INT: strcat(outbuf, " = Y intersect"); break;
			case STAT_SLOPE: strcat(outbuf, " = slope"); break;
			case STAT_NUM_REG: strcat(outbuf, " = n"); break;
			case STAT_SUMX_REG: strcat(outbuf, " = sum of X's"); break;
			case STAT_SUMX2_REG: strcat(outbuf, " = sum of x2's"); break;
			case STAT_SUMY_REG: strcat(outbuf, " = sum of Y's"); break;
			case STAT_SUMY2_REG: strcat(outbuf, " = sum of y2's"); break;
			case STAT_SUMXY_REG: strcat(outbuf, " = sum of XY's"); break;
			}

		gtk_entry_set_text(GTK_ENTRY(entry), outbuf);
	}
}

void gui_dispreg(int i) {
	xdispreg(gtk_dcalc_registers_popup, i);
}

void gui_dispregs(void) {
	int i;

	if (gtk_dcalc_registers_popup) {
		for (i = 0; i < NUMREGS; i++)
			gui_dispreg(i);
    }
}

void gtk_dcalc_toggle_registers(void) {
    GtkCheckMenuItem *register_window_menu_item = (GtkCheckMenuItem *) gtk_dcalc_get_widget("gtk_dcalc_toggle_registers",
                                                                                  "register_window_menu_item");
    
    on_register_window_activate_enabled = 0;
	if (!gtk_dcalc_registers_popup) {
        GtkLabel *mem_label = GTK_LABEL(gtk_dcalc_get_widget("gtk_dcalc_toggle_registers", "mem_label"));
        
		gtk_dcalc_registers_popup = gtk_dcalc_get_widget("gtk_dcalc_toggle_registers", "gtk_dcalc_mem_dialog");
		gui_dispregs();
        gtk_label_set_text(mem_label, gtk_dcalc_registers_label);
		gtk_widget_show(gtk_dcalc_registers_popup);
        gtk_window_set_title(GTK_WINDOW(gtk_dcalc_registers_popup), gtk_dcalc_registers_label);
        gtk_check_menu_item_set_active(register_window_menu_item, TRUE);
        g_signal_connect(G_OBJECT(gtk_dcalc_registers_popup), "key-press", G_CALLBACK(on_mem_dialog_key_press_event), NULL);
	} else {
		gtk_widget_hide(gtk_dcalc_registers_popup);
		gtk_dcalc_registers_popup = NULL;
        gtk_check_menu_item_set_active(register_window_menu_item, FALSE);
	}
    on_register_window_activate_enabled = 1;
}

void gui_pop_up_reg(void) {
	gtk_dcalc_toggle_registers();
}

static int do_mem_dialog(char *buf) {
	int reply, i;
    GtkLabel *mem_label = GTK_LABEL(gtk_dcalc_get_widget("do_mem_dialog", "mem_label"));
    char tbuf[MAX_ENTRY];

    dcalc_cpy(tbuf, buf, MAX_ENTRY);
    strncat(tbuf, gtk_dcalc_registers_label, MAX_ENTRY - 1 - strlen(buf));
	gtk_dcalc_mem_dialog = gtk_dcalc_get_widget("do_mem_dialog", "gtk_dcalc_mem_dialog");
	gtk_window_set_title(GTK_WINDOW (gtk_dcalc_mem_dialog), buf);
    gtk_label_set_text(mem_label, tbuf);
	for (i=0; i < NUMREGS; i++)
		xdispreg(gtk_dcalc_mem_dialog, i);
	reply = gtk_dialog_run(GTK_DIALOG(gtk_dcalc_mem_dialog));
    gui_msg("");
	return (reply==0)? -1: gtk_dcalc_mem_value;
}

int gui_store(char *buf) {
    gui_msg(buf);
	return(do_mem_dialog("Store..."));
}

int gui_recall(char *buf) {
    gui_msg(buf);
	return(do_mem_dialog("Recall..."));
}

void on_algebraic_activate();

static void pull_in_css_style() {
    Filename *filename;
    
    /* Pull in CSS style info */
    Filename css_candidates[] = {
        "",
        "/etc/gdcalc/gdcalc.css",
        ""
    };
    GError *err = NULL;
        
    GtkCssProvider *css_provider = gtk_css_provider_new();
    GdkDisplay* Display = gdk_display_get_default();
    GdkScreen* Screen = gdk_display_get_default_screen(Display);
    gtk_style_context_add_provider_for_screen(Screen,
                                              GTK_STYLE_PROVIDER(css_provider),
                                              GTK_STYLE_PROVIDER_PRIORITY_USER);

        
    if (css_filename && strlen(css_filename)) {
        dcalc_cpy(css_candidates[0], css_filename, MAXPATHLEN);
        g_free(css_filename);
    } else {
        dcalc_cpy(css_candidates[0], getenv("HOME"), MAXPATHLEN);
        strcat(css_candidates[0], "/.config/gdcalc/gdcalc.css");
    }            
    for (filename = css_candidates; strlen(*filename); filename++) {
        err = NULL;
        gtk_css_provider_load_from_path(css_provider, *filename, &err);
        if (err == NULL) {
            printf("gdcalc: using CSS style from '%s'\n", *filename);
            break;
        } else {
            fprintf(stderr, "gdcalc: tried to read '%s' and failed:\n%s\n", *filename, err->message);
            g_error_free(err);
        }
    }
}

static void setup_about_dialog() {
    GError *err = NULL;
    GtkWidget *w;
    GdkPixbuf *icon = NULL;
    Filename icon_candidates[] = {
        PACKAGE_DATA_DIR "/pixmaps/HP-16C-48.png",
        ""
    };
    Filename *filename;

    w = gtk_dcalc_get_widget("main", "about_dialog");
    for (filename = icon_candidates; strlen(*filename); filename++) {
        err = NULL;
        icon = gdk_pixbuf_new_from_file(*filename, &err);
        if (err == NULL && icon) {
            printf("gdcalc: using icon file '%s'\n", *filename);
            break;
        }
        if (err) g_error_free(err);
        printf("gdcalc: tried to read icon from '%s' and failed\n", *filename);
        /* but we can continue without an icon ... */
    }
    gtk_about_dialog_set_logo((GtkAboutDialog *)w, icon);
        
#define V(x) "Version " x

    gtk_about_dialog_set_version((GtkAboutDialog *)w, VERSION);
}

static void pull_in_glade_file() {
    int retval = 1;
    GError *err = NULL;
    Filename glade_candidates[] = {
        "./gdcalc.glade", /* pick up dev versions */
        "../gdcalc.glade",
        PACKAGE_DATA_DIR "/gdcalc/gdcalc.glade",
        ""
    };
    Filename *filename;
    
    builder = gtk_builder_new();
    for (filename = glade_candidates; strlen(*filename); filename++) {
        gtk_builder_add_from_file(builder, *filename, &err);
        if (err == NULL) {
            printf("gdcalc: using glade file '%s'\n", *filename);
            retval = 0;
            break;
        } else {
            fprintf(stderr, "gdcalc: tried to read '%s' and failed:\n%s\n", *filename, err->message);
            g_error_free(err);
            err = NULL;
        }
    }
    if (retval != 0) {
        fprintf(stderr, "gdcalc: no glade file\n");
        exit(1);
    }
}

int
main(int argc, char *argv[])
{
    GtkWidget *w = NULL;
    int version = 0;
    GOptionEntry opts[] = {
        {
        "config",
        'c',
        G_OPTION_FLAG_NONE,
        G_OPTION_ARG_FILENAME,
        &css_filename,
        "configuration (css) file",
        "filename",
        },
        {
        "version",
        'V',
        G_OPTION_FLAG_NONE,
        G_OPTION_ARG_NONE,
        &version,
        "version",
        NULL,
        },
        { NULL }
    };
    
    gtk_init_with_args(&argc, &argv,
                       NULL,
                       opts,
                       NULL, NULL);

    if (version) {
        printf("gdcalc: version %s\n", VERSION);
        exit(0);
    }

    pull_in_glade_file();
    
    /* get a handle for the main_window */
    main_window = gtk_dcalc_get_widget("main", "main_window");

    setup_about_dialog();
    
	gtk_widget_show(main_window);

	gtk_dcalc_entry_x = (GtkEntry *) gtk_dcalc_get_widget("main", "entry_X");

    /* initialise dcalc engine */
	dcalc_initialise();
	gtk_dcalc_display_buttons(dcalc_mode);

	/* gtk_check_menu_item_set_active actually calls the widgets
	   activate method - ie simulating a button release!! This can
	   lead to an infinite loop of invocations - so we call
	   gtk_builder_connect_signals() _after_ this code. */
	w = gtk_dcalc_get_widget("main",
                   (dcalc_mode==SCI)? "scientific_mode":
                   (dcalc_mode==FIN)? "financial_mode":
                   (dcalc_mode==STAT)? "statistics_mode":
                   "programming_mode");
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(w), 1);

	w = gtk_dcalc_get_widget("main", dcalc_degree? "degree": "radians");
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(w), 1);

	w = gtk_dcalc_get_widget("main",
                   (dcalc_floatmode==ENGFORMAT)? "engineering_numbering":
                   (dcalc_floatmode==SCIFORMAT)? "scientific_numbering":
                   "fixed_point_numbering");
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(w), 1);

	w = gtk_dcalc_get_widget("main","annuity_in_advance");
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(w), dcalc_finPayAt0? 1: 0);

	w = gtk_dcalc_get_widget("main", dcalc_algebraic_mode?"algebraic":"rpn");
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(w), 1);

    gtk_builder_connect_signals(builder, NULL);

	gui_dispnums();
	gui_msg(DEF_SIG(VERSION));

	if (gtk_dcalc_registers_popup) { /* set to 1 by gui_read_settings */
		gtk_dcalc_registers_popup = NULL;
		gtk_dcalc_toggle_registers();
	}

    pull_in_css_style();

	gtk_main ();
	return 0;
}


/* For emacs: */

/* Local Variables: */
/* eval:(setq tab-width 4) */
/* End: */
