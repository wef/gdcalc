/*
 * From: https://www.gnu.org/software/bison/manual/html_node/Mfcalc-Rules.html
 */

/* NB  reduce/reduce conflicts are exp vs iexp and not actually encountered */

%{
#include <stdio.h>  /* For printf, etc. */
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>   /* For pow, used in the grammar. */
#include <ctype.h>
#include "dcalc.h"
    
int yylex (void);
char *yylex_ptr;
void yyerror (char const *);

/* Function type. */
typedef double (func_t) (double);
typedef double (opr_t) (double, double);
typedef double (ropr_t) (double, double);
typedef long (ifunc_t) (long);
typedef long (iopr_t) (long, long);

#ifdef DEBUG
#  define TRACE(x) x
#else
#  define TRACE(x)
#endif

/* Data type for links in the chain of symbols. */
struct symrec {
    char *name;  /* name of symbol */
    int type;    /* type of symbol: either VAR, IVAR, IFUN, OPR, ROPR, IOPR */
    union {
        double var;    /* value of a VAR */
        func_t *fun;   /* value of a FUN */
        opr_t *opr;    /* value of an OPR */
        ropr_t *ropr;    /* value of a ROPR (OPR with reversed params) */

        /* integer types */
        long ivar;     /* value of an IVAR */
        ifunc_t *ifun; /* value of a FUN */
        iopr_t *iopr;  /* value of an IOPR */
    } value;
    struct symrec *next;  /* link field */
};
typedef struct symrec symrec;

/* The symbol table: a chain of 'struct symrec'. */
symrec *sym_table;
symrec *putsym (char const *name, int sym_type);
symrec *getsym (char const *name);

symrec *putsym (char const *name, int sym_type) {
    symrec *res = (symrec *) malloc (sizeof (symrec));
    res->name = strdup (name);
    res->type = sym_type;
    res->value.var = 0; /* Set value to 0 even if fun. */
    res->next = sym_table;
    sym_table = res;
    return res;
}

symrec *getsym (char const *name) {
    for (symrec *p = sym_table; p; p = p->next)
        if (strcmp (p->name, name) == 0)
            return p;
    return NULL;
}
%}


%define api.value.type union /* Generate YYSTYPE from these types: */
%token <double>  NUM     /* Double precision number. */
%token <long>  INUM     /* Integer number. */
%token <symrec*> VAR IVAR FUN IFUN OPR ROPR IOPR/* Symbol table pointer: variable/function. */
%nterm <double> exp
%nterm <long> iexp

%precedence '='
%left '-' '+'
%left '*' '/' '%' OPR ROPR IOPR
%precedence NEG /* negation--unary minus */
%right '^'      /* exponentiation */
%right '!' /* factorial */
              
%% /* The grammar follows. */

dummy:
exp    { dcalc_xfReg = $1; }
| iexp { dcalc_xiReg = $1; }
;

iexp:
INUM                  { ;                                }
| YYEOF               { TRACE(printf("YYEOF = %ld\n", $$)); dcalc_push($$); }
| IVAR                { TRACE(printf("IVAR '%s' = %ld %lo %lx\n", $1->name, $1->value.ivar, $1->value.ivar, $1->value.ivar)); $$ = $1->value.ivar; }
| IVAR '=' iexp       { $$ = $3; $1->value.ivar = $3;    }
| IFUN '(' iexp ')'   { $$ = $1->value.ifun ($3);        }
| iexp '+' iexp       { $$ = $1 + $3;                    }
| iexp '-' iexp       { $$ = $1 - $3;                    }
| iexp '*' iexp       { $$ = $1 * $3;                    }
| iexp '/' iexp       { $$ = $1 / $3;                    }
| '-' iexp  %prec NEG { $$ = -$2;                        }
| '~' iexp  %prec NEG { $$ = ~$2;                        }
| iexp '^' iexp       { $$ = dcalc_power($1, $3);        }
| iexp IOPR iexp      { $$ = $2->value.iopr($1, $3);     }
| '(' iexp ')'        { $$ = $2;                         }
;

exp:
NUM                  { ;                                }
| YYEOF              { TRACE(printf("YYEOF = %lf\n", $$)); dcalc_xfReg = $$; }
| VAR                { TRACE(printf("VAR '%s' = %lf\n", $1->name, $1->value.var)); $$ = $1->value.var;              }
| VAR '=' exp        { $$ = $3; $1->value.var = $3;     }
| FUN '(' exp ')'    { $$ = $1->value.fun ($3);         }
| exp '+' exp        { TRACE(printf("+\n")); $$ = $1 + $3;    TRACE(printf("$$=%lf\n", $$));                }
| exp '-' exp        { $$ = $1 - $3;                    }
| exp '*' exp        { $$ = $1 * $3;                    }
| exp '/' exp        { $$ = $1 / $3;                    }
| exp '%' exp        { $$ = $1 * $3 / 100;              }
| '-' exp  %prec NEG { $$ = -$2;                        }
| exp '^' exp        { $$ = pow ($1, $3);               }
| exp OPR exp        { $$ = $2->value.opr($1, $3);      }
| exp ROPR exp       { $$ = $2->value.ropr($3, $1);     }
| '(' exp ')'        { $$ = $2;                         }
| exp '!'            { $$ = dcalc_factorial($1);        }
;

/* End of grammar. */
%%

struct init1 {
  char const *name;
  func_t *fun;
};

struct init1 const funs[] = {
    { "sin",   dcalc_sin },
    { "cos",   dcalc_cos },
    { "tan",   dcalc_tan },
    { "asin",  dcalc_asin },
    { "acos",  dcalc_acos },
    { "atan",  dcalc_atan },
    { "sinh",  dcalc_sinh },
    { "cosh",  dcalc_cosh },
    { "tanh",  dcalc_tanh },
    { "asinh", dcalc_asinh },
    { "acosh", dcalc_acosh },
    { "atanh", dcalc_atanh },
    { "exp",   exp },
    { "log10", log10 },
    { "loge",  log },
    { "sqrt",  sqrt },
    { "int",   dcalc_int },
    { "frc",   dcalc_frc },
    { "rcl",   dcalc_rcl },
    { 0, 0 },
};

struct init2 {
    char const *name;
    opr_t *opr;
};

struct init2 const oprs[] = {
    { "datedelta", dcalc_datedelta },
    { "dateplus", dcalc_dateplus },
    { "perm", dcalc_perm },
    { "comb", dcalc_comb },
    { 0, 0 },
};

struct init3 {
    char const *name;
    ropr_t *ropr;
};

struct init3 const roprs[] = {
    { "percentch", dcalc_percentch },
    { "pluspercent", dcalc_pluspercent },
    { 0, 0 },
};

struct init4 {
    char const *name;
    ifunc_t *ifunc;
};

struct init4 const ifuns[] = {
    { "pf", dcalc_factors },
    { "rcl", dcalc_ircl },
    { 0, 0 },
};

struct init5 {
    char const *name;
    iopr_t *iopr;
};

struct init5 const ioprs[] = {
    { "and", dcalc_and },
    { "or", dcalc_or },
    { "xor", dcalc_xor },
    { "mod", dcalc_mod },
    { "lshift", dcalc_shiftl },
    { "rshift", dcalc_shiftr },
    { 0, 0 },
};

/* Put functions in table. */
void
yyinit (char *buf) {
    symrec *ptr;
    TRACE(printf("yyinit('%s')\n", buf));
    if (dcalc_mode == PROG) {
        for (int i = 0; ifuns[i].name; i++) {
            ptr = putsym (ifuns[i].name, IFUN);
            ptr->value.ifun = ifuns[i].ifunc;
        }
        for (int i = 0; ioprs[i].name; i++) {
            ptr = putsym (ioprs[i].name, IOPR);
            ptr->value.iopr = ioprs[i].iopr;
        }
        ptr = putsym ("x", IVAR);
        ptr->value.ivar = dcalc_xSave;
        ptr = putsym ("ans", IVAR);
        ptr->value.ivar = dcalc_xSave;
        ptr = putsym ("y", IVAR);
        ptr->value.ivar = dcalc_ySave;
        ptr = putsym ("z", IVAR);
        ptr->value.ivar = dcalc_zSave;
        ptr = putsym ("t", IVAR);
        ptr->value.ivar = dcalc_tSave;
        ptr = putsym ("l", IVAR);
        ptr->value.ivar = dcalc_lSave;
    } else {
        for (int i = 0; funs[i].name; i++) {
            ptr = putsym (funs[i].name, FUN);
            ptr->value.fun = funs[i].fun;
        }
        for (int i = 0; oprs[i].name; i++) {
            ptr = putsym (oprs[i].name, OPR);
            ptr->value.opr = oprs[i].opr;
        }
        for (int i = 0; roprs[i].name; i++) {
            ptr = putsym (roprs[i].name, ROPR);
            ptr->value.ropr = roprs[i].ropr;
        }
        ptr = putsym ("x", VAR);
        ptr->value.var = dcalc_xfSave;
        ptr = putsym ("ans", VAR);
        ptr->value.var = dcalc_xfSave;
        ptr = putsym ("y", VAR);
        ptr->value.var = dcalc_yfSave;
        ptr = putsym ("z", VAR);
        ptr->value.var = dcalc_zfSave;
        ptr = putsym ("t", VAR);
        ptr->value.var = dcalc_tfSave;
        ptr = putsym ("l", VAR);
        ptr->value.var = dcalc_lfSave;
        ptr = putsym ("pi", VAR);
        ptr->value.var = 4.0*atan(1.0);
        ptr = putsym ("e", VAR);
        ptr->value.var = exp(1.0);
    }

    yylex_ptr = buf;
}

int
yylex (void)
{
    int c = *yylex_ptr;
    char buf[MAX_ENTRY];

    TRACE(printf("yylex: '%s'\n", yylex_ptr));
    
    /* Ignore white space, get first nonwhite character. */
    while ((c = *yylex_ptr) != 0 && (c == ' ' || c == '\t'))
        yylex_ptr++;

    if (c == 0) {
        TRACE(printf("yylex returning YYEOF\n"));
            return YYEOF;
    }

    dcalc_cpy(buf, yylex_ptr, MAX_ENTRY);

    /* Char starts a number => parse the number. */
    if (dcalc_mode != PROG) {
        if (isdigit (c) || (c == '.')) {
            char *p;
            
            for (p = buf; *p == '.' || isdigit(*p); p++)
                yylex_ptr++;
            *p = 0;
            if (sscanf (buf, "%lf", &yylval.NUM) != 1) {
                yyerror(buf);
                yylval.NUM = 0.0;
            }
            TRACE(printf("yylex returning NUM %lf\n", yylval.NUM));
            return NUM;
        }
    } else { /* mode == PROG NB HEX number must start with a digit or 0x */
        if (isdigit (c)) {
            char *p;
            char *d, *s;
            long temp = 0;
            int isNeg = 0;
        
            switch (dcalc_intmode) {
            case ASCIIM: /* FIXME - this will never work!! */
                yylex_ptr += 4;
                buf[4] = 0;
            
                temp = dcalc_asc2int(buf);
                break;
            case IP:
                for (p = buf; isdigit(*p) || *p == '.'; p++)
                    yylex_ptr++;
                *p = 0;
                temp = dcalc_ip2int(buf);
                break;
            case BIN:
                for (p = buf; *p == '0' || *p == '1'; p++)
                    yylex_ptr++;
                *p = 0;
                if (strlen(buf) == 32 && buf[0] == '1') {
                    d = buf; s = d + 1; while (*s) *d++=*s++; *d=0;
                    isNeg = 1;
                }
                temp = strtol(buf, NULL, 2);
                break;
            case OCT:
                for (p = buf; *p >= '0' || *p <= '7'; p++)
                    yylex_ptr++;
                *p = 0;
                if (strlen(buf) == 11) {
                    if (buf[0] == '2') {
                        d = buf; s = d + 1; while (*s) *d++=*s++; *d=0;
                        isNeg = 1;
                    }
                    if (buf[0] == '3') {
                        buf[0] = '1';
                        isNeg = 1;
                    }
                }
                temp = strtol(buf, NULL, 8);
                break;
            case DEC:
                for (p = buf; isdigit(*p); p++)
                    yylex_ptr++;
                *p = 0;
                temp = strtol(buf, NULL, 10);
                break;
            case HEX:
                p = buf;
                if (*p == '0' && *(p+1) == 'x') {
                    strcpy(buf, p + 2);
                    yylex_ptr += 2;
                }
                for (; isxdigit(*p); p++)
                    yylex_ptr++;
                *p = 0;
                if (strlen(buf) == 8) {
                    char b[2];
                    int leftDigit;
                    b[0] = buf[0];
                    b[1] = 0;
                    leftDigit = strtol(b, NULL, 16);
                    if (leftDigit & 8) {
                        buf[0] = (leftDigit & 7) + '0';
                        isNeg = 1;
                    }
                }
                temp = strtol(buf, NULL, 16);
                break;
            }
            yylval.INUM = isNeg? temp|0x80000000: temp;
        
            TRACE(printf("yylex returning INUM %ld\n", yylval.INUM));
            return INUM;
        }
    }
    
    /* Char starts an identifier => read the name. */
    if (isalpha (c)) {
        static ptrdiff_t bufsize = 0;
        static char *symbuf = 0;

        ptrdiff_t i = 0;
        do

        {
            /* If buffer is full, make it bigger. */
            if (bufsize <= i)
            {
                bufsize = 2 * bufsize + 40;
                symbuf = realloc (symbuf, (size_t) bufsize);
            }
            /* Add this character to the buffer. */
            symbuf[i++] = (char) c;
            /* Get another character. */
            c = *++yylex_ptr;
        } while (isalnum (c));

        symbuf[i] = '\0';
        
        symrec *s = getsym (symbuf);
        if (dcalc_mode == PROG) {
            if (!s)
                s = putsym (symbuf, IVAR);
            yylval.IVAR = s;
            TRACE(printf("yylex returning %s of type %d value %ld\n", s->name, s->type, s->value.ivar));
        } else {
            if (!s)
                s = putsym (symbuf, VAR);
            yylval.VAR = s; /* or yylval.FUN = s. */
            TRACE(printf("yylex returning %s of type %d value %lf\n", s->name, s->type, s->value.var));
        }
        return s->type;
    }
    
    /* Any other character is a token by itself eg '-' !!!. */
    TRACE(printf("yylex returning char %c\n", c));
        yylex_ptr++;
    return c;
}

/* Called by yyparse on error. */
void yyerror (char const *s)
{
    char buf[MAX_ENTRY];
    snprintf(buf, 100, "yyparse: %s\n", s);
    gui_msg(buf);
}


